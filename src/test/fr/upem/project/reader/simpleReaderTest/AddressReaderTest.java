package test.fr.upem.project.reader.simpleReaderTest;

import main.frame.address.Ipv4Frame;
import main.reader.simpleReader.AddressReader;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import static org.junit.jupiter.api.Assertions.*;

public class AddressReaderTest {

    private void createIPv4Address(ByteBuffer address){
        address.put((byte) 4);
        address.put((byte) 192);
        address.put((byte) 52);
        address.put((byte) 12);
        address.put((byte) 24);
        address.putInt(7777);
    }

    @Test
    void shouldThrowNullPointerExceptionWhenCreateReaderWithNullBytebuffer(){
        assertThrows(NullPointerException.class, () -> new AddressReader(null));
    }

    @Test
    void shouldReturnIPv4Address(){
        var address = ByteBuffer.allocate(Byte.BYTES * 5 + Integer.BYTES);
        createIPv4Address(address);
        var reader = new AddressReader(address);
        reader.process();
        var ipv4Address = reader.get();
        assertAll(
                () -> {
                    assertTrue(ipv4Address instanceof Ipv4Frame);
                },
                () -> {
                    assert ipv4Address instanceof Ipv4Frame;
                    assertEquals(7777, ((Ipv4Frame) ipv4Address).getPort());
                },
                () -> {
                    assert ipv4Address instanceof Ipv4Frame;
                    assertArrayEquals(new byte[]{(byte) 192, 52, 12, 24}, ((Ipv4Frame) ipv4Address).getAddress());
                }
        );
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenDataNotYetRead(){
        var address = ByteBuffer.allocate(Byte.BYTES * 5 + Integer.BYTES);
        var reader = new AddressReader(address);
        reader.process();
        assertThrows(IllegalStateException.class, reader::get);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenProcessAlreadyDone(){
        var address = ByteBuffer.allocate(Byte.BYTES * 5 + Integer.BYTES);
        createIPv4Address(address);
        var reader = new AddressReader(address);
        reader.process();
        assertThrows(IllegalStateException.class, reader::process);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenIpVersionIsIncorrect(){
        var address = ByteBuffer.allocate(Byte.BYTES);
        address.put((byte) 5);
        var reader = new AddressReader(address);
        assertThrows(IllegalStateException.class, reader::process);
    }

}
