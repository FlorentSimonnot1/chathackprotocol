package test.fr.upem.project.reader.simpleReaderTest;

import main.reader.simpleReader.ByteReader;
import main.reader.simpleReader.LongReader;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class LongReaderTest {

    @Test
    void shouldThrowNullPointerExceptionWhenCreateReaderWithNullBytebuffer(){
        assertThrows(NullPointerException.class, () -> new LongReader(null));
    }


    @Test
    void shouldReturn2(){
        var bb = ByteBuffer.allocate(Long.BYTES);
        bb.putLong(2);
        var reader = new LongReader(bb);
        reader.process();
        var res = (long) reader.get();
        assertEquals(2, res);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenDataNotYetRead(){
        var bb = ByteBuffer.allocate(Long.BYTES);
        var reader = new LongReader(bb);
        reader.process();
        assertThrows(IllegalStateException.class, reader::get);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenProcessAlreadyDone(){
        var bb = ByteBuffer.allocate(Long.BYTES);
        bb.putLong(2);
        var reader = new LongReader(bb);
        reader.process();
        assertThrows(IllegalStateException.class, reader::process);
    }

}
