package test.fr.upem.project.reader.simpleReaderTest;

import main.reader.simpleReader.ByteReader;
import main.reader.simpleReader.ShortReader;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ShortReaderTest {

    @Test
    void shouldThrowNullPointerExceptionWhenCreateReaderWithNullBytebuffer(){
        assertThrows(NullPointerException.class, () -> new ShortReader(null));
    }


    @Test
    void shouldReturn2(){
        var bb = ByteBuffer.allocate(Short.BYTES);
        bb.putShort((short) 2);
        var reader = new ShortReader(bb);
        reader.process();
        var res = (short) reader.get();
        assertEquals(2, res);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenDataNotYetRead(){
        var bb = ByteBuffer.allocate(Short.BYTES);
        var reader = new ShortReader(bb);
        reader.process();
        assertThrows(IllegalStateException.class, reader::get);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenProcessAlreadyDone(){
        var bb = ByteBuffer.allocate(Short.BYTES);
        bb.putShort((short) 2);
        var reader = new ShortReader(bb);
        reader.process();
        assertThrows(IllegalStateException.class, reader::process);
    }

}
