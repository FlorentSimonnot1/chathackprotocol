package test.fr.upem.project.reader.simpleReaderTest;

import main.reader.simpleReader.AddressReader;
import main.reader.simpleReader.ByteReader;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.*;

class ByteReaderTest {

    @Test
    void shouldThrowNullPointerExceptionWhenCreateReaderWithNullBytebuffer(){
        assertThrows(NullPointerException.class, () -> new ByteReader(null));
    }

    @Test
    void shouldReturn2(){
        var bb = ByteBuffer.allocate(Byte.BYTES);
        bb.put((byte) 2);
        var reader = new ByteReader(bb);
        reader.process();
        var res = (byte) reader.get();
        assertEquals(2, res);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenDataNotYetRead(){
        var bb = ByteBuffer.allocate(Byte.BYTES);
        var reader = new ByteReader(bb);
        reader.process();
        assertThrows(IllegalStateException.class, () -> {
            var res = (byte) reader.get();
        });
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenProcessAlreadyDone(){
        var bb = ByteBuffer.allocate(Byte.BYTES);
        bb.put((byte) 2);
        var reader = new ByteReader(bb);
        reader.process();
        assertThrows(IllegalStateException.class, reader::process);
    }

}