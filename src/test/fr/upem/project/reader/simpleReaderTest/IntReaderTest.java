package test.fr.upem.project.reader.simpleReaderTest;

import main.reader.simpleReader.ByteReader;
import main.reader.simpleReader.IntReader;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.*;

public class IntReaderTest {

    @Test
    void shouldThrowNullPointerExceptionWhenCreateReaderWithNullBytebuffer(){
        assertThrows(NullPointerException.class, () -> new IntReader(null));
    }


    @Test
    void shouldReturn2(){
        var bb = ByteBuffer.allocate(Integer.BYTES);
        bb.putInt(2);
        var reader = new IntReader(bb);
        reader.process();
        var res = (int) reader.get();
        assertEquals(2, res);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenDataNotYetRead(){
        var bb = ByteBuffer.allocate(Integer.BYTES);
        var reader = new IntReader(bb);
        reader.process();
        assertThrows(IllegalStateException.class, reader::get);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenProcessAlreadyDone(){
        var bb = ByteBuffer.allocate(Integer.BYTES);
        bb.putInt(2);
        var reader = new IntReader(bb);
        reader.process();
        assertThrows(IllegalStateException.class, reader::process);
    }

}
