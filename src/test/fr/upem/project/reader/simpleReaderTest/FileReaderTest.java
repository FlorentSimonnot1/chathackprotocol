package test.fr.upem.project.reader.simpleReaderTest;

import main.frame.Utils;
import main.reader.Reader;
import main.reader.simpleReader.FileReader;
import main.reader.simpleReader.StringReader;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class FileReaderTest {
    /*private final String path = "/src/test/resources/files/";


    @Test
    void shouldReturnHelloWorld(){
        var file = Utils.putFile(Paths.get(path+"file.txt"));
        if(file == null){
            throw new IllegalArgumentException("File doesn't exists");
        }
        var bb = ByteBuffer.allocate(file.remaining());
        bb.put(file);
        var reader = new FileReader(bb);
        reader.process();
        ByteBuffer res = reader.get();
        String txt = StandardCharsets.UTF_8.decode(res).toString();
        assertEquals("Hello World!", txt);
    }

    @Test
    void shouldGetRefillStatusWhenBufferRemainingIsTooShort(){
        var bb = ByteBuffer.allocate(Short.BYTES);
        bb.putShort((short) 2);
        var reader = new FileReader(bb);
        var status = reader.process();
        assertEquals(Reader.ProcessStatus.REFILL, status);
    }

    @Test
    void shouldGetErrorStatusWhenSizeReadingIsNegative(){
        var bb = ByteBuffer.allocate(Integer.BYTES);
        bb.putInt(-100);
        var reader = new FileReader(bb);
        var status = reader.process();
        assertEquals(Reader.ProcessStatus.ERROR, status);
    }


    @Test
    void shouldThrowIllegalStateExceptionWhenProcessAlreadyDone(){
        var file = Utils.putFile(Paths.get(path+"file.txt"));
        if(file == null) throw new IllegalArgumentException("File doesn't exists");
        var bb = ByteBuffer.allocate(file.remaining());
        bb.put(file);
        var reader = new FileReader(bb);
        reader.process();
        assertThrows(IllegalStateException.class, reader::process);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenGetDataBeforeProcessDone(){
        var file = Utils.putFile(Paths.get(path+"file.txt"));
        if(file == null) throw new IllegalArgumentException("File doesn't exists");
        var bb = ByteBuffer.allocate(file.remaining());
        bb.put(file);
        var reader = new StringReader(bb);
        assertThrows(IllegalStateException.class, reader::get);
    }

    @Test
    void shouldGetRefillStatusWhenDataIsNotCompletelyReceive(){
        var file = Utils.putFile(Paths.get(path+"bigFile.txt"));
        if(file == null) throw new IllegalArgumentException("File doesn't exists");
        var bbout = ByteBuffer.allocate(1024);
        Utils.fillAsPossible(file, bbout);
        var reader = new FileReader(bbout);
        assertEquals(Reader.ProcessStatus.REFILL, reader.process());
    }

    @Test
    void shouldReadABigFileSuccessfully(){
        var file = Utils.putFile(Paths.get(path+"bigFile.txt"));
        if(file == null) throw new IllegalArgumentException("File doesn't exists");
        var bb = ByteBuffer.allocate(1024);
        Utils.fillAsPossible(file, bb);
        var reader = new FileReader(bb);
        var status = reader.process();
        while(file.hasRemaining()){
            Utils.fillAsPossible(file, bb);
            status = reader.process();
        }
        assertEquals(Reader.ProcessStatus.DONE, status);
    }
*/
}