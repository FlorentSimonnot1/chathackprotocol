package test.fr.upem.project.reader.simpleReaderTest;

import main.frame.Utils;
import main.reader.Reader;
import main.reader.simpleReader.ByteReader;
import main.reader.simpleReader.StringReader;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StringReaderTest {

    @Test
    void shouldThrowNullPointerExceptionWhenCreateReaderWithNullBytebuffer(){
        assertThrows(NullPointerException.class, () -> new StringReader(null));
    }


    @Test
    void shouldReturnHello(){
        var word = Utils.putString("Hello");
        var bb = ByteBuffer.allocate(word.remaining());
        bb.put(word);
        var reader = new StringReader(bb);
        reader.process();
        assertEquals("Hello", reader.get());
    }

    @Test
    void shouldGetRefillStatusWhenBufferRemainingIsTooShort(){
        var bb = ByteBuffer.allocate(Short.BYTES);
        bb.putShort((short) 2);
        var reader = new StringReader(bb);
        var status = reader.process();
        assertEquals(Reader.ProcessStatus.REFILL, status);
    }

    @Test
    void shouldGetErrorStatusWhenSizeReadingIsNegative(){
        var bb = ByteBuffer.allocate(Integer.BYTES);
        bb.putInt(-100);
        var reader = new StringReader(bb);
        var status = reader.process();
        assertEquals(Reader.ProcessStatus.ERROR, status);
    }


    @Test
    void shouldThrowIllegalStateExceptionWhenProcessAlreadyDone(){
        var word = Utils.putString("Hello");
        var bb = ByteBuffer.allocate(word.remaining());
        bb.put(word);
        var reader = new StringReader(bb);
        reader.process();
        assertThrows(IllegalStateException.class, reader::process);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenGetDataBeforeProcessDone(){
        var word = Utils.putString("Hello");
        var bb = ByteBuffer.allocate(word.remaining());
        bb.put(word);
        var reader = new StringReader(bb);
        assertThrows(IllegalStateException.class, reader::get);
    }

    @Test
    void shouldGetRefillStatusWhenDataIsNotCompletelyReceive(){
        var size = 2000;
        var bigWord = ByteBuffer.allocate(Byte.BYTES * size);
        for(int i = 0; i < size; i++) bigWord.put((byte) 1);
        bigWord.flip();
        var bb = ByteBuffer.allocate(Integer.BYTES + bigWord.remaining());
        bb.putInt(size*2).put(bigWord);
        var reader = new StringReader(bb);
        assertEquals(Reader.ProcessStatus.REFILL, reader.process());
    }

}