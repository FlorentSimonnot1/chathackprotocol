package test.fr.upem.project.reader.frameReaderTest;

import main.frame.AckMDPServerFrame;
import main.opcodes.Opcodes;
import main.reader.frameReader.AckMDPServerReader;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.*;

public class AckMDPServerReaderTest {

    @Test
    void shouldThrowNullPointerExceptionWhenCreateReaderWithNullBytebuffer(){
        assertThrows(NullPointerException.class, () -> new AckMDPServerReader(null, Opcodes.ASK_CREDENTIALS_OR_SERVER_MDP_SUCCESS));
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenCreateReaderWithBadOpcode(){
        assertThrows(IllegalArgumentException.class, () -> new AckMDPServerReader(ByteBuffer.allocate(Byte.BYTES), Opcodes.CQUIT));
    }

    @Test
    void shouldReturnGreatOpcodeAndId(){
        var bb = ByteBuffer.allocate(Long.BYTES);
        var time = System.currentTimeMillis();
        bb.putLong(time);
        var reader = new AckMDPServerReader(bb, Opcodes.ASK_GUEST_LOGIN_MDP_SERVER);
        reader.process();
        var frame = (AckMDPServerFrame) reader.get();
        assertAll(
                () -> assertEquals(Opcodes.ASK_GUEST_LOGIN_MDP_SERVER, frame.getOpcode()),
                () -> assertEquals(time, frame.getId())
        );
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenProcessAlreadyDone(){
        var bb = ByteBuffer.allocate(Long.BYTES);
        var time = System.currentTimeMillis();
        bb.putLong(time);
        var reader = new AckMDPServerReader(bb, Opcodes.ASK_GUEST_LOGIN_MDP_SERVER);
        reader.process();
        assertThrows(IllegalStateException.class, reader::process);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenGetDataIsCalledBeforeProcess(){
        var bb = ByteBuffer.allocate(Long.BYTES);
        var time = System.currentTimeMillis();
        bb.putLong(time);
        var reader = new AckMDPServerReader(bb, Opcodes.ASK_GUEST_LOGIN_MDP_SERVER);
        assertThrows(IllegalStateException.class, reader::get);
    }

}
