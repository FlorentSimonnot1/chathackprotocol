package test.fr.upem.project.reader.frameReaderTest;

import main.frame.ClientNotFoundFrame;
import main.frame.ClientToClientFrame;
import main.frame.ClientToServerErrorFrame;
import main.frame.Utils;
import main.reader.frameReader.ClientNotFoundReader;
import main.reader.frameReader.ClientToClientReader;
import main.reader.frameReader.ClientToServerErrorReader;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import static org.junit.jupiter.api.Assertions.*;

public class ClientToServerErrorReaderTest {
    @Test
    void shouldThrowNullPointerExceptionWhenBytebufferIsNull(){
        assertThrows(NullPointerException.class, () -> new ClientToServerErrorReader(null));
    }

    @Test
    void shouldReturnJean(){
        var jean = Utils.putString("Jean");
        var bb = ByteBuffer.allocate(jean.remaining());
        bb.put(jean);
        var reader = new ClientToServerErrorReader(jean);
        reader.process();
        var frame = (ClientToServerErrorFrame) reader.get();
        assertEquals("Jean", frame.getPseudo());
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenGetDataBeforeProcessDone(){
        var jean = Utils.putString("Jean");
        var bb = ByteBuffer.allocate(jean.remaining());
        bb.put(jean);
        var reader = new ClientToServerErrorReader(bb);
        assertThrows(IllegalStateException.class, reader::get);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenProcessAlreadyDone(){
        var jean = Utils.putString("Jean");
        var bb = ByteBuffer.allocate(jean.remaining());
        bb.put(jean);
        var reader = new ClientToServerErrorReader(bb);
        reader.process();
        assertThrows(IllegalStateException.class, reader::process);
    }
}
