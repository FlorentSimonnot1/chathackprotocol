package test.fr.upem.project.reader.frameReaderTest;

import main.frame.SendFileFrame;
import main.frame.Utils;
import main.reader.frameReader.SendFileReader;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

public class SendFileFrameReaderTest {

    @Test
    void shouldThrowNullPointerExceptionWhenBytebufferIsNull(){
        assertThrows(NullPointerException.class, () -> new SendFileReader(null));
    }

    @Test
    void shouldReturnGreatFrame(){
        var fileName = Utils.putString("Name");
        var fileTest = Utils.putString("aahahhe fffffjjr e,djofeffjfkle fkfkeerf");
        var bb = ByteBuffer.allocate(fileName.remaining() + fileTest.remaining());
        bb.put(fileName).put(fileTest);
        var reader = new SendFileReader(bb);
        reader.process();
        var frame = (SendFileFrame) reader.get();
        assertAll(
                () ->{
                    assertEquals("Name", frame.getName());
                },
                () -> {
                    assertEquals("aahahhe fffffjjr e,djofeffjfkle fkfkeerf", StandardCharsets.UTF_8.decode(frame.getFile()).toString());
                }
        );
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenGetDataBeforeProcessDone(){
        var test = Utils.putString("Test");
        var bb = ByteBuffer.allocate(test.remaining());
        bb.put(test);
        var reader = new SendFileReader(bb);
        assertThrows(IllegalStateException.class, reader::get);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenProcessAlreadyDone(){
        var fileName = Utils.putString("Name");
        var fileTest = Utils.putString("aahahhe fffffjjr e,djofeffjfkle fkfkeerf");
        var bb = ByteBuffer.allocate(fileName.remaining() + fileTest.remaining());
        bb.put(fileName).put(fileTest);
        var reader = new SendFileReader(bb);
        reader.process();
        assertThrows(IllegalStateException.class, reader::process);
    }


}
