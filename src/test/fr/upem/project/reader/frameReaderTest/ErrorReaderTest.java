package test.fr.upem.project.reader.frameReaderTest;

import main.frame.ErrorFrame;
import main.frame.Utils;
import main.opcodes.Opcodes;
import main.reader.frameReader.ErrorReader;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.*;

public class ErrorReaderTest {

    @Test
    void shouldThrowNullPointerExceptionWhenCreateReaderWithNullBytebuffer(){
        assertThrows(NullPointerException.class, () -> new ErrorReader(null));
    }

    @Test
    void shouldReadErrorNicknameAlreadyUsedAndReadAMessage(){
        var msg = Utils.putString("Error 404");
        var bb = ByteBuffer.allocate(Byte.BYTES + msg.remaining());
        bb.put(Opcodes.ERR_NICKNAME_ALREADY_USED).put(msg);
        var reader = new ErrorReader(bb);
        reader.process();
        var frame = (ErrorFrame) reader.get();
        assertAll(
                () -> {
                    assertEquals(Opcodes.ERR_NICKNAME_ALREADY_USED, frame.getOpcode());
                },
                () -> {
                    assertEquals("Error 404", frame.getMsg());
                }
        );
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenGetDataBeforeProcessDone(){
        var msg = Utils.putString("Error 404");
        var bb = ByteBuffer.allocate(Byte.BYTES + msg.remaining());
        bb.put(Opcodes.ERR_NICKNAME_ALREADY_USED).put(msg);
        var reader = new ErrorReader(bb);
        assertThrows(IllegalStateException.class, reader::get);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenProcessAlreadyDone(){
        var msg = Utils.putString("Error 404");
        var bb = ByteBuffer.allocate(Byte.BYTES + msg.remaining());
        bb.put(Opcodes.ERR_NICKNAME_ALREADY_USED).put(msg);
        var reader = new ErrorReader(bb);
        reader.process();
        assertThrows(IllegalStateException.class, reader::process);
    }

}
