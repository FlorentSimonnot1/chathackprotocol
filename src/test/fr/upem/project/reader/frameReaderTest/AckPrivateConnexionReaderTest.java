package test.fr.upem.project.reader.frameReaderTest;

import main.frame.AckPrivateConnexionFrame;
import main.frame.Utils;
import main.opcodes.Opcodes;
import main.reader.frameReader.AckPrivateConnexionReader;
import main.reader.frameReader.ErrorReader;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.*;

public class AckPrivateConnexionReaderTest {
    private static final int port = 7777;
    private static final long token = System.currentTimeMillis();
    private static final byte[] addressBytes = new byte[]{(byte) 192, 127, 52, 24};

    private AckPrivateConnexionReader createReader(){
        var pseudo = Utils.putString("jean");
        var bb = ByteBuffer.allocate(pseudo.remaining() + Byte.BYTES * 5 + Integer.BYTES + Long.BYTES);
        bb.put(pseudo).put((byte) 4).put(addressBytes).putInt(port).putLong(token);
        return new AckPrivateConnexionReader(bb);
    }

    @Test
    void shouldThrowNullPointerExceptionWhenCreateReaderWithNullBytebuffer(){
        assertThrows(NullPointerException.class, () -> new AckPrivateConnexionReader(null));
    }

    @Test
    void shouldReturnGreatFrame(){
        var reader = createReader();
        reader.process();
        var frame = (AckPrivateConnexionFrame) reader.get();
        assertAll(
                () -> {
                    assertEquals("jean", frame.getPseudo());
                },
                () -> {
                    assertEquals(token, frame.getToken());
                },
                () -> {
                    assertEquals(port, frame.getAddress().getPort());
                },
                () -> {
                    assertEquals(4, frame.getAddress().getVersion());
                },
                () -> {
                    assertArrayEquals(addressBytes, frame.getAddress().getAddress());
                }
        );
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenGetDataBeforeProcessDone(){
        var reader = createReader();
        assertThrows(IllegalStateException.class, reader::get);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenProcessAlreadyDone(){
        var reader = createReader();
        reader.process();
        assertThrows(IllegalStateException.class, reader::process);
    }
}
