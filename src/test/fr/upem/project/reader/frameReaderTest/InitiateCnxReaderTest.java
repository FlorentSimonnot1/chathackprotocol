package test.fr.upem.project.reader.frameReaderTest;

import main.frame.InitiateCnxFrame;
import main.frame.Utils;
import main.opcodes.Opcodes;
import main.reader.frameReader.ErrorReader;
import main.reader.frameReader.InitiateCnxReader;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.*;

public class InitiateCnxReaderTest {
    private static final long token = System.currentTimeMillis();

    @Test
    void shouldThrowNullPointerExceptionWhenCreateReaderWithNullBytebuffer(){
        assertThrows(NullPointerException.class, () -> new InitiateCnxReader(null));
    }

    @Test
    void shouldReturnGreatToken(){
        var bb = ByteBuffer.allocate(Long.BYTES);
        bb.putLong(token);
        var reader = new InitiateCnxReader(bb);
        reader.process();
        var frame = (InitiateCnxFrame) reader.get();
        assertEquals(token, frame.getTokenID());
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenGetDataBeforeProcessDone(){
        var bb = ByteBuffer.allocate(Long.BYTES);
        bb.putLong(token);
        var reader = new InitiateCnxReader(bb);
        assertThrows(IllegalStateException.class, reader::get);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenProcessAlreadyDone(){
        var bb = ByteBuffer.allocate(Long.BYTES);
        bb.putLong(token);
        var reader = new InitiateCnxReader(bb);
        reader.process();
        assertThrows(IllegalStateException.class, reader::process);
    }

}
