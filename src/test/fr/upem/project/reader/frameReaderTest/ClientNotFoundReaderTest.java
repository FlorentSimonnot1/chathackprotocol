package test.fr.upem.project.reader.frameReaderTest;

import main.frame.ClientNotFoundFrame;
import main.frame.Utils;
import main.opcodes.Opcodes;
import main.reader.frameReader.ClientNotFoundReader;
import main.reader.frameReader.ErrorReader;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.*;

public class ClientNotFoundReaderTest {

    @Test
    void shouldThrowNullPointerExceptionWhenByteBufferIsNull(){
        assertThrows(NullPointerException.class, () -> new ClientNotFoundReader(null));
    }

    @Test
    void shouldReturnJean(){
        var jean = Utils.putString("Jean");
        var bb = ByteBuffer.allocate(jean.remaining());
        bb.put(jean);
        var reader = new ClientNotFoundReader(bb);
        reader.process();
        var frame = (ClientNotFoundFrame) reader.get();
        assertEquals("Jean", frame.getDest());
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenGetDataBeforeProcessDone(){
        var jean = Utils.putString("Jean");
        var bb = ByteBuffer.allocate(jean.remaining());
        bb.put(jean);
        var reader = new ClientNotFoundReader(bb);
        assertThrows(IllegalStateException.class, reader::get);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenProcessAlreadyDone(){
        var jean = Utils.putString("Jean");
        var bb = ByteBuffer.allocate(jean.remaining());
        bb.put(jean);
        var reader = new ClientNotFoundReader(bb);
        reader.process();
        assertThrows(IllegalStateException.class, reader::process);
    }

}
