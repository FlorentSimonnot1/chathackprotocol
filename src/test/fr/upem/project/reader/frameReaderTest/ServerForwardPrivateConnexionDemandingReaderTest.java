package test.fr.upem.project.reader.frameReaderTest;

import main.frame.ServerForwardPrivateConnexionDemandingFrame;
import main.frame.Utils;
import main.reader.frameReader.SendFileReader;
import main.reader.frameReader.ServerForwardPrivateConnexionDemandingReader;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.*;

public class ServerForwardPrivateConnexionDemandingReaderTest {
    private static final String name = "Jean";

    private ServerForwardPrivateConnexionDemandingReader createReader(){
        var jean = Utils.putString(name);
        var bb = ByteBuffer.allocate(jean.remaining());
        bb.put(jean);
        return new ServerForwardPrivateConnexionDemandingReader(bb);
    }

    @Test
    void shouldThrowNullPointerExceptionWhenBytebufferIsNull(){
        assertThrows(NullPointerException.class, () -> new ServerForwardPrivateConnexionDemandingReader(null));
    }

    @Test
    void shouldReturnJean(){
        var reader = createReader();
        reader.process();
        var frame = (ServerForwardPrivateConnexionDemandingFrame) reader.get();
        assertEquals(name, frame.getSender());
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenGetDataBeforeProcessDone(){
        var reader = createReader();
        assertThrows(IllegalStateException.class, reader::get);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenProcessAlreadyDone(){
        var reader = createReader();
        reader.process();
        assertThrows(IllegalStateException.class, reader::process);
    }

}
