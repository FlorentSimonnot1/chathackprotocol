package test.fr.upem.project.reader.frameReaderTest;

import main.frame.PrivateMsgFrame;
import main.frame.Utils;
import main.reader.frameReader.PrivateMsgReader;
import main.reader.frameReader.SendFileReader;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

public class PrivateMessageReaderTest {
    private static final String hello = "Hello everyone !";

    private PrivateMsgReader createReader(){
        var msg = StandardCharsets.UTF_8.encode(hello);
        var bb = ByteBuffer.allocate(Integer.BYTES + msg.remaining());
        bb.putInt(msg.remaining()).put(msg);
        return new PrivateMsgReader(bb);
    }

    @Test
    void shouldThrowNullPointerExceptionWhenBytebufferIsNull(){
        assertThrows(NullPointerException.class, () -> new PrivateMsgReader(null));
    }

    @Test
    void shouldReturnHelloEveryone(){
        var reader = createReader();
        reader.process();
        var frame = (PrivateMsgFrame) reader.get();
        assertEquals(hello, frame.getMessage());
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenGetDataBeforeProcessDone(){
        var reader = createReader();
        assertThrows(IllegalStateException.class, reader::get);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenProcessAlreadyDone(){
        var reader = createReader();
        reader.process();
        assertThrows(IllegalStateException.class, reader::process);
    }

}
