package test.fr.upem.project.reader.frameReaderTest;


import main.frame.ClientToClientFrame;
import main.frame.Utils;
import main.reader.frameReader.ClientNotFoundReader;
import main.reader.frameReader.ClientToClientReader;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

public class ClientToClientReaderTest {

    @Test
    void shouldThrowNullPointerExceptionWhenBytebufferIsNull(){
        assertThrows(NullPointerException.class, () -> new ClientToClientReader(null));
    }

    @Test
    void shouldReturnJean(){
        var jean = Utils.putString("Jean");
        var bb = ByteBuffer.allocate(jean.remaining());
        bb.put(jean);
        var reader = new ClientToClientReader(jean);
        reader.process();
        var frame = (ClientToClientFrame) reader.get();
        assertEquals("Jean", frame.getPseudo());
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenGetDataBeforeProcessDone(){
        var jean = Utils.putString("Jean");
        var bb = ByteBuffer.allocate(jean.remaining());
        bb.put(jean);
        var reader = new ClientToClientReader(bb);
        assertThrows(IllegalStateException.class, reader::get);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenProcessAlreadyDone(){
        var jean = Utils.putString("Jean");
        var bb = ByteBuffer.allocate(jean.remaining());
        bb.put(jean);
        var reader = new ClientToClientReader(bb);
        reader.process();
        assertThrows(IllegalStateException.class, reader::process);
    }

}
