package test.fr.upem.project.reader.factoryTest;

import main.opcodes.Opcodes;
import main.reader.factory.ReaderFactory;
import main.reader.frameReader.AckReader;
import main.reader.frameReader.ErrorReader;
import main.reader.frameReader.LoginWithCredentialsReader;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.*;


/**
 * Tests the ReaderFactory class
 */
class ReaderFactoryTest {

    @Test
    void createReaderFactory() {
        assertThrows(NullPointerException.class, () -> ReaderFactory.createReaderFactory(null));
    }

    @Test
    void getReader() {
        var bb = ByteBuffer.allocate(1024);
        var factory = ReaderFactory.createReaderFactory(bb);
        assertAll(
                () -> assertTrue(factory.getReader(Opcodes.ERR) instanceof ErrorReader),
                () -> assertTrue(factory.getReader(Opcodes.ASK_LOG_CNX) instanceof LoginWithCredentialsReader),
                () -> assertTrue(factory.getReader(Opcodes.CQUIT) instanceof AckReader),
                () -> assertThrows(IllegalArgumentException.class, () -> factory.getReader((byte) -127)),
                () -> assertThrows(IllegalArgumentException.class, () -> factory.getReader((byte) 127))
        );
    }
}