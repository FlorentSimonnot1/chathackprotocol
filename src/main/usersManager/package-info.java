/**
 * Provides UsersManager classes used to stock information about clients.
 * We can know which client is connected. Their login or some requests for example.
 */
package main.usersManager;