package main.usersManager;

import java.nio.channels.SelectionKey;

/**
 * User class represents a user. The class makes it easier to save the SelectionKey and the connection mode.
 */
public class User {
    private final SelectionKey key;
    private final boolean guestConnexion;

    /**
     * Create a new user identified by the SelectionKey <i>key</i>. Precise with <i>guestConnexion</i> if the user wanting to connect in guest mode.
     * @param key the user SelectionKey
     * @param guestConnexion true if the user wanting to connect in guest mode. False if it used the authentication mode.
     */
    public User(SelectionKey key, boolean guestConnexion) {
        this.key = key;
        this.guestConnexion = guestConnexion;
    }

    /**
     * Get the user SelectionKey
     * @return the user SelectionKey
     */
    public SelectionKey getKey() {
        return key;
    }

    /**
     * Check if user use a guest mode.
     * @return true if the user used a guest mode. False else.
     */
    public boolean isGuestConnexion() {
        return guestConnexion;
    }
}
