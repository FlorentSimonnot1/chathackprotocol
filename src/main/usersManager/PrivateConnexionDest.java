package main.usersManager;

import java.nio.channels.SelectionKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * PrivateConnexionDest class make the management of private connections on the side of the dest client easier.
 */
public class PrivateConnexionDest {
    private final List<String> privateConnexionInvitations = new ArrayList<>();
    private final Map<Long, String> waitingConnexion = new HashMap<>();
    private final Map<SelectionKey, String> interlocutorsKey = new HashMap<>();

    /**
     * Register the user nickname wanting to create a private connection
     * @param interlocutor the interlocutor nickname
     */
    public void addNewPrivateConnexionInvitation(String interlocutor){privateConnexionInvitations.add(interlocutor);}

    /**
     * Check if interlocutor already send a private connection invitation.
     * @param interlocutor the interlocutor nickname
     * @return true if interlocutor already send a private connection invitation. False else.
     */
    public boolean invitationAlreadyExists(String interlocutor){return privateConnexionInvitations.contains(interlocutor);}

    /**
     * Remove the private connection invitation send by the interlocutor.
     * @param interlocutor the interlocutor nickname
     */
    public void removeInvitation(String interlocutor){privateConnexionInvitations.remove(interlocutor);}

    /**
     * Register a private connection request initiate by <i>interlocutor</i> identified by the <i>tokenId</i> in a waiting list.
     * @param tokenId the request tokenId
     * @param interlocutor the user nickname
     */
    public void putNewWaitingConnexion(Long tokenId, String interlocutor){
        waitingConnexion.put(tokenId, interlocutor);
    }

    /**
     * Check if the <i>tokenId</i> corresponds to a private connexion request.
     * @param tokenId the tokenId of a request
     * @return true if the <i>tokenId</i> corresponds to a private connexion request. False else.
     */
    public boolean waitingConnexionExists(Long tokenId){
        return waitingConnexion.containsKey(tokenId);
    }

    /**
     * Remove the private connection request identified by the <i>tokenId</i> from the waiting list.
     * @param tokenId a private connection request tokenId
     */
    public void removePrivateConnexionWaiting(Long tokenId){waitingConnexion.remove(tokenId);}

    /**
     * Get the nickname of the interlocutor associate to the private connection request tokenId.
     * @param tokenId the private connection request tokenId
     * @return the nickname of the interlocutor
     * @throws IllegalArgumentException if tokenId isn't a private connection request tokenId
     */
    public String getInterlocutorOfDiscussion(Long tokenId){
        if(!waitingConnexionExists(tokenId)) throw new IllegalArgumentException("discussion " + tokenId + " doesn't exists !");
        return waitingConnexion.get(tokenId);
    }

    /**
     * Register a new interlocutor.
     * @param key the interlocutor SelectionKey
     * @param interlocutor the interlocutor nickname
     */
    public void registerInterlocutorKey(SelectionKey key, String interlocutor){
        interlocutorsKey.put(key, interlocutor);
    }

    /**
     * Check if the user represented by the SelectionKey <i>key</i> is connected with the current user.
     * @param key a user SelectionKey
     * @return true if the user represented by the SelectionKey <i>key</i> is connected with the current user. False else.
     */
    public boolean keyExists(SelectionKey key){return interlocutorsKey.containsKey(key);}

    /**
     * Get the user nickname corresponding to the user associate to the SelectionKey <i>key</i>
     * @param key the user SelectionKey
     * @return the nickname associate to the selectionKey
     * @throws IllegalArgumentException if interlocutor represented by the <i>key</i> doesn't exists
     */
    public String getInterlocutorOfDiscussion(SelectionKey key){
        if(!interlocutorsKey.containsKey(key)) throw new IllegalArgumentException("Any user for the key " + key);
        return interlocutorsKey.get(key);
    }

    /**
     * Check if the user represented by the nickname <i>interlocutor</i> is connected with the current user.
     * @param interlocutor the interlocutor nickname
     * @return true if the user represented by the nickname is an interlocutor of the current user. False else.
     */
    public boolean privateConnexionAlreadyExists(String interlocutor){
        return interlocutorsKey.entrySet().stream().anyMatch(u -> u.getValue().equals(interlocutor));
    }

    /**
     * Get the SelectionKey of the interlocutor represented by the nickname <i>interlocutor</i>.
     * @param interlocutor the interlocutor nickname
     * @return the SelectionKey associate to the nickname
     * @throws IllegalArgumentException if interlocutor represented by the <i>interlocutor</i> nickname doesn't exists
     */
    public SelectionKey getInterlocutorKey(String interlocutor){
        var res = interlocutorsKey.entrySet().stream().filter(u -> u.getValue().equals(interlocutor)).findFirst();
        if(res.isPresent()) return res.get().getKey();
        throw new IllegalArgumentException(interlocutor + " is not in register");
    }

    /**
     * Clear manager when user identified by the SelectionKey <i>key</i> wanting to disconnect.
     * @param key the user SelectionKey
     */
    public void clearAfterDisconnection(SelectionKey key){
        interlocutorsKey.remove(key);
    }

    /**
     * Apply action for each value from the interlocutors.
     * @param action - an action.
     */
    public void forEach(BiConsumer<? super SelectionKey, ? super String> action){
        interlocutorsKey.forEach(action);
    }



}
