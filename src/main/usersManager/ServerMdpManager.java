package main.usersManager;

import java.util.HashMap;
import java.util.Map;

/**
 * ServerMdpManager class allow to save requests from the server to the MDP Server.
 */
public class ServerMdpManager {

    private final Map<Long, User> mdpServerRequests = new HashMap<>();

    /**
     * Register a new request identified by a tokenId <i>id</i> and initiate by the user <i>user</i>
     * @param id - the request tokenId
     * @param user - the user wanting to initiate the request
     * @see User
     */
    public void addNewMdpServerRequest(Long id, User user){
        mdpServerRequests.putIfAbsent(id, user);
    }

    /**
     * Check if the request exists.
     * @param id - the request tokenId
     * @return true if the request identified by the tokenId <i>id</i> exists. False else.
     */
    public boolean requestsAlreadyExists(Long id){
        return mdpServerRequests.containsKey(id);
    }

    /**
     * Remove the request identified by the tokenId <i>id</i>.
     * @param id - the request tokenId
     */
    public void removeMdpServerRequest(Long id){
        mdpServerRequests.remove(id);
    }

    /**
     * Get the user associate to the request tokenId.
     * @param id - the tokenId of a request
     * @return the user associate to the request tokenId <i>id</i>
     * @throws IllegalArgumentException if the tokenId doesn't exists
     * @see User
     */
    public User getUser(Long id){
        if(!mdpServerRequests.containsKey(id)) throw new IllegalArgumentException();
        return mdpServerRequests.get(id);
    }


}
