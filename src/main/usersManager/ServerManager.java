package main.usersManager;

import java.nio.channels.SelectionKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ServerManager class implements methods to save some information about clients and their requests.
 */
public class ServerManager {

    private final Map<SelectionKey, String> waitingConnexion = new HashMap<>();
    private final Map<SelectionKey, String> connexions = new HashMap<>();
    private final Map<String, List<String>> privateConnexionRequests = new HashMap<>();

    /**
     * Register a user wanting to connect. The user is represented by a SelectionKey and a login.
     * @param key the user SelectionKey
     * @param login the user nickname
     */
    public void addUserWaitingConnexion(SelectionKey key, String login){
        waitingConnexion.putIfAbsent(key, login);
    }

    /**
     * Check if a user is already waiting for a connection.
     * @param key the user SelectionKey
     * @return true if the user have already asked to connect to the server, false else.
     */
    public boolean userAlreadyWaitingConnexion(SelectionKey key){
        return waitingConnexion.containsKey(key);
    }

    /**
     * Remove a user from the connections waiting list.
     * @param key the user selectionKey
     */
    public void removeUserWaitingConnexion(SelectionKey key){
        waitingConnexion.remove(key);
    }

    /**
     * Get the login associate to a user SelectionKey.
     * @param key the user SelectionKey
     * @return the login as String associate to the user
     * @exception IllegalArgumentException if the user is not registered
     */
    public String getLogin(SelectionKey key){
        if(!waitingConnexion.containsKey(key)) throw new IllegalArgumentException("User " + key + " is not registered");
        return waitingConnexion.get(key);
    }

    /**
     * Register an established connection for the user who is represented by the SelectionKey and the login.
     * @param key the user SelectionKey
     * @param login the user nickname
     */
    public void addNewConnexion(SelectionKey key, String login){
        connexions.putIfAbsent(key, login);
    }

    /**
     * Check if the user is already connected.
     * @param key the user SelectionKey
     * @return true if the user is already connected. False else.
     */
    public boolean userAlreadyConnected(SelectionKey key){
        return connexions.containsKey(key);
    }

    /**
     * Remove a user from the connections list.
     * @param key the user SelectionKey
     */
    public void removeConnexion(SelectionKey key){
        connexions.remove(key);
    }

    /**
     * Check if a user is connected.
     * @param login the user nickname
     * @return true if the user represented by the <i>login</i> is connected. False else.
     */
    public boolean userExists(String login){
        return connexions.entrySet().stream().anyMatch(e -> login.equals(e.getValue()));
    }

    /**
     * Get the nickname corresponding to a user who is represented by a SelectionKey.
     * @param key the user SelectionKey
     * @return the nickname associate to the user
     *
     * @throws IllegalArgumentException if the user is not connected
     */
    public String getLoginConnectedUser(SelectionKey key){
        if(!connexions.containsKey(key)) throw new IllegalArgumentException();
        return connexions.get(key);
    }

    /**
     * Get the SelectionKey of a user represented by a nickname.
     * @param interlocutor the user nickname
     * @return the SelectionKey associate to the user
     * @throws IllegalArgumentException if the user is not connected
     */
    public SelectionKey searchConnectedUser(String interlocutor){
        var res = connexions.entrySet().stream().filter(u -> u.getValue().equals(interlocutor)).findFirst();
        if(res.isPresent()) return res.get().getKey();
        throw new IllegalArgumentException(interlocutor + " is not in register");
    }

    /**
     * Register a user represented by the <i>src</i> nickname wanting to initiate a private connexion with the user represented by the <i>dest</i> nickname.
     * @param src the nickname of the user wanting to initiate a connection
     * @param dest the nickname of the target user
     */
    public void addPrivateConnexionRequest(String src, String dest){
        if(!privateConnexionRequests.containsKey(dest)){
            privateConnexionRequests.put(dest, new ArrayList<>());
        }
        privateConnexionRequests.get(dest).add(src);
    }

    /**
     * Remove a private connexion request between the users represented by the nicknames <i>src</i> and <i>dest</i>.
     * @param src the nickname of the user wanting to initiate a connection
     * @param dest the nickname of the target user
     */
    public void removePrivateConnexionRequest(String src, String dest){
        if(!privateConnexionRequests.containsKey(dest)) return;
        privateConnexionRequests.get(dest).remove(src);
    }

    /**
     * Check if a private connection request between the users represented by the nicknames <i>src</i> and <i>dest</i> is already demanding.
     * @param src the nickname of the user wanting to initiate a connection
     * @param dest the nickname of the target user
     * @return true if a request exists for this users. False else.
     */
    public boolean privateConnexionRequestExists(String src, String dest){
        if(privateConnexionRequests.containsKey(dest)){
            return privateConnexionRequests.get(dest).contains(src);
        }
        return false;
    }

    /**
     * Clear data about the user associate to the SelectionKey <i>key</i>.
     * Clear a waiting for connection, a established connection and all of its private connection requests.
     * @param key the user SelectionKey
     */
    public void clearAllDataForUser(SelectionKey key){
        System.out.println("KEY " + key);
        var login = getLoginConnectedUser(key);
        waitingConnexion.remove(key);
        connexions.remove(key);
        //Clear received requests
        privateConnexionRequests.remove(login);
        //Clear requests send by the current user
        privateConnexionRequests.forEach((k, v) -> {
            v.forEach(u -> {
                if(u.equals(login)) v.remove(u);
            });
        });
    }
}
