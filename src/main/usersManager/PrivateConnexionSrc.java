package main.usersManager;

import java.nio.channels.SelectionKey;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;

/**
 * PrivateConnexionSrc class make the management of private connections on the side of the source client easier.
 */
public class PrivateConnexionSrc {
    private final Map<String, Optional<Long>> privateConnexions = new HashMap<>();
    private final Map<String, SelectionKey> interlocutorsKey = new HashMap<>();

    /**
     * Add a new private connexion when it's established.
     * @param interlocutor  pseudo of the interlocutor
     * @param tokenId  conversation's id
     */
    public void putNewPrivateConnexion(String interlocutor, Long tokenId){
        privateConnexions.put(interlocutor, Optional.of(tokenId));
    }

    /**
     * Remove the interlocutor and its key.
     * @param interlocutor  the interlocutor nickname
     */
    public void removePrivateConnexionRequest(String interlocutor){
        privateConnexions.remove(interlocutor);
    }

    /**
     * Check if a conversation with <i>interlocutor</i> is already established.
     * @param interlocutor  the interlocutor's interlocutor
     * @return true if connexion is already established. False else.
     */
    public boolean privateConnexionAlreadyEstablished(String interlocutor){
        return privateConnexions.containsKey(interlocutor) && privateConnexions.get(interlocutor).isPresent();
    }

    /**
     * Initiate a private connection request for the interlocutor
     * @param interlocutor  the interlocutor nickname
     */
    public void putNewPrivateConnexionRequest(String interlocutor){
        privateConnexions.put(interlocutor, Optional.empty());
    }

    /**
     * Check if a private connection between the current user and the interlocutor represented by the nickname <i>interlocutor</i> have been already asked.
     * @param interlocutor  the interlocutor nickname
     * @return true if a private connection between the current user and the interlocutor represented by the nickname <i>interlocutor</i> have been already asked. False else.
     */
    public boolean privateConnexionAlreadyAsked(String interlocutor){
        return privateConnexions.containsKey(interlocutor) && privateConnexions.get(interlocutor).isEmpty();
    }

    /**
     * Check if the interlocutor represented by the nickname <i>interlocutor</i> isn't already registered
     * @param interlocutor  the interlocutor nickname
     * @return true if the interlocutor represented by the nickname <i>interlocutor</i> isn't already registered. False else.
     */
    public boolean userNotAlreadyRegister(String interlocutor){
        return !privateConnexions.containsKey(interlocutor);
    }

    /**
     * Get the nickname of the interlocutor associate to the private connexion request corresponding to the <i>tokenId</i>.
     * @param tokenId  the private connection request tokenId
     * @return  the nickname of the interlocutor
     * @throws IllegalArgumentException if the tokenId not corresponding to a private connection request
     */
    public String searchInterlocutor(Long tokenId){
        var res = privateConnexions.entrySet().stream().filter(u -> u.getValue().equals(Optional.of(tokenId))).findFirst();
        if(res.isPresent()) return res.get().getKey();
        throw new IllegalArgumentException("Discussion " + tokenId + " doesn't exists");
    }

    /**
     * Register a new interlocutor represented by the nickname <i>interlocutor</i> and the SelectionKey <i>key</i>.
     * @param interlocutor  the interlocutor nickname
     * @param key  the interlocutor SelectionKey
     */
    public void registerInterlocutorKey(String interlocutor, SelectionKey key){
        interlocutorsKey.put(interlocutor, key);
    }

    /**
     * Get the SelectionKey of the interlocutor represented by the nickname <i>interlocutor</i>.
     * @param interlocutor  the interlocutor nickname
     * @return the SelectionKey associate to the nickname
     * @throws IllegalArgumentException if interlocutor represented by the <i>interlocutor</i> nickname doesn't exists
     */
    public SelectionKey getInterlocutorKey(String interlocutor){
        if(!interlocutorsKey.containsKey(interlocutor)) throw new IllegalArgumentException();
        return interlocutorsKey.get(interlocutor);
    }

    /**
     * Check if the user represented by the SelectionKey <i>key</i> is connected with the current user.
     * @param key  the user SelectionKey
     * @return true if the user represented by the SelectionKey is an interlocutor of the current user. False else.
     */
    public boolean privateConnexionAlreadyExists(SelectionKey key){
        return interlocutorsKey.entrySet().stream().anyMatch(u -> u.getValue().equals(key));
    }

    /**
     * Get the nickname of the interlocutor represented by the SelectionKey <i>key</i>.
     * @param key the interlocutor SelectionKey
     * @return the nickname associate to the selectionKey
     * @throws IllegalArgumentException if interlocutor represented by the <i>key</i> doesn't exists
     */
    public String searchInterlocutor(SelectionKey key){
        var res = interlocutorsKey.entrySet().stream().filter(u -> u.getValue().equals(key)).findFirst();
        if(res.isPresent()) return res.get().getKey();
        throw new IllegalArgumentException(key + " is not registered");
    }

    /**
     * Clear the manager when the interlocutor quit the private connection.
     * @param dest the interlocutor
     */
    public void clearAfterDisconnection(String dest){
        privateConnexions.remove(dest);
        interlocutorsKey.remove(dest);
    }

    /**
     * Check if the user represented by the SelectionKey <i>key</i> is connected with the current user.
     * @param key the interlocutor SelectionKey
     * @return true if the interlocutor is connected with the current user. False else.
     */
    public boolean keyExists(SelectionKey key){
        return interlocutorsKey.entrySet().stream().anyMatch(u -> u.getValue().equals(key));
    }

    /**
     * Do action for each key from the map.
     * @param action an action
     */
    public void forEach(BiConsumer<? super String, ? super SelectionKey> action){
        interlocutorsKey.forEach(action);
    }

}
