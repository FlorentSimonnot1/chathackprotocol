package main.opcodes;

/**
 * This interface defines all opcodes use in frames.
 * We can found request opcodes, response opcodes and error opcodes.
 * @author Florent Simonnot
 */
public interface Opcodes {

    //Opcodes used in frames which are send to MDP Server.

    /**
     * <i>ASK_CREDENTIALS_OR_SERVER_MDP_SUCCESS</i> represents an authentication login to MDP Server or a positive response from this Server.
     */
    byte ASK_CREDENTIALS_OR_SERVER_MDP_SUCCESS = 1;
    /**
     * <i>ASK_GUEST_LOGIN_MDP_SERVER</i> represents a guest login to MDP Server.
     */
    byte ASK_GUEST_LOGIN_MDP_SERVER = 2;
    /**
     * <i>ACK_SERVER_MDP_FAILED</i> represents a failed connexion to MDP Server because of a wrong login/password .
     */
    byte ACK_SERVER_MDP_FAILED = 0;

    // Opcodes used to precise the IP Address version.

    /**
     * <i>IPV4</i> represents an IPV4 Address.
     */
    byte IPV4 = 4;
    /**
     * <i>IPV6</i> represents an IPV6 Address.
     */
    byte IPV6 = 6;

    //Opcodes used for connexions.

    /**
     * <i>ASK_PERMISSION_PV_CNX</i> send a private connexion request from the current client to the server.
     */
    byte ASK_PERMISSION_PV_CNX = 7;
    /**
     * <i>ASK_GUEST_CNX</i> send a guest login request.
     */
    byte ASK_GUEST_CNX = 20;
    /**
     * <i>ASK_LOG_CNX</i> send an authentication login request.
     */
    byte ASK_LOG_CNX = 21;
    /**
     * <i>ASK_PV_CNX</i> send a private connexion request to the interlocutor.
     */
    byte ASK_PV_CNX = 22;

    //Opcodes used by the server to a client when preparing a private connexion.

    /**
     * <i>BROADCAST_MSG</i> send a broadcast message to the server.
     */
    byte BROADCAST_MSG = 10;
    /**
     * <i>PV_CNX_ASKED</i> send a private connexion request receive by the source client to the target client.
     */
    byte PV_CNX_ASKED = 11;

    //Opcodes for send a public/private message and file.

    /**
     * <i>SND_BROADCAST_MSG</i> send a message for all clients which are connected.
     */
    byte SND_BROADCAST_MSG = 14;
    /**
     * <i>SND_PV_MSG</i> send a message to a client. A private connexion have to be established.
     */
    byte SND_PV_MSG = 15;
    /**
     * <i>SND_PV_FILE</i> send a file to a client. A private connexion have to be established.
     */
    byte SND_PV_FILE = 16;

    //Opcodes for quit a connexion.

    /**
     * <i>SQUIT</i> close a client-server connexion.
     */
    byte SQUIT = 110;
    /**
     * <i>CQUIT</i> close a client-client connexion.
     */
    byte CQUIT = 120;

    //Response values

    /**
     * <i>ACK_MSG_SEND</i> response send when <i>SND_BROADCAST_MSG</i> is asked.
     */
    byte ACK_MSG_SEND = 80;
    /**
     * <i>ACK_MSG_RECV</i> response send when <i>BROADCAST_MSG</i> is used.
     */
    byte ACK_MSG_RECV = 81;
    /**
     * <i>ACK_ACCEPT_PV_CNX</i> response send when <i>PV_CNX_ASKED</i> is asked. The client accepts a private connexion.
     */
    byte ACK_ACCEPT_PV_CNX = 82;
    /**
     * <i>ACK_REFUSE_PV_CNX</i> response send when <i>PV_CNX_ASKED</i> is asked. The client refuses a private connexion.
     */
    byte ACK_REFUSE_PV_CNX = 83;
    /**
     * <i>ACK_PV_CNX_ESTABLISHED</i> response when a private connexion (client-client) is successfully established.
     */
    byte ACK_PV_CNX_ESTABLISHED = 84;
    /**
     * <i>ACK_SV_CNX_ESTABLISHED</i> response when a client-server connexion is successfully established.
     */
    byte ACK_SV_CNX_ESTABLISHED = 85;

    //Error values

    /**
     * <i>ERR</i> must be used when a error throw. This opcode precedes an other error opcode which specify the error.
     */
    byte ERR = -1;
    /**
     * <i>ERR_SV_CNX</i> reports an error when trying a client-server connexion.
     */
    byte ERR_SV_CNX = -2;
    /**
     * <i>ERR_PV_CNX</i> reports an error when trying a client-client connexion.
     */
    byte ERR_PV_CNX = -3;

    //Next errors report a problem with user nickname, password..

    /**
     * <i>ERR_NO_SUCH_NICKNAME</i> reports an error when we used a nickname which doesn't exists.
     */
    byte ERR_NO_SUCH_NICKNAME = -4;
    /**
     * <i>ERR_NICKNAME_ALREADY_USED</i> reports an error when user use a nickname which is already use.
     */
    byte ERR_NICKNAME_ALREADY_USED = -5;
    /**
     * <i>ERR_USER_NOT_LOGGED</i> reports an error when user use a command without authentication.
     */
    byte ERR_USER_NOT_LOGGED = -6;
    /**
     * <i>ERR_USER_DOESNT_EXISTS</i> reports an error when user wants to log with a unknown nickname.
     */
    byte ERR_USER_DOESNT_EXISTS = -7;

    /**
     * <i>ERR_CLIENT_NOT_ACCESSIBLE</i> reports when client is not accessible.
     */
    byte ERR_CLIENT_NOT_ACCESSIBLE = -8;
    /**
     * <i>ERR_PSW_MISMATCH</i> reports an error about a wrong password.
     */
    byte ERR_PSW_MISMATCH = -10;

}
