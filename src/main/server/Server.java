package main.server;

import main.context.UniqueContext;
import main.frame.*;

import main.usersManager.ServerManager;
import main.usersManager.ServerMdpManager;
import main.usersManager.User;
import main.visitor.MdpServerVisitorImpl;
import main.visitor.NotConnectedVisitorImpl;
import main.visitor.ServerVisitorImpl;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.nio.channels.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a server.
 */
public class Server {

    static private final Logger logger = Logger.getLogger(Server.class.getName());

    public final ServerManager serverManager = new ServerManager();
    public final ServerMdpManager serverMdpManager = new ServerMdpManager();

    private final ServerSocketChannel serverSocketChannel;
    private final SocketChannel serverSocketChannelServerToServer;
    private final Selector selector;
    private SelectionKey uniqueKey;

    /**
     * Sends a connexion demanding frame to the MDP server.
     * It can be a frame for an authentication login or a guest login.
     * Registers the request in the MDP manager and the connexion demanding in the server manager.
     * Then, cancels the client key.
     * @param frame the frame we want to send to the MDP server
     * @param id the request token Id
     * @param user an object which represents the user
     * @param login the user nickname
     * @see User
     */
    public void addAuthCnxFrameToQueue(Frame frame, long id, User user, String login){
        UniqueContext ctx = (UniqueContext) uniqueKey.attachment();
        ctx.addToQueue(frame);
        serverMdpManager.addNewMdpServerRequest(id, user);

        if(!serverManager.userAlreadyWaitingConnexion(user.getKey())){
            serverManager.addUserWaitingConnexion(user.getKey(), login);
        }

        //Cancel key for blocking receive messages
        user.getKey().cancel();
    }

    /**
     * Receives the MDP server response for the request associate to the tokenId <i>id</i>.
     * Registers the Socket Channel and create a new Context of type CONNECTED_SERVER associate to the new SelectionKey.
     * Sends the response to the client. Removes the client from the waiting connexions list. Adds the client in the connexion list.
     * Removes the tokenId from the requests in the MDP manager.
     * @param frame the MDP server response
     * @param id the request token Id
     * @see UniqueContext.ContextType
     */
    public void addAckMDPServerFrameToQueue(Frame frame, Long id){
        if(!serverMdpManager.requestsAlreadyExists(id)) throw new IllegalStateException("Bad ID " + id);
        var clientKey = serverMdpManager.getUser(id).getKey();
        try {
            var sc = (SocketChannel) clientKey.channel();
            sc.configureBlocking(false);
            var newKey = sc.register(selector, SelectionKey.OP_READ);
            var visitor = new ServerVisitorImpl(this, logger, newKey);
            var context = new UniqueContext(newKey, UniqueContext.ContextType.CONNECTED_SERVER, visitor);
            newKey.attach(context);
            context.addToQueue(frame);

            if(!serverManager.userAlreadyWaitingConnexion(clientKey)) throw new IllegalStateException();
            //Get login of current key
            var login = serverManager.getLogin(clientKey);
            //Remove key from waiting connexion map
            serverManager.removeUserWaitingConnexion(clientKey);
            //Add key in connexion map
            if(!(frame instanceof ErrorFrame))
                serverManager.addNewConnexion(newKey, login);
            //Remove request from MDP server
            serverMdpManager.removeMdpServerRequest(id);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends an error frame to the client associate to the <i>clientKey</i>
     * @param frame the error frame
     * @param clientKey the client SelectionKey
     */
    public void sendAuthError(ErrorFrame frame, SelectionKey clientKey){
        UniqueContext ctx = (UniqueContext) clientKey.attachment();
        ctx.addToQueue(frame);
    }

    /**
     * Broadcasts a message to clients which are connected.
     * @param frame the frame contains the message
     * @param senderKey the sender SelectionKey
     */
    public void broadcastMessage(DiffMsgServerFrame frame, SelectionKey senderKey){
        for (SelectionKey key : selector.keys()) {
            var ctx = (UniqueContext) key.attachment();

            if (ctx == null) {
                continue;
            }

            if(ctx.getType() == UniqueContext.ContextType.CONNECTED_SERVER && senderKey != ctx.getKey()) {
                ctx.addToQueue(frame);
            }
        }
    }

    /**
     * Sends an refused private connexion frame to the client associate to the <i>requestPseudo</i>
     * @param frame the frame wanting to send
     * @param requestPseudo the client wanting to established a private connexion
     */
    public void sendRefusedPrivateConnexionAcknowledge(ClientToServerErrorFrame frame, String requestPseudo){
        //Verify dest user exists
        if(!serverManager.userExists(requestPseudo)){
            logger.log(Level.WARNING, requestPseudo + " is not connected");
            return;
        }
        //Get the selection key of the requestPseudo
        var requestPseudoKey = serverManager.searchConnectedUser(requestPseudo);
        UniqueContext ctx = (UniqueContext) requestPseudoKey.attachment();
        ctx.addToQueue(frame);
    }

    /**
     * Sends an accepted private connexion frame to the client associate to the <i>requestPseudo</i>
     * @param frame the frame wanting to send
     * @param requestPseudo the client wanting to established a private connexion
     */
    public void sendAcceptedPrivateConnexionAcknowledge(AckPrivateConnexionFrame frame, String requestPseudo){
        //Verify dest user exists
        if(!serverManager.userExists(requestPseudo)){
            logger.log(Level.WARNING, requestPseudo + " is not connected");
            return;
        }
        //Get the selection key of the requestPseudo
        var requestPseudoKey = serverManager.searchConnectedUser(requestPseudo);
        UniqueContext ctx = (UniqueContext) requestPseudoKey.attachment();
        ctx.addToQueue(frame);
    }

    private void sendClientNotFound(ClientNotFoundFrame frame, SelectionKey key){
        UniqueContext ctx = (UniqueContext) key.attachment();
        ctx.addToQueue(frame);
    }

    /**
     * Sends a private connexion demanding from the user associate to the SelectionKey <i>requestKey</i> to the client <i>dest</i>.
     * @param frame the frame to send
     * @param dest the client dest
     * @param requestKey the client we want a private connexion with the client dest
     */
    public void addAskPrivateConnexionToQueue(ServerForwardPrivateConnexionDemandingFrame frame, String dest, SelectionKey requestKey){
        //Verify dest user exists
        if(!serverManager.userExists(dest)){
            logger.log(Level.WARNING, dest + " is not connected");
            sendClientNotFound(new ClientNotFoundFrame(dest), requestKey);
            return;
        }
        //Get the selection key of the dest
        var destKey = serverManager.searchConnectedUser(dest);
        var ctx = (UniqueContext) destKey.attachment();
        ctx.addToQueue(frame);
    }

    /**
     * Creates a new Server Object.
     * @param port the port used to connect the client to the server.
     * @throws IOException see IOException from the ServerSocketChannel bind method
     * @see ServerSocketChannel
     */
    public Server(int port) throws IOException {
        serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.bind(new InetSocketAddress(port));
        serverSocketChannelServerToServer = SocketChannel.open();
        selector = Selector.open();
    }


    private void launch() throws IOException {
        /*Connexion client to server */
        serverSocketChannel.configureBlocking(false);
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

        /*Connexion server to MDP server*/
        serverSocketChannelServerToServer.configureBlocking(false);
        serverSocketChannelServerToServer.connect(new InetSocketAddress("localhost", 4545));
        uniqueKey = serverSocketChannelServerToServer.register(selector, SelectionKey.OP_CONNECT);
        var visitor = new MdpServerVisitorImpl(this);
        var context = new UniqueContext(uniqueKey, UniqueContext.ContextType.MDP_SERVER, visitor);
        uniqueKey.attach(context);

        while (!Thread.interrupted()) {
            try {
                selector.select(this::treatKey);
            } catch (UncheckedIOException tunneled) {
                throw tunneled.getCause();
            }
        }

    }

    private void treatKey(SelectionKey key) {
        try {
            if(!key.isValid()){
                serverManager.clearAllDataForUser(key);
                key.cancel();
            }
            if (key.isValid() && key.isAcceptable()) {
                doAccept(key);
            }
        } catch (IOException ioe) {
            // lambda call in select requires to tunnel IOException
            throw new UncheckedIOException(ioe);
        }

        try {
            if(key.isValid() && key.isConnectable()){
                ((UniqueContext) key.attachment()).doConnect();
            }
            if (key.isValid() && key.isWritable()) {
                ((UniqueContext) key.attachment()).doWrite();
            }
            if (key.isValid() && key.isReadable()) {
                ((UniqueContext) key.attachment()).doRead();
            }
        } catch (IOException | InterruptedException e) {
            logger.log(Level.INFO, "Connection closed with client due to IOException", e);
            silentlyClose(key);
        }
    }

    private void doAccept(SelectionKey key) throws IOException {
        SocketChannel sc = serverSocketChannel.accept();

        if (sc != null) {
            sc.configureBlocking(false);
            SelectionKey clientKey = sc.register(selector, SelectionKey.OP_READ);
            var visitor = new NotConnectedVisitorImpl(this, logger, clientKey);
            var context = new UniqueContext(clientKey, UniqueContext.ContextType.NOT_CONNECTED_SERVER, visitor);
            clientKey.attach(context);
        }
    }

    private void silentlyClose(SelectionKey key) {
        Channel sc = key.channel();
        try {
            sc.close();
        } catch (IOException e) {
            // ignore exception
        }
    }

    public static void main(String[] args) throws NumberFormatException, IOException {
        if (args.length != 1) {
            usage();
            return;
        }

        var sv = new Server(Integer.parseInt(args[0]));
        sv.launch();
    }

    private static void usage() {
        System.out.println("Usage : Server port");
    }

    /**
     * Check if the MDP server is accessible.
     * @return true if the MDP server is accessible. False else.
     */
    public boolean mdpServerIsAccessible() {
        return uniqueKey.isValid();
    }
}
