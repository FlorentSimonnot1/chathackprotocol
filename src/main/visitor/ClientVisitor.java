package main.visitor;

import main.frame.*;

/**
 * ClientVisitor implements methods to visit a frame from client.
 * It used from a public context
 */
public interface ClientVisitor extends Visitor {

    /**
     * Do action when the client received an error frame
     * @param frame an error frame
     */
    void visitError(ErrorFrame frame);

    /**
     * Do action when the client received a acknowledge frame
     * @param frame an acknowledge frame
     */
    void visitAck(AckFrame frame);

    /**
     * Do action when the client received a broadcast message
     * @param frame a broadcast message frame
     */
    void visitReceiveBroadcastMessage(DiffMsgServerFrame frame);

    /**
     * Do action when client received a private connexion request from the server
     * @param frame a private connexion request frame from the server
     */
    void visitReceivePrivateConnexionDemanding(ServerForwardPrivateConnexionDemandingFrame frame);

    /**
     * Do action when the client received a negative response to a private connexion transmit by the server
     * @param frame an error private connexion frame
     */
    void visitPrivateConnexionRequestRefused(ClientToServerErrorFrame frame);

    /**
     * Do action when the client received a positive response to a private connexion transmit by the server
     * @param frame a positive acknowledge for a private connexion
     */
    void visitPrivateConnexionRequestAccepted(AckPrivateConnexionFrame frame);

    /**
     * Do action when the client received a clientNotFoundError from the server.
     * @param frame a clientNotFoundError frame
     */
    void visitClientNotFound(ClientNotFoundFrame frame);
}
