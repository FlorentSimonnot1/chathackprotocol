package main.visitor;

import main.frame.AckMDPServerFrame;
import main.frame.ErrorFrame;
import main.frame.Frame;
import main.context.ClosedFunctionalInterface;
import main.frame.AckFrame;
import main.opcodes.Opcodes;
import main.server.Server;

import java.io.IOException;

/**
 * Class which implements the MdpServerVisitor
 */
public class MdpServerVisitorImpl implements MdpServerVisitor {
    private final Server server;

    @Override
    public void accept(Frame frame) throws IOException, InterruptedException {
        frame.accept(this);
    }

    public MdpServerVisitorImpl(Server server) {
        this.server = server;
    }

    @Override
    public void setClosedMethod(ClosedFunctionalInterface closedMethod) { }

    @Override
    public void visitAckMDPServer(AckMDPServerFrame frame) {
        var opcode = frame.getOpcode();
        var id = frame.getId();

        Frame response;
        //Server MDP said login/password is valid
        if(opcode == 1 && !server.serverMdpManager.getUser(id).isGuestConnexion()){
            response = new AckFrame(Opcodes.ACK_SV_CNX_ESTABLISHED);
        }
        //Server MDP said login doesn't exists in BD
        else if(opcode == 0 && server.serverMdpManager.getUser(id).isGuestConnexion()){
            response = new AckFrame(Opcodes.ACK_SV_CNX_ESTABLISHED);
        }
        else{
            //Connection failed. Remove client from users
            response = new ErrorFrame(Opcodes.ERR_SV_CNX, "Please verify your login or password");
        }
        server.addAckMDPServerFrameToQueue(response, id);
    }

}
