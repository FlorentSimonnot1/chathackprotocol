package main.visitor;

import main.frame.*;
import main.context.ClosedFunctionalInterface;
import main.opcodes.Opcodes;
import main.server.Server;
import main.usersManager.User;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.util.logging.Logger;

/**
 * Class which implements the the NotConnectedVisitor.
 */
public class NotConnectedVisitorImpl implements NotConnectedVisitor{
    private final Server server;
    private final SelectionKey key;
    private final Logger logger;
    private ClosedFunctionalInterface closedMethod;

    @Override
    public void accept(Frame frame) throws IOException, InterruptedException {
        frame.accept(this);
    }

    public NotConnectedVisitorImpl(Server server, Logger logger, SelectionKey key) {
        this.server = server;
        this.logger = logger;
        this.key = key;
    }

    @Override
    public void setClosedMethod(ClosedFunctionalInterface closedMethod) {
        this.closedMethod = closedMethod;
    }

    @Override
    public void visitLoginWithCredentials(LoginWithCredentialsFrame frame){
        var login = frame.getLogin();
        var password = frame.getPassword();
        var user = new User(key, false);

        //Verify the MDP server is accessible
        if(!server.mdpServerIsAccessible()){
            logger.info("Server can't accept connexion due to internal error");
            server.sendAuthError(new ErrorFrame(Opcodes.ERR_SV_CNX, "Impossible to established a connexion with the server due to an internal error "), key);
            return;
        }

        //Check pseudo already exists in map
        if(server.serverManager.userExists(frame.getLogin())){
            var f = new ErrorFrame(Opcodes.ERR_NICKNAME_ALREADY_USED, "Sorry " + login + " already used");
            server.sendAuthError(f, key);
            return;
        }

        logger.info(login + " wanting to connect with password");
        var f = new AskLoginWithCredentialsServerToServerFrame(Opcodes.ASK_CREDENTIALS_OR_SERVER_MDP_SUCCESS, login, password);
        server.addAuthCnxFrameToQueue(f, f.getId(), user, login);
    }

    @Override
    public void visitLoginWithoutCredentials(LoginWithoutCredentialsFrame frame) {
        var login = frame.getLogin();

        //Verify the MDP server is accessible
        if(!server.mdpServerIsAccessible()){
            logger.info("Server can't accept connexion due to internal error");
            server.sendAuthError(new ErrorFrame(Opcodes.ERR_SV_CNX, "Impossible to established a connexion with the server due to an internal error "), key);
            return;
        }

        //Check pseudo already exists in map
        if(server.serverManager.userExists(frame.getLogin())){
            var f = new ErrorFrame(Opcodes.ERR_NICKNAME_ALREADY_USED, "Sorry " + login + " already used");
            server.sendAuthError(f, key);
            return;
        }

        var user = new User(this.key, true);
        var f = new AskLoginWithoutCredentialsServerToServerFrame(Opcodes.ASK_GUEST_LOGIN_MDP_SERVER, login);
        server.addAuthCnxFrameToQueue(f, f.getId(), user, login);
    }

    @Override
    public void visitDefault(Frame frame) {
        logger.info("You are not connected");
        closedMethod.apply();
    }
}
