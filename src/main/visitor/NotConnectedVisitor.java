package main.visitor;

import main.frame.Frame;
import main.frame.LoginWithCredentialsFrame;
import main.frame.LoginWithoutCredentialsFrame;

/**
 * NotConnectedVisitor implements methods to visit a frame from client.
 * It used from a NotConnectedContext in Server
 */
public interface NotConnectedVisitor extends Visitor {

    /**
     * Do action when server receive a Authentication Login frame
     * @param frame a LoginWithCredentialsFrame
     */
    void visitLoginWithCredentials(LoginWithCredentialsFrame frame);

    /**
     * Do action when server receive a Guest Login frame
     * @param frame a LoginWithoutCredentialsFrame
     */
    void visitLoginWithoutCredentials(LoginWithoutCredentialsFrame frame);

    /**
     * Do action when the server receive a frame which not passed in one of the two previously methods. This apparently to an error.
     * @param frame a frame
     */
    void visitDefault(Frame frame);

}
