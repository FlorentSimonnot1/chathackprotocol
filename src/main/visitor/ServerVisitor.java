package main.visitor;

import main.frame.*;

/**
 * ServerVisitor implements methods to visit a frame from the MDP Server.
 * It used from a ServerContext.
 */
public interface ServerVisitor extends Visitor {

    /**
     * Do action when server received a broadcast message from a client
     * @param frame a broadcast message frame
     * @see DiffMsgClientFrame
     */
    void visitBroadcastMessageFromClient(DiffMsgClientFrame frame);

    /**
     * Do action when server received a private connexion request
     * @param frame a private connexion request frame
     * @see ClientToClientFrame
     */
    void visitPrivateConnexionDemanding(ClientToClientFrame frame);

    /**
     * Do action when server received a negative response from a client which have received a private connexion request
     * @param frame a negative response from a private connexion request
     * @see ClientToServerErrorFrame
     */
    void visitPrivateConnexionRefused(ClientToServerErrorFrame frame);

    /**
     * Do action when server received a positive response from a client which have received a private connexion request
     * @param frame an acknowledge private connexion frame
     * @see AckPrivateConnexionFrame
     */
    void visitPrivateConnexionAccepted(AckPrivateConnexionFrame frame);

    /**
     * Do action when server received a frame from a client which want to disconnect it
     * @param frame an acknowledge frame
     * @see AckFrame
     */
    void visitDisconnection(AckFrame frame);
}
