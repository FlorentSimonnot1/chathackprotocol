package main.visitor;

import main.frame.*;
import main.client.Client;
import main.context.ClosedFunctionalInterface;
import main.opcodes.Opcodes;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.util.logging.Logger;

/**
 * Class which implements the PrivateClientVisitor.
 */
public class PrivateClientVisitorImpl implements PrivateClientVisitor {
    private final Client client;
    private final Logger logger;
    private final SelectionKey key;
    private ClosedFunctionalInterface closedMethod;

    @Override
    public void accept(Frame frame){
        frame.accept(this);
    }

    public PrivateClientVisitorImpl(Client client, Logger logger, SelectionKey key) {
        this.client = client;
        this.logger = logger;
        this.key = key;
    }

    @Override
    public void setClosedMethod(ClosedFunctionalInterface closedMethod) {
        this.closedMethod = closedMethod;
    }

    @Override
    public void visitInitiateCnx(InitiateCnxFrame frame) {
        var token = frame.getTokenID();
        if(!client.privateConnexionDest.waitingConnexionExists(token)){
            client.sendFrameToPrivateContext(this.key, new ErrorFrame(Opcodes.ERR_PV_CNX, " you haven't permissions !"));
            /*Add close connexion*/
            return;
        }
        var interlocutor = client.privateConnexionDest.getInterlocutorOfDiscussion(token);
        client.privateConnexionDest.removePrivateConnexionWaiting(token);
        client.privateConnexionDest.registerInterlocutorKey(this.key, interlocutor);
        logger.info("Private connexion with " + interlocutor + " is established !");
        client.sendFrameToPrivateContext(this.key, new AckCnxFrame(token));
    }

    @Override
    public void visitAckCnx(AckCnxFrame frame) {
        var token = frame.getTokenId();
        var interlocutor = client.privateConnexionSrc.searchInterlocutor(token);
        logger.info("You've just establish a new private connexion with " + interlocutor);
        client.privateConnexionSrc.registerInterlocutorKey(interlocutor, this.key);
    }

    @Override
    public void visitReceivePrivateMsg(PrivateMsgFrame frame) {
        if(client.privateConnexionSrc.privateConnexionAlreadyExists(key)){
            var sender = client.privateConnexionSrc.searchInterlocutor(key);
            var message = frame.getMessage();
            System.out.println(sender + " send a private message : " + message);
        }
        else if(client.privateConnexionDest.keyExists(key)){
            var sender = client.privateConnexionDest.getInterlocutorOfDiscussion(key);
            var message = frame.getMessage();
            System.out.println(sender + " send a private message : " + message);
        }
        else{
            throw new IllegalStateException("Oups");
        }

    }

    @Override
    public void visitReceiveFile(SendFileFrame frame) {
        var fileName = frame.getName();
        if(client.privateConnexionSrc.privateConnexionAlreadyExists(key)){
            var sender = client.privateConnexionSrc.searchInterlocutor(key);
            System.out.println(sender + " send a file : " + fileName);
        }
        else if(client.privateConnexionDest.keyExists(key)){
            var sender = client.privateConnexionDest.getInterlocutorOfDiscussion(key);
            System.out.println(sender + " send a file : " + fileName);
        }
        try {
            Utils.writeFile(client.getLogin(), fileName, frame.getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void visitDisconnection(AckFrame frame) {
        //if I am the client/client
        logger.info("disconnection demanding");
        if(client.privateConnexionDest.keyExists(key)){
            var dest = client.privateConnexionDest.getInterlocutorOfDiscussion(key);
            client.privateConnexionDest.clearAfterDisconnection(key);
            logger.info("You have been disconnected with " + dest);
        }
        if(client.privateConnexionSrc.keyExists(key)){
            var dest = client.privateConnexionSrc.searchInterlocutor(key);
            client.privateConnexionSrc.clearAfterDisconnection(dest);
            logger.info("You have been disconnected with " + dest);
        }
        closedMethod.apply();
    }
}
