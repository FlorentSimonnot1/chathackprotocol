package main.visitor;

import main.frame.Frame;
import main.context.ClosedFunctionalInterface;

import java.io.IOException;

/**
 * Visitor Interface implements a method used by all visitors.
 */
public interface Visitor {

    /**
     * Change the ClosedFunctionalInterface method of the visitor.
     * @param closedMethod a method to close the context from the visitor.
     * @see ClosedFunctionalInterface
     */
    void setClosedMethod(ClosedFunctionalInterface closedMethod);

    /**
     * Takes a frame and called the frame accept method in according to the visitor
     * @param frame a Frame
     * @throws IOException when clientVisitor is called
     * @throws InterruptedException when clientVisitor is called
     */
    void accept(Frame frame) throws IOException, InterruptedException;

}
