package main.visitor;

import main.frame.*;
import main.context.ClosedFunctionalInterface;
import main.opcodes.Opcodes;
import main.server.Server;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.util.logging.Logger;

/**
 * Class which implements the ServerVisitor.
 */
public class ServerVisitorImpl implements ServerVisitor{
    private final Server server;
    private final Logger logger;
    private final SelectionKey key;
    private ClosedFunctionalInterface closedMethod;



    public ServerVisitorImpl(Server server, Logger logger, SelectionKey key) {
        this.server = server;
        this.logger = logger;
        this.key = key;
    }

    @Override
    public void accept(Frame frame) {
        frame.accept(this);
    }

    @Override
    public void setClosedMethod(ClosedFunctionalInterface closedMethod) {
        this.closedMethod = closedMethod;
    }

    @Override
    public void visitPrivateConnexionDemanding(ClientToClientFrame frame) {
        var dest = frame.getPseudo();
        var sender = server.serverManager.getLoginConnectedUser(this.key);

        if(sender.equals(dest)){
            server.sendAuthError(new ErrorFrame(Opcodes.ERR_CLIENT_NOT_ACCESSIBLE, "Can't create a private connexion with yourself"), this.key);
            return;
        }

        server.serverManager.addPrivateConnexionRequest(sender, dest);
        server.addAskPrivateConnexionToQueue(new ServerForwardPrivateConnexionDemandingFrame(sender), dest, this.key);
    }

    @Override
    public void visitPrivateConnexionRefused(ClientToServerErrorFrame frame) {
        var pseudoRequest = frame.getPseudo();
        var pseudoDest = server.serverManager.getLoginConnectedUser(this.key);
        //Skip if pseudo never asked for a private connexion
        if(!server.serverManager.privateConnexionRequestExists(pseudoRequest, pseudoDest)){
            logger.info("No private connexion between " + pseudoRequest + " and " + pseudoDest + " asked");
            return;
        }
        frame.setPseudo(pseudoDest);
        server.sendRefusedPrivateConnexionAcknowledge(frame, pseudoRequest);
        server.serverManager.removePrivateConnexionRequest(pseudoRequest, pseudoDest);
    }

    @Override
    public void visitBroadcastMessageFromClient(DiffMsgClientFrame frame) {
        var message = frame.getMessage();
        if(!server.serverManager.userAlreadyConnected(this.key)) throw new IllegalStateException("Key is not registered");
        var pseudo = server.serverManager.getLoginConnectedUser(this.key);
        var f = new DiffMsgServerFrame(pseudo, message);
        server.broadcastMessage(f, this.key);
    }

    @Override
    public void visitPrivateConnexionAccepted(AckPrivateConnexionFrame frame) {
        var pseudoRequest = frame.getPseudo();
        var pseudoDest = server.serverManager.getLoginConnectedUser(this.key);
        //Skip if pseudo never asked for a private connexion
        if(!server.serverManager.privateConnexionRequestExists(pseudoRequest,  pseudoDest)){
            logger.info("No private connexion between " + pseudoRequest + " and " + pseudoDest + " asked");
            return;
        }
        frame.setPseudo(pseudoDest);
        server.sendAcceptedPrivateConnexionAcknowledge(frame, pseudoRequest);
        server.serverManager.removePrivateConnexionRequest(pseudoRequest, pseudoDest);
    }

    @Override
    public void visitDisconnection(AckFrame frame) {
        logger.info("Disconnection is demanding");
        if(frame.getOpcode() != Opcodes.SQUIT) throw new IllegalStateException();
        closedMethod.apply();
        //server.serverManager.removeConnexion(this.key);
        server.serverManager.clearAllDataForUser(this.key);
    }
}
