package main.visitor;

import main.frame.*;
import main.client.Client;
import main.context.ClosedFunctionalInterface;
import main.opcodes.Opcodes;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class which implements the ClientVisitor.
 */
public class ClientVisitorImpl implements ClientVisitor{
    private final Client client;
    private final Logger logger;
    private ClosedFunctionalInterface closedMethod;

    public ClientVisitorImpl(Client client, Logger logger) {
        this.client = client;
        this.logger = logger;
    }

    @Override
    public void accept(Frame frame) throws IOException, InterruptedException {
        frame.accept(this);
    }

    /**
     * Get the client of this visitor
     * @return - the client used in this visitor
     * @see Client
     */
    public Client getClient() {
        return client;
    }

    @Override
    public void setClosedMethod(ClosedFunctionalInterface closedMethod) {
        this.closedMethod = closedMethod;
    }

    @Override
    public void visitError(ErrorFrame frame) {
        if(frame.getOpcode() == Opcodes.ERR_SV_CNX){
            logger.log(Level.WARNING, frame.getMsg());
            closedMethod.apply();
        }
        if(frame.getOpcode() == Opcodes.ERR_NICKNAME_ALREADY_USED){
            logger.log(Level.WARNING, frame.getMsg());
            closedMethod.apply();
        }
    }

    @Override
    public void visitAck(AckFrame frame) {
        if(frame.getOpcode() == Opcodes.ACK_SV_CNX_ESTABLISHED){
            logger.info("Your are connected !");
        }
    }

    @Override
    public void visitReceiveBroadcastMessage(DiffMsgServerFrame frame) {
        var sender = frame.getPseudo();
        var message = frame.getMessage();
        System.out.println(sender + " send a broadcast message : " + message);
    }

    @Override
    public void visitReceivePrivateConnexionDemanding(ServerForwardPrivateConnexionDemandingFrame frame){
        var sender = frame.getSender();
        if(!client.privateConnexionDest.invitationAlreadyExists(sender)){
            client.privateConnexionDest.addNewPrivateConnexionInvitation(sender);
        }
        System.out.println(sender + " wants to established a private connexion. Yo can answer with command $"+sender + " Y  or  $"+sender+ " N");
    }

    @Override
    public void visitPrivateConnexionRequestRefused(ClientToServerErrorFrame frame) {
        var interlocutor = frame.getPseudo();
        client.privateConnexionSrc.removePrivateConnexionRequest(interlocutor);
        logger.info(interlocutor + " refused a private connexion");
    }

    @Override
    public void visitPrivateConnexionRequestAccepted(AckPrivateConnexionFrame frame){
        var interlocutor = frame.getPseudo();
        var address = frame.getAddress();
        var token = frame.getToken();
        logger.info(interlocutor + " have accepted a private request");
        client.createPrivateConnexion(interlocutor, address.getAddress(), address.getPort(), token);
    }

    @Override
    public void visitClientNotFound(ClientNotFoundFrame frame) {
        var dest = frame.getDest();
        logger.info("Your private connexion request failed. " + dest + " is not a client.");
        client.privateConnexionSrc.removePrivateConnexionRequest(dest);
    }
}
