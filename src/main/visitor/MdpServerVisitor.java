package main.visitor;

import main.frame.AckMDPServerFrame;

/**
 * MdpServerVisitor implements methods to visit a frame from the MDP Server.
 * It used from a MDPServer context.
 */
public interface MdpServerVisitor extends Visitor {

    /**
     * Do action when server receive a response from the MDP Server.
     * @param frame - the response as a frame of the MDP server
     */
    void visitAckMDPServer(AckMDPServerFrame frame);
}
