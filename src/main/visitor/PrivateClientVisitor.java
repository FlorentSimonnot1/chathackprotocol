package main.visitor;

import main.frame.*;
import main.opcodes.Opcodes;

/**
 * PrivateClientVisitor implements methods to visit a frame from client.
 * It used from a PrivateContext in the client
 */
public interface PrivateClientVisitor extends Visitor{
	/**
	 * Do action when the client received a private message
	 * @param frame a private message frame
	 * @see PrivateMsgFrame
	 */
	void visitReceivePrivateMsg(PrivateMsgFrame frame);

	/**
	 * Do action when the client received a file in a private connexion
	 * @param frame a send file frame
	 * @see SendFileFrame
	 */
	void visitReceiveFile(SendFileFrame frame);

	/**
	 * Do action when client received a private connexion frame
	 * @param frame an initiateConnexionFrame
	 * @see InitiateCnxFrame
	 */
	void visitInitiateCnx(InitiateCnxFrame frame);

	/**
	 * Do action when client received an acknowledge of a private connexion establishment
	 * @param frame an Ack connexion frame
	 * @see AckCnxFrame
	 */
	void visitAckCnx(AckCnxFrame frame);

	/**
	 * Do action when client received a disconnection frame for a private connexion
	 * @param frame an Ack frame which contains one opcode which corresponds to the private disconnection opcode
	 * @see Opcodes
	 * @see AckFrame
	 */
	void visitDisconnection(AckFrame frame);
}
