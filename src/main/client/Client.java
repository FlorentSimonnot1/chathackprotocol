package main.client;

import main.context.Context;
import main.context.UniqueContext;
import main.frame.*;
import main.opcodes.Opcodes;
import main.frame.Frame;
import main.frame.address.AddressFrame;
import main.frame.address.Ipv4Frame;
import main.frame.address.Ipv6Frame;
import main.usersManager.PrivateConnexionDest;
import main.usersManager.PrivateConnexionSrc;
import main.visitor.ClientVisitorImpl;
import main.visitor.PrivateClientVisitorImpl;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a client which use the ChatHackProtocol defined in the RFC.
 */
public class Client{
    private static final Logger logger = Logger.getLogger(Client.class.getName());

    private final SocketChannel sc;
    private final Selector selector;
    private SelectionKey uniqueKey;

    private final BlockingQueue<Frame> consoleQueue = new LinkedBlockingQueue<>();
    private final ReentrantLock lock = new ReentrantLock();
    //Active when external server close the context.
    private boolean errorOccurred = false;

    /* -------------------------- *\
                USER
    \* -------------------------- */
    private final String login;
    private final Optional<String> password;

    /**
     * Get the client login.
     * @return the client login as a String.
     */
    public String getLogin() {
        return login;
    }

    /* -------------------------- *\
            USERS COLLECTIONS
    \* -------------------------- */
    private final Map<String, List<Frame>> messageWaiting = new HashMap<>();
    private final Map<String, List<Frame>> fileWaiting = new HashMap<>();

    /**
     * Manager for the private connexion. Used by the client we have asked a private connexion
     * @see PrivateConnexionSrc
     */
    public final PrivateConnexionSrc privateConnexionSrc = new PrivateConnexionSrc();
    /**
     * Manager for the private connexion. Used by the client we have accepted a private connexion
     * @see PrivateConnexionDest
     */
    public final PrivateConnexionDest privateConnexionDest = new PrivateConnexionDest();

    /* ------------------------- *\
          CLIENT BECOME SERVER
    \* ------------------------- */
    private final ServerSocketChannel serverSocketChannel;
    private final SocketAddress serverSocketAddress;

    /* -------------------------- *\
          PRIVATE CONSTRUCTORS
    \* -------------------------- */
    private Client(SocketAddress socketAddress, String login) throws IOException {
        this.sc = SocketChannel.open(socketAddress);
        this.selector = Selector.open();
        this.login = login;
        this.password = Optional.empty();
        /*configure server socket*/
        this.serverSocketChannel = ServerSocketChannel.open();
        this.serverSocketAddress = new InetSocketAddress(getFreePort());
        this.serverSocketChannel.bind(serverSocketAddress);
    }

    private Client(SocketAddress socketAddress, String login, String password) throws IOException {
        this.sc = SocketChannel.open(socketAddress);
        this.selector = Selector.open();
        this.login = login;
        this.password = Optional.of(password);
        /*configure server socket*/
        this.serverSocketChannel = ServerSocketChannel.open();
        this.serverSocketAddress = new InetSocketAddress(getFreePort());
        this.serverSocketChannel.bind(serverSocketAddress);
    }

    /* -------------------------- *\
            STATIC FACTORY
    \* -------------------------- */

    /**
     * Creates a client as guest mode.
     * @param socketAddress the client socketAddress
     * @param login the client login
     * @return a client
     * @throws IOException due to an IOException when used open method from ServerSocketChannel.
     * @see ServerSocketChannel
     */
    public static Client createGuestClient(SocketAddress socketAddress, String login) throws IOException {
        checkLength(login, "Your login is too long");
        return new Client(socketAddress, login);
    }

    /**
     *
     * Creates a client as authentication mode.
     * @param socketAddress the client socketAddress
     * @param login the client login
     * @param password the client password
     * @return a client
     * @throws IOException due to an IOException when used open method from ServerSocketChannel. `
     * @see ServerSocketChannel
     */
    public static Client createAuthClient(SocketAddress socketAddress, String login, String password) throws IOException{
        checkLength(login, "Error : Your login is too long !");
        checkLength(password, "Error : Your password is too long !");
        return new Client(socketAddress, login, password);
    }

    /* -------------------------- *\
            CHECK LOGIN/PSW
    \* -------------------------- */
    private static void checkLength(String string, String errorMessage){
        if(string.length() > 50) {
            System.out.println(errorMessage);
            System.exit(1);
        }
    }

    private int getFreePort() throws IOException {
        //Get a free port when 0 passed
        try (ServerSocket socket = new ServerSocket(0)) {
            socket.setReuseAddress(true);
            return socket.getLocalPort();
        }
    }

    private Thread createConsole(){
        return new Thread(() -> {
            try {
                sendFrame();
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        });
    }

    private void executeConsole(){

        if(consoleQueue.isEmpty()) return;
        var frame = consoleQueue.poll();
        ((UniqueContext) uniqueKey.attachment()).addToQueue(frame);

    }

    private void launch() throws IOException {
        Thread thread = createConsole();

        /* ---------------- *\
          CONNEXION TO SERVER
        \* ---------------- */
        ByteBuffer bb;
        if(password.isPresent()){
            bb  = new LoginWithCredentialsFrame(login, password.get()).bufferize();
        }else{
            bb = new LoginWithoutCredentialsFrame(login).bufferize();
        }
        sc.write(bb);

        /* ---------------- *\
          INIT CLIENT SOCKET
        \* ---------------- */
        sc.configureBlocking(false);
        uniqueKey = sc.register(selector, SelectionKey.OP_WRITE | SelectionKey.OP_READ);
        var visitor = new ClientVisitorImpl(this, logger);
        var context = new UniqueContext(uniqueKey, UniqueContext.ContextType.PUBLIC_CLIENT, visitor);
        uniqueKey.attach(context);

        /* ---------------- *\
          INIT SERVER SOCKET
        \* ---------------- */
        serverSocketChannel.configureBlocking(false);
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

        thread.start();

        //Handle a CTRL^C
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                System.out.println("Shutting down ...");
                //Disconnection from outside
                if(errorOccurred){
                    System.out.println("You have been disconnected !");
                    return;
                }
                //Some cleaning up code...
                //Clear privates conversations, send disconnection frame to server and clients which share a private connection.
                //Send disconnection requests for all users.
                privateConnexionDest.forEach((key, user) -> sendFrameToPrivateContext(key, new AckFrame(Opcodes.CQUIT)));
                privateConnexionSrc.forEach((user, key) -> sendFrameToPrivateContext(key, new AckFrame(Opcodes.CQUIT)));
                sc.write((new AckFrame(Opcodes.SQUIT)).bufferize());
                sc.close();
                Thread.sleep(200);
            } catch (InterruptedException | IOException e) {
                Thread.currentThread().interrupt();
                e.printStackTrace();
            }
        }));

        while (!Thread.interrupted()) {
            selector.select(this::treatKey);
            executeConsole();
        }
    }

    private void treatKey(SelectionKey key) {
        try {
            if (key.isValid() && key.isAcceptable()) {
                doAccept();
            }
        } catch (IOException ioe) {
            // lambda call in select requires to tunnel IOException
            throw new UncheckedIOException(ioe);
        }
        try {
            if(key.isValid() && key.isConnectable()){
                ((Context) key.attachment()).doConnect();
            }
            if (key.isValid() && key.isWritable()) {
                ((Context) key.attachment()).doWrite();
            }
            if (key.isValid() && key.isReadable()) {
                ((Context) key.attachment()).doRead();
            }
        } catch (IOException | InterruptedException e) {
            logger.log(Level.INFO, "Connection closed with client due to IOException", e);
            close();
        }
    }

    private void doAccept() throws IOException {
        SocketChannel sc = serverSocketChannel.accept();

        if (sc != null) {
            sc.configureBlocking(false);
            SelectionKey clientKey = sc.register(selector, SelectionKey.OP_READ);
            logger.info("Connexion accepted " + clientKey);
            var visitor = new PrivateClientVisitorImpl(this, logger, clientKey);
            var context = new UniqueContext(clientKey, UniqueContext.ContextType.PRIVATE_CLIENT, visitor);
            clientKey.attach(context);
        }
    }

    /**
     * Shutdowns the client.
     */
    public void close() {
        try {
            sc.close();
            errorOccurred = true;
            System.exit(1);
        } catch (IOException ignored) {

        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        Client client;

        if(args.length == 3){
            client = Client.createGuestClient(
                    new InetSocketAddress(args[0], Integer.parseInt(args[1])),
                    args[2]
            );
        }
        else if(args.length == 4){
            client = Client.createAuthClient(
                    new InetSocketAddress(args[0], Integer.parseInt(args[1])),
                    args[2],
                    args[3]
            );
        }
        else{
            usage();
            return;
        }

        client.launch();
    }

    private void sendFrame() throws InterruptedException {
        while (!Thread.interrupted()) {
            try (Scanner scan = new Scanner(System.in)) {
                String line;
                while (scan.hasNextLine()) {
                    lock.lock();
                    try {
                        line = scan.nextLine();
                        MessageParsing.parsing(line, consoleQueue, this);
                        selector.wakeup();
                    }
                    finally {
                        lock.unlock();
                    }
                }
            }
        }
    }

    static class MessageParsing{

        private static Frame createPrivateConnexionResponse(String response, String login, Client client){
            if(response.equals("Y")){
                var address = ((InetSocketAddress) client.serverSocketAddress).getAddress().getAddress();
                var port = ((InetSocketAddress) client.serverSocketAddress).getPort();
                AddressFrame frame;
                if(address.length == 4){
                    frame = new Ipv4Frame(address, port);
                }
                else if(address.length == 16){
                    frame = new Ipv6Frame(address, port);
                }
                else{
                    throw new IllegalStateException("Bad IP version");
                }
                var token = System.currentTimeMillis();
                client.privateConnexionDest.removeInvitation(login);
                client.privateConnexionDest.putNewWaitingConnexion(token, login);

                return new AckPrivateConnexionFrame(login, frame, token);
            }
            else{
                return new ClientToServerErrorFrame(login);
            }
        }

        static void parsing(String message, BlockingQueue<Frame> queue, Client client) throws InterruptedException {
            switch(message.charAt(0)){
                case '$' :
                    var split$ = message.split(" ");
                    if(split$.length != 2){
                        System.out.println("Bad format for command : Response to private connexion request");
                        return;
                    }
                    var destLogin$ = split$[0].substring(1);
                    if(!client.privateConnexionDest.invitationAlreadyExists(destLogin$)) {
                        logger.info(destLogin$ + " not in the invitations list");
                        return;
                    }
                    if(split$[1].toUpperCase().equals("Y") || split$[1].toUpperCase().equals("N"))
                        queue.put(createPrivateConnexionResponse(split$[1].toUpperCase(), destLogin$, client));
                    else{
                        System.out.println("Bad format for command : Response to private connexion request");
                        return;
                    }
                    break;
                case '@' :
                    var split = message.split(" ");
                    if (split.length < 2) {
                        System.out.println("Bad format for command : Send private message");
                        return;
                    }
                    var destLogin = split[0].substring(1);
                    var splitMessage = message.substring(destLogin.length() + 2);
                    //Connexion already exists but this client is the client/server
                    if(client.privateConnexionDest.privateConnexionAlreadyExists(destLogin)){
                        var key = client.privateConnexionDest.getInterlocutorKey(destLogin);
                        client.sendFrameToPrivateContext(key, new PrivateMsgFrame(splitMessage));
                        return;
                    }
                    //No private connexion with the destLogin
                    if(client.privateConnexionSrc.userNotAlreadyRegister(destLogin)){
                        var list = new LinkedList<Frame>();
                        client.fileWaiting.put(destLogin, new LinkedList<>());
                        list.add(new PrivateMsgFrame(splitMessage));
                        client.messageWaiting.put(destLogin, list);
                        client.privateConnexionSrc.putNewPrivateConnexionRequest(destLogin);
                        queue.put(new ClientToClientFrame(destLogin));
                        return;
                    }
                    //Connexion already asked => add message to a queue
                    if(client.privateConnexionSrc.privateConnexionAlreadyAsked(destLogin)){
                        client.messageWaiting.get(destLogin).add(new PrivateMsgFrame(splitMessage));
                        return;
                    }
                    //Connexion already exists with another client
                    var key = client.privateConnexionSrc.getInterlocutorKey(destLogin);
                    client.sendFrameToPrivateContext(key, new PrivateMsgFrame(splitMessage));
                    break;
                case '/' :
                    var splitF = message.split(" ");
                    if (splitF.length != 2){
                        System.out.println("Bad format for the command : Send a file. ");
                        return;
                    }
                    var destLoginf = splitF[0].substring(1);
                    var path = message.substring(destLoginf.length() + 2);
                    var optional = Utils.createSendFileFrame(path);
                    //Encoding failed
                    if(optional.isEmpty()){
                        System.out.println("Impossible to send the file " + path);
                        return;
                    }
                    var file = optional.get();
                    //Connexion already exists but this client is the client/server
                    if(client.privateConnexionDest.privateConnexionAlreadyExists(destLoginf)){
                        var keyDF = client.privateConnexionDest.getInterlocutorKey(destLoginf);
                        client.sendFrameToPrivateContext(keyDF, file);
                        return;
                    }
                    //No private connexion with the destLogin
                    if(client.privateConnexionSrc.userNotAlreadyRegister(destLoginf)){
                        var list = new LinkedList<Frame>();
                        client.messageWaiting.put(destLoginf, new LinkedList<>());
                        list.add(file);
                        client.fileWaiting.put(destLoginf, list);
                        client.privateConnexionSrc.putNewPrivateConnexionRequest(destLoginf);
                        queue.put(new ClientToClientFrame(destLoginf));
                        return;
                    }
                    //Connexion already asked => add message to a queue
                    if(client.privateConnexionSrc.privateConnexionAlreadyAsked(destLoginf)){
                        client.fileWaiting.get(destLoginf).add(file);
                        return;
                    }
                    //Connexion already exists with another client
                    var keyF = client.privateConnexionSrc.getInterlocutorKey(destLoginf);
                    logger.info("file send " + path);
                    client.sendFrameToPrivateContext(keyF, file);
                    break;
                case '#' :
                    var splitd = message.split(" ");
                    //Ignore because of bad command
                    if(splitd.length > 1) {
                        System.out.println("verify your command for disconnection");
                        return;
                    }
                    //Private connexion disconnection
                    if(message.length() > 1){
                        var dest = message.substring(1);
                        //if client/server wants to disconnect
                        if(client.privateConnexionDest.privateConnexionAlreadyExists(dest)){
                            var keyDest = client.privateConnexionDest.getInterlocutorKey(dest);
                            client.sendFrameToPrivateContext(keyDest, new AckFrame(Opcodes.CQUIT));
                            logger.info("Disconnection with " + dest);
                            client.privateConnexionDest.clearAfterDisconnection(keyDest);
                        }
                        else if(client.privateConnexionSrc.privateConnexionAlreadyEstablished(dest)){
                            var keyDest = client.privateConnexionSrc.getInterlocutorKey(dest);
                            client.sendFrameToPrivateContext(keyDest, new AckFrame(Opcodes.CQUIT));
                            logger.info("Disconnection with " + dest);
                            client.privateConnexionSrc.clearAfterDisconnection(dest);
                        }
                        else{
                            logger.info("Can't disconnect to " + dest + " because there is any connexion");
                        }
                        return;
                    }
                    queue.put(new AckFrame(Opcodes.SQUIT));
                    break;
                default: queue.put(new DiffMsgClientFrame(message)); break;
            }
        }

    }

    /**
     * Sends a frame to the client associate to the <i>key</i>.
     * @param key the dest SelectionKey
     * @param frame the frame wanting to send
     */
    public void sendFrameToPrivateContext(SelectionKey key, Frame frame){
        UniqueContext ctx = (UniqueContext) key.attachment();
        ctx.addToQueue(frame);
    }

    private static void usage() {
        System.out.println("Usage 1 : socketAddress port login \nUsage 2 : socketAddress port login password ");
    }

    private void privateConnexion(SocketChannel scp, long id) throws IOException {
        scp.write(new InitiateCnxFrame(id).bufferize());
    }

    /**
     * Creates a private connexion. Connect this client to the interlocutor.
     * Adds the interlocutor in private connexion manager.
     * Sends messages and files that were sleeping while waiting for the connection.
     * @param pseudo the interlocutor nickname
     * @param address the interlocutor IP address
     * @param port the interlocutor port
     * @param token the private connexion token.
     */
    public void createPrivateConnexion(String pseudo, byte[] address, int port, long token){
        SocketChannel scp;
        try {
            var serverPrivateAddress = new InetSocketAddress(InetAddress.getByAddress(address), port);
            scp = SocketChannel.open(serverPrivateAddress);
            privateConnexion(scp, token);
            scp.configureBlocking(false);
            var keyPrivate = scp.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE | SelectionKey.OP_CONNECT);
            var visitor = new PrivateClientVisitorImpl(this, logger, keyPrivate);
            var context = new UniqueContext(keyPrivate, UniqueContext.ContextType.PRIVATE_CLIENT, visitor);
            keyPrivate.attach(context);

            //Associate token to pseudo
            privateConnexionSrc.putNewPrivateConnexion(pseudo, token);
            //Associate the client key to the client pseudo
            privateConnexionSrc.registerInterlocutorKey(pseudo, keyPrivate);

            //Send message/file to this client if messages/files waiting in the queue and clear the queues
            messageWaiting.get(pseudo).forEach(f -> sendFrameToPrivateContext(keyPrivate, f));
            fileWaiting.get(pseudo).forEach(f -> sendFrameToPrivateContext(keyPrivate, f));
            messageWaiting.get(pseudo).clear();
            fileWaiting.get(pseudo).clear();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
