package main.reader.frameReader;

import main.frame.AckFrame;
import main.reader.Reader;

/**
 * AckReader implements Reader interface.
 * This class is a reader which read an acknowledgement.
 */
public class AckReader implements Reader {

    private final byte opcode;

	/**
     * This method is the constructor of the class AckReader.
	 * @param opcode the opcode from the acknowledgement as a short.
	 */
    public AckReader(byte opcode) {
        this.opcode = opcode;
    }

	/**
	 * @return ProcessStatus a value from <i>ProcessStatus</i> enum.
	 */
    @Override
    public ProcessStatus process() {
        return ProcessStatus.DONE;
    }

    /**
     * Get an object which corresponds to the result of the reading
     * @return an object
     */
    @Override
    public Object get() {
        return new AckFrame(opcode);
    }

    @Override
    public void reset() {
        //Nothing
    }
}
