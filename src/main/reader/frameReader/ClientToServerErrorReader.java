package main.reader.frameReader;

import java.nio.ByteBuffer;
import java.util.Objects;

import main.frame.ClientToServerErrorFrame;
import main.reader.Reader;
import main.reader.simpleReader.StringReader;

/**
 * ClientToServerErrorReader implements Reader interface.
 * This class is a reader which read a frame when the server cannot log a Client.
 */
public class ClientToServerErrorReader implements Reader{

	/**
	 * Status enumeration used for the frame reader.
	 * Its correspond to status for the frame reading.
	 */
    private enum State {
        DONE, WAITING_LOGIN, ERROR
    };

    private State state = State.WAITING_LOGIN;

    private final ByteBuffer bb;
    private String pseudo;

	/**
     * This method is the constructor of the class ClientToServerErrorReader.
     * It allow us to extract the login from a ByteBuffer.
	 * @param bb a ByteBuffer that represent a frame.
	 */
    public ClientToServerErrorReader(ByteBuffer bb) {
        this.bb = Objects.requireNonNull(bb);
    }

	/**
	 * Extract the pseudo from the ByteBuffer.
	 * @return ProcessStatus a value from <i>ProcessStatus</i> enum.
	 */
    @Override
    public ProcessStatus process() {
        if (state == State.DONE || state == State.ERROR) {
            throw new IllegalStateException();
        }

        var stringReader = new StringReader(bb);
        ProcessStatus loginStatus = stringReader.process();

        if (loginStatus != ProcessStatus.DONE)
            return loginStatus;

        pseudo = stringReader.get();
        stringReader.reset();
        this.state = State.DONE;
        return ProcessStatus.DONE;
    }

    /**
     * Get an object which corresponds to the result of the reading
     * @return an object
     */
    @Override
    public Object get() {
        if(state != State.DONE){
            throw new IllegalStateException();
        }
        return new ClientToServerErrorFrame(pseudo);
    }

    /**
     *	Reset the state of the reader.
     */
    @Override
    public void reset() {
        this.state = State.WAITING_LOGIN;
    }

}
