package main.reader.frameReader;

import main.frame.ServerForwardPrivateConnexionDemandingFrame;
import main.reader.Reader;
import main.reader.simpleReader.StringReader;

import java.nio.ByteBuffer;

/**
 * ServerForwardPrivateConnexionDemandingReader implements Reader interface.
 * This class is a reader which read a frame when the server forward a request of private connection.
 */
public class ServerForwardPrivateConnexionDemandingReader implements Reader{

	/**
	 * Status enumeration used for the frame reader.
	 * Its correspond to status for the frame reading.
	 */
    private enum State {
        DONE, WAITING, ERROR
    };

    private State state = State.WAITING;

    private final StringReader stringReader;
    private String sender;

	/**
     * This method is the constructor of the class ServerForwardPrivateConnexionDemandingReader.
	 * @param bb a ByteBuffer that represent a frame.
	 */
    public ServerForwardPrivateConnexionDemandingReader(ByteBuffer bb) {
        this.stringReader = new StringReader(bb);
    }

	/**
	 * Extract the login from a ByteBuffer.
	 * @return ProcessStatus a value from <i>ProcessStatus</i> enum.
	 */
    @Override
    public ProcessStatus process() {
        if (state != State.WAITING) {
            throw new IllegalStateException();
        }

        ProcessStatus tokenStatus = stringReader.process();

        if (tokenStatus != ProcessStatus.DONE) {
            return tokenStatus;
        }
        sender = stringReader.get();
        stringReader.reset();
        
        this.state = State.DONE;
        return ProcessStatus.DONE;
    }

    /**
     * Get an object which corresponds to the result of the reading
     * @return an object
     */
    @Override
    public Object get() {
        if(state != State.DONE){
            throw new IllegalStateException();
        }
        return new ServerForwardPrivateConnexionDemandingFrame(sender);
    }

    /**
     *	Reset the state of the reader.
     */
    @Override
    public void reset() {
        this.state = State.WAITING;
        stringReader.reset();
    }

}
