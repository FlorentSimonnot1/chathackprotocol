package main.reader.frameReader;

import java.nio.ByteBuffer;

import main.frame.DiffMsgClientFrame;
import main.reader.Reader;
import main.reader.simpleReader.StringReader;

/**
 * DiffMsgClientReader implements Reader interface.
 * This class is a reader which read a frame when a client wants to send a broadcast message.
 */
public class DiffMsgClientReader implements Reader {

	/**
	 * Status enumeration used for the frame reader.
	 * Its correspond to status for the frame reading.
	 */
	private enum State {
        DONE, WAITING_MSG, ERROR
    };
	
    private State state = State.WAITING_MSG;
    
    private final StringReader stringReader;
    private String message;

	/**
     * This method is the constructor of the class DiffMsgClientReader.
     * It allow us to extract the broadcast message from a ByteBuffer.
	 * @param bb a ByteBuffer that represent a frame.
	 */
    public DiffMsgClientReader(ByteBuffer bb) {
        this.stringReader = new StringReader(bb);;
    }

	/**
	 * Extract the broadcast message from a ByteBuffer.
	 * @return ProcessStatus a value from <i>ProcessStatus</i> enum.
	 */
    @Override
    public ProcessStatus process() {
        if (state != State.WAITING_MSG) {
            throw new IllegalStateException();
        }

        ProcessStatus messageStatus = stringReader.process();

        if (messageStatus != ProcessStatus.DONE) {
            return messageStatus;
        }
        message = stringReader.get();
        stringReader.reset();
        this.state = State.DONE;
        return ProcessStatus.DONE;    }

    /**
     * Get an object which corresponds to the result of the reading
     * @return an object
     */
    @Override
    public Object get() {
        if(state != State.DONE) {
        	throw new IllegalStateException();
        }
        return new DiffMsgClientFrame(message);
    }

    /**
     *	Reset the state of the reader.
     */
    @Override
    public void reset() {
        this.state = State.WAITING_MSG;
        stringReader.reset();
    }
}
