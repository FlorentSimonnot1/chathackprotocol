package main.reader.frameReader;

import main.frame.ClientNotFoundFrame;
import main.reader.Reader;
import main.reader.simpleReader.StringReader;

import java.nio.ByteBuffer;

/**
 * ClientNotFoundReader implements Reader interface.
 * This class is a reader which read a frame when the server cannot find a client.
 */
public class ClientNotFoundReader implements Reader {

	/**
	 * Status enumeration used for the frame reader.
	 * Its correspond to status for the frame reading.
	 */
    private enum State {
        DONE, WAITING, ERROR
    };

    private State state = State.WAITING;

    private final StringReader stringReader;
    private String dest;

	/**
     * This method is the constructor of the class ClientNotFoundReader.
     * It allow us to extract the address from a ByteBuffer.
	 * @param bb a ByteBuffer that represent a frame.
	 */
    public ClientNotFoundReader(ByteBuffer bb) {
        this.stringReader = new StringReader(bb);
    }

	/**
	 * Extract the error message from a ByteBuffer.
	 * @return ProcessStatus a value from <i>ProcessStatus</i> enum.
	 */
    @Override
    public ProcessStatus process() {

        var status = stringReader.process();
        if(status != ProcessStatus.DONE) return status;
        dest = stringReader.get();
        this.state = State.DONE;
        return ProcessStatus.DONE;
    }

    /**
     * Get an object which corresponds to the result of the reading
     * @return an object
     */
    @Override
    public Object get() {
        if(state != State.DONE) throw new IllegalStateException();
        return new ClientNotFoundFrame(dest);
    }

    /**
     *	Reset the state of the reader.
     */
    @Override
    public void reset() {
        stringReader.reset();
        state = State.WAITING;
    }
}
