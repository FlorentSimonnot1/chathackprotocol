package main.reader.frameReader;

import java.nio.ByteBuffer;

import main.frame.DiffMsgServerFrame;
import main.reader.Reader;
import main.reader.simpleReader.StringReader;

/**
 * DiffMsgClientReader implements Reader interface.
 * This class is a reader which read a frame when the server forward a broadcast message.
 */
public class DiffMsgServerReader implements Reader{

	/**
	 * Status enumeration used for the frame reader.
	 * Its correspond to status for the frame reading.
	 */
    private enum State {
        DONE, WAITING_LOGIN, WAITING_MSG, ERROR
    };

    private State state = State.WAITING_LOGIN;

    private String login;
    private String message;
    private final StringReader stringReader;

	/**
     * This method is the constructor of the class DiffMsgClientReader.
     * It allow us to extract the login and the broadcast message from a ByteBuffer.
	 * @param bb a ByteBuffer that represent a frame.
	 */
    public DiffMsgServerReader(ByteBuffer bb) {
        this.stringReader = new StringReader(bb);
    }

	/**
	 * Extract the information from a ByteBuffer step by step.
	 * First the login and then the broadcast message.
	 * @return ProcessStatus a value from <i>ProcessStatus</i> enum.
	 */
    @Override
    public ProcessStatus process() {
        if (state == State.DONE || state == State.ERROR) {
            throw new IllegalStateException();
        }

        switch (state){
            case WAITING_LOGIN:
                ProcessStatus loginStatus = stringReader.process();
                if (loginStatus != ProcessStatus.DONE) return loginStatus;
                login = stringReader.get();
                stringReader.reset();
                state = State.WAITING_MSG;
            case WAITING_MSG:
                ProcessStatus messageStatus = stringReader.process();
                if(messageStatus != ProcessStatus.DONE) return messageStatus;
                message = stringReader.get();
                stringReader.reset();
                break;
            default: throw new AssertionError();
        }
        
        this.state = State.DONE;
        return ProcessStatus.DONE;
    }

    /**
     * Get an object which corresponds to the result of the reading
     * @return an object
     */
    @Override
    public Object get() {
        if(state != State.DONE){
            throw new IllegalStateException();
        }
        return new DiffMsgServerFrame(login, message);
    }

    /**
     *	Reset the state of the reader.
     */
    @Override
    public void reset() {
        this.state = State.WAITING_LOGIN;
    }
}
