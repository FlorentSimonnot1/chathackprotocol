package main.reader.frameReader;

import main.frame.AckPrivateConnexionFrame;
import main.frame.address.AddressFrame;
import main.reader.Reader;
import main.reader.simpleReader.AddressReader;
import main.reader.simpleReader.LongReader;
import main.reader.simpleReader.StringReader;

import java.nio.ByteBuffer;
import java.util.Objects;

/**
 * AckPrivateConnexionReader implements Reader interface.
 * This class is a reader which read a frame when a client wants to establish a private connection with another client.
 */
public class AckPrivateConnexionReader implements Reader {

	/**
	 * Status enumeration used for the frame reader.
	 * Its correspond to status for the frame reading.
	 */
    private enum State{
        DONE, WAITING_PSEUDO, WAITING_ADDRESS,WAITING_TOKEN, ERROR
    }

    private final ByteBuffer bb;
    private State state = State.WAITING_PSEUDO;
    private String pseudo;
    private AddressFrame address;
    private long token;

	/**
     * This method is the constructor of the class AckPrivateConnexionReader.
     * It allow us to extract the information from a ByteBuffer.
	 * @param bb a ByteBuffer that represent a frame.
	 */
    public AckPrivateConnexionReader(ByteBuffer bb) {
        this.bb = Objects.requireNonNull(bb);
    }

	/**
	 * Extract the information from a ByteBuffer step by step.
	 * First this method extract the pseudo then the address.
	 * And finally the token.
	 * @return ProcessStatus a value from <i>ProcessStatus</i> enum.
	 */
    @Override
    public ProcessStatus process() {

        if(state == State.DONE || state == State.ERROR) throw new IllegalStateException("Illegal State");

        switch (state){
            case WAITING_PSEUDO:
                var pseudoReader = new StringReader(bb);
                var pseudoStatus = pseudoReader.process();
                if(pseudoStatus != ProcessStatus.DONE) return pseudoStatus;
                pseudo = pseudoReader.get();
                pseudoReader.reset();
                state = State.WAITING_ADDRESS;
            case WAITING_ADDRESS:
                var addressReader = new AddressReader(bb);
                var addressStatus = addressReader.process();
                if(addressStatus != ProcessStatus.DONE) return addressStatus;
                address = (AddressFrame) addressReader.get();
                addressReader.reset();
                state = State.WAITING_TOKEN;
            case WAITING_TOKEN:
                var tokenReader = new LongReader(bb);
                var tokenStatus = tokenReader.process();
                if(tokenStatus != ProcessStatus.DONE) return tokenStatus;
                token = (long) tokenReader.get();
                tokenReader.reset();
                break;
            default: throw new AssertionError();
        }

        this.state = State.DONE;
        return ProcessStatus.DONE;
    }

    /**
     * Get an object which corresponds to the result of the reading
     * @return an object
     */
    @Override
    public Object get() {
        if(state == State.DONE)
            return new AckPrivateConnexionFrame(pseudo, address, token);
        throw new IllegalStateException();
    }

    /**
     *	Reset the state of the reader.
     */
    @Override
    public void reset() {
        state = State.WAITING_PSEUDO;
    }
}
