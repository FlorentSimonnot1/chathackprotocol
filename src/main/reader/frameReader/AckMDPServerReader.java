package main.reader.frameReader;

import java.nio.ByteBuffer;
import java.util.Objects;

import main.frame.AckMDPServerFrame;
import main.opcodes.Opcodes;
import main.reader.Reader;
import main.reader.simpleReader.LongReader;

/**
 * AckMDPServerReader implements Reader interface.
 * This class is a reader which read a frame when the MDPserver send an acknowledgement.
 */
public class AckMDPServerReader implements Reader {

	/**
	 * Status enumeration used for the frame reader.
	 * Its correspond to status for the frame reading.
	 */
    private enum State {
        DONE, WAITING, ERROR
    };

    private State state = State.WAITING;
    private final LongReader reader;
    private final byte opcode;
    private long id;

	/**
     * This method is the constructor of the class AckMDPServerReader.
     * It allow us to extract the long corresponding to the id of a connection from a ByteBuffer.
	 * @param byteBuffer a ByteBuffer that represent a frame.
	 * @param opcode opcode of the acknowledgement as a byte.
	 */
    public AckMDPServerReader(ByteBuffer byteBuffer, byte opcode) {
        Objects.requireNonNull(byteBuffer);

        if(opcode < Opcodes.ACK_SERVER_MDP_FAILED || opcode > Opcodes.ASK_GUEST_LOGIN_MDP_SERVER) throw new IllegalArgumentException("Bad opcode");

        this.reader = new LongReader(byteBuffer);
        this.opcode = opcode;
    }

	/**
	 * Extract the id from a ByteBuffer.
	 * @return ProcessStatus a value from <i>ProcessStatus</i> enum.
	 */
    @Override
    public ProcessStatus process() {
        if(state != State.WAITING)
            throw new IllegalStateException("Process already done");

        var status = reader.process();

        if(status != ProcessStatus.DONE)
            return status;

        id = (long) reader.get();
        reader.reset();
        state = State.DONE;
        return ProcessStatus.DONE;

    }

    /**
     * Get the frame corresponding.
     * @return an Object.
     */
    @Override
    public Object get() {
        if(state != State.DONE)
            throw new IllegalStateException();
        return new AckMDPServerFrame(opcode, id);
    }

    /**
     *	Reset the state of the reader.
     */
    @Override
    public void reset() {
        state = State.WAITING;
    }
}
