package main.reader.frameReader;

import main.frame.LoginWithCredentialsFrame;
import main.reader.Reader;
import main.reader.simpleReader.StringReader;

import java.nio.ByteBuffer;

/**
 * LoginWithCredentialsReader implements Reader interface.
 * This class is a reader which read a frame when a client send a request of a connection with login to the server.
 */
public class LoginWithCredentialsReader implements Reader {

	/**
	 * Status enumeration used for the frame reader.
	 * Its correspond to status for the frame reading.
	 */
    private enum State {
        DONE, WAITING_LOGIN, WAITING_PASSWORD, ERROR
    };

    private State state = State.WAITING_LOGIN;

    private String login;
    private String password;
    private final StringReader stringReader;

	/**
     * This method is the constructor of the class LoginWithCredentialsReader.
	 * @param bb a ByteBuffer that represent a frame.
	 */
    public LoginWithCredentialsReader(ByteBuffer bb) {
        this.stringReader = new StringReader(bb);
    }

	/**
	 * Extract the login and password token from a ByteBuffer.
	 * @return ProcessStatus a value from <i>ProcessStatus</i> enum.
	 */
    @Override
    public ProcessStatus process() {
        if (state == State.DONE || state == State.ERROR) {
            throw new IllegalStateException();
        }

        switch (state){
            case WAITING_LOGIN:
                ProcessStatus loginStatus = stringReader.process();
                if (loginStatus != ProcessStatus.DONE) return loginStatus;
                login = stringReader.get();
                stringReader.reset();
                state = State.WAITING_LOGIN;
            case WAITING_PASSWORD:
                ProcessStatus passwordStatus = stringReader.process();
                if(passwordStatus != ProcessStatus.DONE) return passwordStatus;
                password = stringReader.get();
                stringReader.reset();
                break;
            default: throw new AssertionError();
        }

        this.state = State.DONE;
        return ProcessStatus.DONE;
    }

    /**
     * Get an object which corresponds to the result of the reading
     * @return an object
     */
    @Override
    public Object get() {
        if(state != State.DONE){
            throw new IllegalStateException();
        }
        return new LoginWithCredentialsFrame(login, password);
    }
    /**
     *	Reset the state of the reader.
     */
    @Override
    public void reset() {
        this.state = State.WAITING_LOGIN;
    }
}
