package main.reader.frameReader;

import main.frame.LoginWithoutCredentialsFrame;
import main.reader.Reader;
import main.reader.simpleReader.StringReader;

import java.nio.ByteBuffer;

/**
 * LoginWithoutCredentialsReader implements Reader interface.
 * This class is a reader which read a frame when a client send a request of a guest connection to the server.
 */
public class LoginWithoutCredentialsReader implements Reader {

	/**
	 * Status enumeration used for the frame reader.
	 * Its correspond to status for the frame reading.
	 */
    private enum State {
        DONE, WAITING_LOGIN, ERROR
    };

    private State state = State.WAITING_LOGIN;

    private final StringReader stringReader;
    private String login;

	/**
     * This method is the constructor of the class LoginWithoutCredentialsReader.
	 * @param bb a ByteBuffer that represent a frame.
	 */
    public LoginWithoutCredentialsReader(ByteBuffer bb) {
        this.stringReader = new StringReader(bb);
    }

	/**
	 * Extract the login from a ByteBuffer.
	 * @return ProcessStatus a value from <i>ProcessStatus</i> enum.
	 */
    @Override
    public ProcessStatus process() {
        if (state != State.WAITING_LOGIN) {
            throw new IllegalStateException();
        }

        ProcessStatus loginStatus = stringReader.process();

        if (loginStatus != ProcessStatus.DONE) {
            return loginStatus;
        }
        login = stringReader.get();
        stringReader.reset();
        this.state = State.DONE;
        return ProcessStatus.DONE;
    }

    /**
     * Get an object which corresponds to the result of the reading
     * @return an object
     */
    @Override
    public Object get() {
        if(state != State.DONE){
            throw new IllegalStateException();
        }
        return new LoginWithoutCredentialsFrame(login);
    }

    /**
     *	Reset the state of the reader.
     */
    @Override
    public void reset() {
        this.state = State.WAITING_LOGIN;
        stringReader.reset();
    }
}
