package main.reader.frameReader;

import main.frame.ErrorFrame;
import main.reader.Reader;
import main.reader.simpleReader.ByteReader;
import main.reader.simpleReader.StringReader;

import java.nio.ByteBuffer;
import java.util.Objects;

/**
 * ErrorReader implements Reader interface.
 * This class is a reader which read a frame when a error is send.
 */
public class ErrorReader implements Reader {

	/**
	 * Status enumeration used for the frame reader.
	 * Its correspond to status for the frame reading.
	 */
    private enum State {
        DONE, WAITING_OPCODE, WAITING_MSG, ERROR
    };

    private State state = State.WAITING_OPCODE;

    private final ByteBuffer bb;
    private byte errorCode;
    private String errorMessage;

	/**
     * This method is the constructor of the class ErrorReader.
	 * @param bb a ByteBuffer that represent a frame.
	 */
    public ErrorReader(ByteBuffer bb) {
        this.bb = Objects.requireNonNull(bb);
    }

	/**
	 * Extract the opcode corresponding to the error and a message from the ByteBuffer.
	 * @return ProcessStatus a value from <i>ProcessStatus</i> enum.
	 */
    @Override
    public ProcessStatus process() {
        if(state == State.DONE || state == State.ERROR)
            throw new IllegalStateException();

        switch(state){
            case WAITING_OPCODE:
                var opcodeReader = new ByteReader(bb);
                var opcodeStatus = opcodeReader.process();
                if(opcodeStatus != ProcessStatus.DONE) return opcodeStatus;
                errorCode = (byte) opcodeReader.get();
                opcodeReader.reset();
                state = State.WAITING_MSG;
            case WAITING_MSG:
                var messageReader = new StringReader(bb);
                var messageStatus = messageReader.process();
                if(messageStatus != ProcessStatus.DONE) return messageStatus;
                errorMessage = messageReader.get();
                messageReader.reset();
                break;
            default: throw new AssertionError();
        }


        this.state = State.DONE;
        return ProcessStatus.DONE;
    }

    /**
     * Get an object which corresponds to the result of the reading
     * @return an object
     */
    @Override
    public Object get() {
        if(state != State.DONE)
            throw new IllegalStateException();
        return new ErrorFrame(errorCode, errorMessage);
    }

    /**
     *	Reset the state of the reader.
     */
    @Override
    public void reset() {
        this.state = State.WAITING_OPCODE;
    }
}
