package main.reader.frameReader;

import main.frame.AckCnxFrame;
import main.reader.Reader;
import main.reader.simpleReader.LongReader;

import java.nio.ByteBuffer;

/**
 * ConnexionAckReader implements Reader interface.
 * This class is a reader which read a frame when the server send an acknowledgement of a connection.
 */
public class ConnexionAckReader implements Reader {

	/**
	 * Status enumeration used for the frame reader.
	 * Its correspond to status for the frame reading.
	 */
    private enum State {
        DONE, WAITING, ERROR
    };

    private State state = State.WAITING;
    private final ByteBuffer bb;
    private long tokenId;

	/**
     * This method is the constructor of the class ConnexionAckReader.
     * It allow us to extract the id from a ByteBuffer.
	 * @param byteBuffer a ByteBuffer that represent a frame.
	 */
    public ConnexionAckReader(ByteBuffer byteBuffer) {
        this.bb = byteBuffer;
    }

	/**
	 * Extract the id from a ByteBuffer.
	 * @return ProcessStatus a value from <i>ProcessStatus</i> enum.
	 */
    @Override
    public ProcessStatus process() {
        if(state != State.WAITING)
            throw new IllegalStateException("Process already done");

        var reader = new LongReader(bb);
        var status = reader.process();
        if(status != ProcessStatus.DONE) return status;
        tokenId = (long) reader.get();
        reader.reset();

        state = State.DONE;
        return ProcessStatus.DONE;

    }

    /**
     * Get an object which corresponds to the result of the reading
     * @return an object
     */
    @Override
    public Object get() {
        if(state == State.DONE)
            return new AckCnxFrame(tokenId);
        throw new IllegalStateException("Process have to be make before get frame");
    }

    /**
     *	Reset the state of the reader.
     */
    @Override
    public void reset() {
        this.state= State.WAITING;
    }
}
