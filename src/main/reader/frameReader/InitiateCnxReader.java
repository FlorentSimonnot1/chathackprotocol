package main.reader.frameReader;

import java.nio.ByteBuffer;

import main.frame.InitiateCnxFrame;
import main.reader.Reader;
import main.reader.simpleReader.LongReader;

/**
 * InitiateCnxReader implements Reader interface.
 * This class is a reader which read a frame when a client send a request of a private connection.
 */
public class InitiateCnxReader implements Reader{

	/**
	 * Status enumeration used for the frame reader.
	 * Its correspond to status for the frame reading.
	 */
	private enum State {
        DONE, WAITING_TOKEN, ERROR
    };
	
    private State state = State.WAITING_TOKEN;
    
    private final LongReader longreader;
    private long token;

	/**
     * This method is the constructor of the class InitiateCnxReader.
	 * @param bb a ByteBuffer that represent a frame.
	 */
    public InitiateCnxReader(ByteBuffer bb) {
        this.longreader = new LongReader(bb);;
    }

	/**
	 * Extract the error token from a ByteBuffer.
	 * @return ProcessStatus a value from <i>ProcessStatus</i> enum.
	 */
    @Override
    public ProcessStatus process() {
        if (state != State.WAITING_TOKEN) {
            throw new IllegalStateException();
        }

        ProcessStatus tokenStatus = longreader.process();

        if (tokenStatus != ProcessStatus.DONE) {
            return tokenStatus;
        }
        token = (long) longreader.get();
        longreader.reset();
        this.state = State.DONE;
        return ProcessStatus.DONE;    }

    /**
     * Get an object which corresponds to the result of the reading
     * @return an object
     */
    @Override
    public Object get() {
        if(state != State.DONE) {
        	throw new IllegalStateException();
        }
        return new InitiateCnxFrame(token);
    }

    /**
     *	Reset the state of the reader.
     */
    @Override
    public void reset() {
        this.state = State.WAITING_TOKEN;
        longreader.reset();
    }
}
