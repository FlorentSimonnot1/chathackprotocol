package main.reader.frameReader;

import java.nio.ByteBuffer;

import main.frame.SendFileFrame;
import main.reader.Reader;
import main.reader.simpleReader.FileReader;
import main.reader.simpleReader.StringReader;

/**
 * InitiateCnxReader implements Reader interface.
 * This class is a reader which read a frame when a client send a file.
 */
public class SendFileReader implements Reader{

	/**
	 * Status enumeration used for the frame reader.
	 * Its correspond to status for the frame reading.
	 */
    private enum State {
        DONE, WAITING_NAME, WAITING_FILE, ERROR
    };

    private State state = State.WAITING_NAME;

    private String name;
    private ByteBuffer file;
    private final FileReader fileReader;
    private final StringReader stringReader;

	/**
     * This method is the constructor of the class SendFileReader.
	 * @param bb a ByteBuffer that represent a frame.
	 */
    public SendFileReader(ByteBuffer bb) {
        this.fileReader = new FileReader(bb);
        this.stringReader = new StringReader(bb);
    }

	/**
	 * Extract the name of the file and then the file from a ByteBuffer.
	 * @return ProcessStatus a value from <i>ProcessStatus</i> enum.
	 */
    @Override
    public ProcessStatus process() {
        if (state == State.DONE || state == State.ERROR) {
            throw new IllegalStateException();
        }

        switch (state){
            case WAITING_NAME:
                ProcessStatus nameStatus = stringReader.process();
                if (nameStatus != ProcessStatus.DONE) return nameStatus;
                name = stringReader.get();
                state = State.WAITING_FILE;
            case WAITING_FILE:
                ProcessStatus fileStatus = fileReader.process();
                if(fileStatus != ProcessStatus.DONE) return fileStatus;
                file = fileReader.get();
                break;
            default: throw new AssertionError();
        }
        
        this.state = State.DONE;
        return ProcessStatus.DONE;
    }

    /**
     * Get an object which corresponds to the result of the reading
     * @return an object
     */
    @Override
    public Object get() {
        if(state != State.DONE){
            throw new IllegalStateException();
        }
        return new SendFileFrame(name, file);
    }

    /**
     *	Reset the state of the reader.
     */
    @Override
    public void reset() {
        this.state = State.WAITING_NAME;
        fileReader.reset();
        stringReader.reset();
    }

}
