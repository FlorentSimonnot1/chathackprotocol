package main.reader.simpleReader;

import java.nio.ByteBuffer;
import java.util.Objects;

import main.reader.Reader;

/**
 * FileReader implements Reader interface.
 * This class is a reader which read a File.
 * It have to read the size and the content.
 */
public class FileReader implements Reader{

	/**
	 * Status enumeration used for the files reader.
	 * Its correspond to status for the frame reading.
	 */
	private enum State {
		DONE, WAITING_SIZE, WAITING_CONTENT, ERROR
	};

	private ByteBuffer bb;
	private State state = State.WAITING_SIZE;
	private ByteBuffer text;

	/**
     * This method is the constructor of the class FileReader.
     * It allow us to extract the file from a ByteBuffer.
	 * @param bb - a ByteBuffer that represent a <i>file</i>.
	 */
	public FileReader(ByteBuffer bb) {
		Objects.requireNonNull(bb);
		this.bb = bb;
	}

	/**
	 * Extract the file from a ByteBuffer step by step.
	 * First we extract the size of the file and then we read the file.
	 * @return ProcessStatus - a value from <i>ProcessStatus</i> enum.
	 */
	@Override
	public ProcessStatus process() {
		if (state==State.DONE || state==State.ERROR) {
			throw new IllegalStateException();
		}

		bb.flip();
		try {
			switch (state) {
			case WAITING_SIZE:
				if (bb.remaining() < Integer.BYTES) {
					return ProcessStatus.REFILL;
				}

				int size = bb.getInt();
				if (size <= 0) {
					return ProcessStatus.ERROR;
				}
				state = State.WAITING_CONTENT;
				text = ByteBuffer.allocate(size);
				//No break in order to continue with WAITING_CONTENT
			case WAITING_CONTENT:
				ProcessStatus fillProcess = fillAsPossible(bb, text);
				if(fillProcess != ProcessStatus.DONE) return fillProcess;
				text.flip();
				state = State.DONE;
				return ProcessStatus.DONE;

			default:
				throw new AssertionError();
			}
		}finally {
			bb.compact();
		}
	}

	private ProcessStatus fillAsPossible(ByteBuffer src, ByteBuffer dst){
		while(src.hasRemaining() && dst.hasRemaining()){
			dst.put(src.get());
		}
		return dst.hasRemaining() ? ProcessStatus.REFILL : ProcessStatus.DONE;
	}

	/**
	 * Get the file.
	 * @return a ByteBuffer - the file.
	 */
	@Override
	public ByteBuffer get() {
		if (state != State.DONE) {
			throw new IllegalStateException();
		}
		return text;
	}

	/**
	 * Reset the state of the reader.
	 */
	@Override
	public void reset() {
		state = State.WAITING_SIZE;
	}
}
