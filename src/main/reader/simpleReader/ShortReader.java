package main.reader.simpleReader;

import main.reader.Reader;
import main.reader.status.Status;

import java.nio.ByteBuffer;
import java.util.Objects;

/**
 * ShortReader implements Reader interface.
 * This class is a reader which read a Short.
 */
public class ShortReader implements Reader {
    private final ByteBuffer bb;
    private Status state = Status.WAITING;
    private short value;

	/**
     * This method is the constructor of the class ShortReader.
     * It allow us to extract a short from a ByteBuffer.
	 * @param bb - a ByteBuffer that represent a <i>short</i>.
	 */
    public ShortReader(ByteBuffer bb) {
        this.bb = Objects.requireNonNull(bb);
    }

	/**
	 * Extract a short from a ByteBuffer.
	 * @return ProcessStatus - a value from <i>ProcessStatus</i> enum
	 */
    @Override
    public ProcessStatus process() {
        if (state != Status.WAITING) {
            throw new IllegalStateException();
        }

        bb.flip();

        try {
            if (bb.remaining() >= Short.BYTES) {
                value = bb.getShort();
                state = Status.DONE;
                return ProcessStatus.DONE;
            } else {
                return ProcessStatus.REFILL;
            }
        }finally {
            bb.compact();
        }
    }

    /**
     *	Get a short.
     * @return - an Object.
     */
    @Override
    public Object get() {
        if (state != Status.DONE) {
            throw new IllegalStateException();
        }
        return value;
    }

    /**
     *	Reset the state of the reader.
     */
    @Override
    public void reset() {
        state = Status.WAITING;
    }
}
