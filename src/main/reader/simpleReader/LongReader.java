package main.reader.simpleReader;

import main.reader.Reader;
import main.reader.status.Status;

import java.nio.ByteBuffer;
import java.util.Objects;

/**
 * LongReader implements Reader interface.
 * This class is a reader which read a Long.
*/
public class LongReader implements Reader {
    private final ByteBuffer bb;
    private Status state = Status.WAITING;
    private long value;

	/**
     * This method is the constructor of the class LongReader.
     * It allow us to extract a long from a ByteBuffer.
	 * @param bb - ByteBuffer that represent a <i>long</i>.
	 */
    public LongReader(ByteBuffer bb) {
        this.bb = Objects.requireNonNull(bb);
    }

	/**
	 * Extract a long from a ByteBuffer.
	 * @return ProcessStatus - a value from <i>ProcessStatus</i> enum.
	 */
    @Override
    public ProcessStatus process() {
        if (state != Status.WAITING) {
            throw new IllegalStateException();
        }

        bb.flip();

        try {
            if (bb.remaining() >= Long.BYTES) {
                value = bb.getLong();
                state = Status.DONE;
                return ProcessStatus.DONE;
            } else {
                return ProcessStatus.REFILL;
            }
        } finally {
            bb.compact();
        }
    }

    /**
     * Get a long.
     * @return an Object.
     */
    @Override
    public Object get() {
        if (state != Status.DONE) {
            throw new IllegalStateException();
        }
        return value;
    }
    
    
    /**
     * Reset the state of the reader.
     */
    @Override
    public void reset() {
        state = Status.WAITING;
    }
}
