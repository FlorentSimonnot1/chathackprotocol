package main.reader.simpleReader;

import main.reader.Reader;
import main.reader.status.Status;

import java.nio.ByteBuffer;
import java.util.Objects;

/**
 * ByteReader implements Reader interface.
 * This class is a reader which read a Byte.
 */
public class ByteReader implements Reader {
    private final ByteBuffer bb;
    private Status state = Status.WAITING;
    private byte value;

	/**
     * This method is the constructor of the class ByteReader.
     * It allow us to extract the file from a ByteBuffer.
	 * @param bb - a ByteBuffer that represent a <i>byte</i>.
	 */
    public ByteReader(ByteBuffer bb) {
        this.bb = Objects.requireNonNull(bb);
    }

	/**
	 * Extract a byte from a ByteBuffer.
	 * @return ProcessStatus - a value from <i>ProcessStatus</i> enum.
	 */
    @Override
    public ProcessStatus process() {
        if (state != Status.WAITING) {
            throw new IllegalStateException();
        }

        bb.flip();

        try {
            if (bb.remaining() >= Byte.BYTES) {
                value = bb.get();
                state = Status.DONE;
                return ProcessStatus.DONE;
            } else {
                return ProcessStatus.REFILL;
            }
        } finally {
            bb.compact();
        }
    }

    /**
     * Get the byte.
     *	@return an Object.
     */
    @Override
    public Object get() {
        if (state != Status.DONE) {
            throw new IllegalStateException();
        }
        return value;
    }

    /**
     * Reset the state of the reader.
     */
    @Override
    public void reset() {
        state = Status.WAITING;
    }
}
