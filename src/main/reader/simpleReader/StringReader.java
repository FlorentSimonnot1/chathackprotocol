package main.reader.simpleReader;

import main.frame.Utils;
import main.reader.Reader;

import javax.annotation.processing.SupportedSourceVersion;
import java.nio.ByteBuffer;
import java.util.Objects;

/**
 * StringReader implements Reader interface.
 * This class is a reader which read a String.
 * It have to read the string size and the content.
 */
public class StringReader implements Reader {

	/**
	 * Status enumeration used for the String reader.
	 * Its correspond to status for the frame reading.
	 */
    private enum State {
        DONE, WAITING_SIZE, WAITING_CONTENT, ERROR
    };

    private ByteBuffer bb;
    private State state = State.WAITING_SIZE;
    private String text;
    ByteBuffer buffer;

	/**
     * This method is the constructor of the class StringReader.
     * It allow us to extract a String from a ByteBuffer.
	 * @param bb - a ByteBuffer that represent a <i>String</i>.
	 */
    public StringReader(ByteBuffer bb) {
        this.bb = Objects.requireNonNull(bb);
    }

	/**
	 * Extract the String from a ByteBuffer step by step.
	 * First we extract the size of the String and then we read the String.
	 * @return ProcessStatus - a value from <i>ProcessStatus</i> enum
	 */
    @Override
    public ProcessStatus process() {
        if (state==State.DONE || state==State.ERROR) {
            throw new IllegalStateException();
        }
        bb.flip();
        try {
            switch (state) {

                case WAITING_SIZE:
                    if (bb.remaining() < Integer.BYTES) {
                        return ProcessStatus.REFILL;
                    }

                    int size = bb.getInt();
                    if (size <= 0) {
                        return ProcessStatus.ERROR;
                    }
                    state = State.WAITING_CONTENT;
                    buffer = ByteBuffer.allocate(size);
                    //No break in order to continue with WAITING_CONTENT
                case WAITING_CONTENT:
                    ProcessStatus fillProcess = fillAsPossible(bb, buffer);
                    if(fillProcess != ProcessStatus.DONE) return fillProcess;
                    buffer.flip();
                    text = Utils.getString(buffer);
                    state = State.DONE;
                    return ProcessStatus.DONE;

                default:
                    throw new AssertionError();
            }
        }finally {
            bb.compact();
        }
    }

    private ProcessStatus fillAsPossible(ByteBuffer src, ByteBuffer dst){
        while(src.hasRemaining() && dst.hasRemaining()){
            dst.put(src.get());
        }
        return dst.hasRemaining() ? ProcessStatus.REFILL : ProcessStatus.DONE;
    }

    /**
     * Get a String.
     *	@return String - the string from the ByteBuffer
     */
    @Override
    public String get() {
        if (state != State.DONE) {
            throw new IllegalStateException();
        }
        return text;
    }

    /**
     * Reset the state of the reader.
     */
    @Override
    public void reset() {
        state = State.WAITING_SIZE;
    }
}
