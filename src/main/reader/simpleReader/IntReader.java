package main.reader.simpleReader;

import main.reader.Reader;
import main.reader.status.Status;

import java.nio.ByteBuffer;
import java.util.Objects;

/**
 * IntReader implements Reader interface.
 * This class is a reader which read an Integer.
 */
public class IntReader implements Reader {
    private final ByteBuffer bb;
    private Status state = Status.WAITING;
    private int value;

	/**
     * This method is the constructor of the class IntReader.
     * It allow us to extract a int from a ByteBuffer.
	 * @param bb - a ByteBuffer that represent a <i>int</i>.
	 */
    public IntReader(ByteBuffer bb) {
        this.bb = Objects.requireNonNull(bb);
    }

	/**
	 * Extract a int from a ByteBuffer.
	 * @return ProcessStatus - a value from <i>ProcessStatus</i> enum.
	 */
    @Override
    public ProcessStatus process() {
        if (state != Status.WAITING) {
            throw new IllegalStateException();
        }

        bb.flip();

        try {
            if (bb.remaining() >= Integer.BYTES) {
                value = bb.getInt();
                state = Status.DONE;
                return ProcessStatus.DONE;
            } else {
                return ProcessStatus.REFILL;
            }
        } finally {
            bb.compact();
        }
    }

    /**
     * Get a int.
     *	@return an Object.
     */
    @Override
    public Object get() {
        if (state != Status.DONE) {
            throw new IllegalStateException();
        }
        return value;
    }

    /**
     * Reset the state of the reader.
     */
    @Override
    public void reset() {
        state = Status.WAITING;
    }
}
