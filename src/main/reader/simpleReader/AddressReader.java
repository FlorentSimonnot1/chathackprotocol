package main.reader.simpleReader;

import main.opcodes.Opcodes;
import main.frame.address.Ipv4Frame;
import main.frame.address.Ipv6Frame;
import main.reader.Reader;
import main.reader.simpleReader.ByteReader;
import main.reader.simpleReader.IntReader;

import java.nio.ByteBuffer;
import java.util.Objects;

/**
 * AddressReader implements Reader interface.
 * This class is a reader which read an address in Ipv4 or Ipv6.
 */
public class AddressReader implements Reader {

	/**
	 * Status enumeration used for the address reader.
	 * Its correspond to status for the frame reading.
	 */
    private enum State {
        DONE, WAITING_VERSION, WAITING_ADDRESS, WAITING_PORT, ERROR
    };

    private State state = State.WAITING_VERSION;
    private final ByteBuffer bb;
    private byte version;
    private byte[] address;
    private int port;

	/**
     * This method is the constructor of the class AddressReader.
     * It allow us to extract the address from a ByteBuffer.
	 * @param bb - a ByteBuffer that represent a frame.
	 */
    public AddressReader(ByteBuffer bb) {
        this.bb = Objects.requireNonNull(bb);
    }

	/**
	 * Extract the file from a ByteBuffer step by step.
	 * First we extract the version of the address and then we read the address.
	 * And finally the port.
	 * @return a value from <i>ProcessStatus</i> enum
	 */
    @Override
    public ProcessStatus process() {
        if(state == State.DONE || state == State.ERROR)
            throw new IllegalStateException();

        switch(state){
            case WAITING_VERSION:
                var versionReader = new ByteReader(bb);
                var versionStatus = versionReader.process();
                if(versionStatus != ProcessStatus.DONE) return versionStatus;
                version = (byte) versionReader.get();
                versionReader.reset();
                state = State.WAITING_ADDRESS;
            case WAITING_ADDRESS:
                if(version != Opcodes.IPV4 && version != Opcodes.IPV6) throw new IllegalStateException("Bad IP version");
                var size = version == Opcodes.IPV4 ? 4 : 16;
                address = new byte[size];
                //Get 4 or 16 bytes in according to IP version
                for(var i = 0; i < size; i++){
                    var addressReader = new ByteReader(bb);
                    var addressStatus = addressReader.process();
                    if(addressStatus != ProcessStatus.DONE) return addressStatus;
                    address[i] = (byte) addressReader.get();
                    addressReader.reset();
                }
                state = State.WAITING_PORT;
            case WAITING_PORT:
                var portReader = new IntReader(bb);
                var portStatus = portReader.process();
                if(portStatus != ProcessStatus.DONE) return portStatus;
                port = (int) portReader.get();
                portReader.reset();
                break;
            default: throw new AssertionError();
        }

        this.state = State.DONE;
        return ProcessStatus.DONE;
    }

    /**
     * Get the frame corresponding to the address in Ipv4 or Ipv6.
     *	@return - an Object
     */
    @Override
    public Object get() {
        if(state != State.DONE)
            throw new IllegalStateException();

        if(version == Opcodes.IPV4) return new Ipv4Frame(address, port);
        else if(version == Opcodes.IPV6) return new Ipv6Frame(address, port);
        else throw new IllegalStateException("Bad IP version");
    }

    /**
     * Reset the state of the reader.
     */
    @Override
    public void reset() {
        this.state = State.WAITING_VERSION;
    }
}
