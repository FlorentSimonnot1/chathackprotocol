/**
 * Provides reader used to read a type in a ByteBuffer.
 * Except the StringReader, the FileReader, and AddressReader the following readers read primitive types.
 */
package main.reader.simpleReader;