package main.reader;

/**
 * Interface class which corresponds to a frame reader.
 */
public interface Reader {

    /**
     * Enum class corresponding to a result of a reading frame.
     * DONE corresponds to a success.
     * REFILL is used when some data misses for complete the frame
     * ERROR is used when an error occur
     */
    enum ProcessStatus {DONE,REFILL,ERROR};

    /**
     * Execute the reading and return the result as a <i>ProcessStatus</i> enum
     * @return a value from <i>ProcessStatus</i> enum
     */
    ProcessStatus process();

    /**
     * Get an object which corresponds to the result of the reading
     * @return an object
     */
    Object get();

    /**
     * Reset the reader.
     */
    void reset();
}
