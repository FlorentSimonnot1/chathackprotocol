package main.reader.factory;

import main.opcodes.Opcodes;
import main.reader.Reader;
import main.reader.frameReader.*;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Objects;

/**
 * Create a reader in according to an opcode.
 */
public class ReaderFactory {
    private final HashMap<Byte, Reader> map;


    private ReaderFactory(HashMap<Byte, Reader> map) {
        this.map = map;
    }

    /**
     * Create a Reader in according to the opcode passed.
     * @param byteBuffer - the ByteBuffer corresponding to the frame
     * @return - a new ReaderFactory
     */
    public static ReaderFactory createReaderFactory(ByteBuffer byteBuffer){
        Objects.requireNonNull(byteBuffer);
        var map = new HashMap<Byte, Reader>();
        map.putIfAbsent(Opcodes.ASK_LOG_CNX, new LoginWithCredentialsReader(byteBuffer));
        map.putIfAbsent(Opcodes.ASK_GUEST_CNX, new LoginWithoutCredentialsReader(byteBuffer));
        map.putIfAbsent(Opcodes.ASK_CREDENTIALS_OR_SERVER_MDP_SUCCESS, new AckMDPServerReader(byteBuffer, Opcodes.ASK_CREDENTIALS_OR_SERVER_MDP_SUCCESS));
        map.putIfAbsent(Opcodes.ACK_SERVER_MDP_FAILED, new AckMDPServerReader(byteBuffer, Opcodes.ACK_SERVER_MDP_FAILED));
        map.putIfAbsent(Opcodes.CQUIT, new AckReader(Opcodes.CQUIT));
        map.putIfAbsent(Opcodes.SQUIT, new AckReader(Opcodes.SQUIT));
        map.putIfAbsent(Opcodes.ACK_SV_CNX_ESTABLISHED, new AckReader(Opcodes.ACK_SV_CNX_ESTABLISHED));
        map.putIfAbsent(Opcodes.SND_BROADCAST_MSG, new DiffMsgClientReader(byteBuffer));
        map.putIfAbsent(Opcodes.BROADCAST_MSG, new DiffMsgServerReader(byteBuffer));
        map.putIfAbsent(Opcodes.PV_CNX_ASKED, new ServerForwardPrivateConnexionDemandingReader(byteBuffer));
        map.putIfAbsent(Opcodes.ERR, new ErrorReader(byteBuffer));
        map.putIfAbsent(Opcodes.SND_PV_MSG, new PrivateMsgReader(byteBuffer));
        map.putIfAbsent(Opcodes.SND_PV_FILE, new SendFileReader(byteBuffer));
        map.putIfAbsent(Opcodes.ASK_PV_CNX, new InitiateCnxReader(byteBuffer));
        map.putIfAbsent(Opcodes.ACK_PV_CNX_ESTABLISHED , new ConnexionAckReader(byteBuffer));
        map.putIfAbsent(Opcodes.ACK_REFUSE_PV_CNX, new ClientToServerErrorReader(byteBuffer));
        map.putIfAbsent(Opcodes.ASK_PERMISSION_PV_CNX, new ClientToClientReader(byteBuffer));
        map.putIfAbsent(Opcodes.ACK_ACCEPT_PV_CNX , new AckPrivateConnexionReader(byteBuffer));
        map.putIfAbsent(Opcodes.ERR_CLIENT_NOT_ACCESSIBLE , new ClientNotFoundReader(byteBuffer));
        return new ReaderFactory(map);
    }

    /**
     * Get the reader associate to the opcode.
     * @param opcode - the frame opcode.
     * @return a Reader capable of reading the frame in according to the opcode.
     * @throws IllegalArgumentException when the opcode isn't associate to a reader.
     */
    public Reader getReader(byte opcode){
        if(!map.containsKey(opcode)) throw new IllegalArgumentException();
        return map.get(opcode);
    }


}
