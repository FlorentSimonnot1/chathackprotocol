/**
 * Provides a factory class which get a reader according to an opcode read before.
 */
package main.reader.factory;