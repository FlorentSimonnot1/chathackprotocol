package main.reader;

import main.frame.Frame;
import main.reader.factory.ReaderFactory;
import main.reader.simpleReader.ByteReader;

import java.nio.ByteBuffer;

/**
 * FrameReader class implements <i>Reader</i> class methods.
 * This class read the opcode and uses the appropriate reader.
 *
 */
public class FrameReader implements Reader {

    private enum State {
        DONE, WAITING_OPCODE, WAITING_FRAME, ERROR
    };

    private State state = State.WAITING_OPCODE;
    private final ByteBuffer byteBuffer;
    private final ReaderFactory factory;
    private byte opcode;
    private Frame frame;

    private FrameReader(ByteBuffer byteBuffer, ReaderFactory factory) {
        this.byteBuffer = byteBuffer;
        this.factory = factory;
    }

    /**
     * Creates a FrameReader.
     * @param byteBuffer the data ByteBuffer
     * @param factory the factory will be use to read frame in according to the opcode
     * @return a FrameReader
     * @see ReaderFactory
     */
    public static FrameReader create(ByteBuffer byteBuffer, ReaderFactory factory){
        return new FrameReader(byteBuffer, factory);
    }

    @Override
    public ProcessStatus process() {
        if(state == State.DONE || state == State.ERROR)
            throw new IllegalStateException();


        switch (state){
            case WAITING_OPCODE:
                var opcodeReader = new ByteReader(byteBuffer);
                var opcodeStatus = opcodeReader.process();
                if(opcodeStatus != ProcessStatus.DONE) return opcodeStatus;
                opcode = (byte) opcodeReader.get();
                this.state = State.WAITING_FRAME;
            case WAITING_FRAME:
                var reader = factory.getReader(opcode);
                var readerStatus = reader.process();
                if(readerStatus != ProcessStatus.DONE) return readerStatus;
                frame = (Frame) reader.get();
                reader.reset();
                this.state = State.DONE;
                return ProcessStatus.DONE;
            default: throw new AssertionError();
        }
    }

    @Override
    public Object get() {
        if(state != State.DONE)
            throw new IllegalStateException();
        return frame;
    }

    @Override
    public void reset() {
        this.state = State.WAITING_OPCODE;
    }
}
