/**
 * Declares some status values which represents the result of reading data.
 */
package main.reader.status;