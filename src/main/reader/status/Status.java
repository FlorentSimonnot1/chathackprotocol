package main.reader.status;

/**
 * Status enumeration used for the simple Reader.
 * Its correspond to status for the frame reading.
 */
public enum Status {
    DONE,
    WAITING,
    ERROR
}
