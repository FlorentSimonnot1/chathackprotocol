/**
 * Provides Bytes Reader which read a ByteBuffer and create a frame according to the data read.
 */
package main.reader;