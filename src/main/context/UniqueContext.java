package main.context;

import main.frame.Frame;
import main.frame.Utils;
import main.reader.FrameReader;
import main.reader.factory.ReaderFactory;
import main.visitor.ClientVisitorImpl;
import main.visitor.Visitor;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.Queue;

/**
 * UniqueContext is an implementation of a Context.
 * This is the only context used by the server and the client.
 * Nevertheless, we used a <i>ContextType</i> enum to precise where the context is used.
 */
public class UniqueContext implements Context {
    private static final int BUFFER_SIZE = 1_024;
    private final SocketChannel sc;
    private final ByteBuffer bbin = ByteBuffer.allocateDirect(BUFFER_SIZE);
    private final ByteBuffer bbout = ByteBuffer.allocateDirect(BUFFER_SIZE);
    private final Queue<ByteBuffer> queue = new LinkedList<>();
    private final ContextType type;
    private boolean closed;
    private final SelectionKey key;
    private final FrameReader reader;
    private final Visitor visitor;

    /**
     * Instantiate a new UniqueContext object linked to a SelectionKey <i>key</i>.
     * We precise the contextType <i>type</i> and passed a visitor.
     * @param key - a SelectionKey linked to this context
     * @param type - the type of this context
     * @param visitor - the visitor we want to apply to this context
     */
    public UniqueContext(SelectionKey key, ContextType type, Visitor visitor) {
        this.sc = (SocketChannel) key.channel();
        this.key = key;
        //Configure the visitor
        this.visitor = visitor;
        ClosedFunctionalInterface closedMethod = () -> {
            closed = true;
        };
        this.visitor.setClosedMethod(closedMethod);

        this.type = type;
        var factory = ReaderFactory.createReaderFactory(bbin);
        this.reader = FrameReader.create(bbin, factory);
    }

    /**
     * Get the SelectionKey associates to this context.
     * @return the SelectionKey associates to this context
     */
    public SelectionKey getKey() {
        return key;
    }

    /**
     * Get the ContextType associates to this context.
     * @return the ContextType associates to this context.
     * @see ContextType
     */
    public ContextType getType() {
        return type;
    }

    @Override
    public void doRead() throws IOException, InterruptedException {
        if (sc.read(bbin) == -1) {
            closed = true;
        }
        processIn();
        if(type == ContextType.NOT_CONNECTED_SERVER){
            return;
        }
        updateInterestOps();
    }

    @Override
    public void doWrite() throws IOException{
        bbout.flip();
        sc.write(bbout);
        bbout.compact();
        processOut();
        updateInterestOps();
    }

    @Override
    public void processIn() throws IOException, InterruptedException {
        for (;;) {
            var status = reader.process();
            switch (status){
                case DONE: var frame = (Frame) reader.get(); visitor.accept(frame); /*frame.accept(visitor);*/ reader.reset(); break;
                case REFILL: return;
                case ERROR: silentlyClose(); return;
                default: throw new AssertionError();
            }
        }
    }

    @Override
    public void processOut() {
        while (!queue.isEmpty() && bbout.hasRemaining()) {
            Utils.fillAsPossible(queue.peek(), bbout);
            if(!queue.peek().hasRemaining())
                queue.poll();
        }
    }

    @Override
    public void doConnect() throws IOException {
        //Useless if its a PUBLIC CLIENT context
        if(type == ContextType.PUBLIC_CLIENT) return;
        if (!sc.finishConnect()) {
            return;
        }
        updateInterestOps();
    }

    @Override
    public void addToQueue(Frame frame) {
        queue.add(frame.bufferize());
        processOut();
        updateInterestOps();
    }

    @Override
    public void updateInterestOps() {
        int ops = 0;

        if (bbin.hasRemaining() && !closed) {
            ops |= SelectionKey.OP_READ;
        }
        if (bbout.position() != 0) {
            ops |= SelectionKey.OP_WRITE;
        }
        if (ops == 0) {
            silentlyClose();
            /*FOR PUBLIC CONTEXT ADD CLOSE CLIENT*/
            if(type == ContextType.PUBLIC_CLIENT) {
                var client = ((ClientVisitorImpl)visitor).getClient();
                client.close();
            }
        } else {
            key.interestOps(ops);
        }
    }

    private void silentlyClose() {
        try {
            sc.close();
        } catch (IOException e) {
            // ignore exception
        }
    }

    /**
     *         Represents value to precise in which context it used.
     */
    public enum ContextType{
        /**
         * <i>PUBLIC_CLIENT</i> precises that the context will be use to represent a client connected to a server. (Client side)
         */
        PUBLIC_CLIENT,
        /**
         * <i>PRIVATE_CLIENT</i> precises that the context will be use to represent a client during a private connexion. (Client Side)
         */
        PRIVATE_CLIENT,
        /**
         * <i>MDP_SERVER</i> precises that the context will be use to represent a server connected with the MDP server. (Server side)
         */
        MDP_SERVER,
        /**
         * <i>NOT_CONNECTED_SERVER</i> precises that the context will be use to represent a client not yet connected to the server. (Server side)
         */
        NOT_CONNECTED_SERVER,
        /**
         * <i>CONNECTED_SERVER</i> precises that the context will be use to represent a client connected to server. (Server side)
         */
        CONNECTED_SERVER
    }



}
