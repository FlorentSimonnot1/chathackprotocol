package main.context;

/**
 * Functional interface used to close a context.
 */
public interface ClosedFunctionalInterface {

    /**
     * Apply the operation.
     */
    void apply();
}
