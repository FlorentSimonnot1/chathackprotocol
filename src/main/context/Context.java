package main.context;

import main.frame.Frame;
import main.visitor.Visitor;

import java.io.IOException;

/**
 * Represents a context interface. Declares its method like read, write data.
 */
public interface Context {

    /**
     * Fills the ByteBuffer with the data received from the SocketChannel.
     * Launch the processIn method.
     * Update the interest operator.
     * @throws IOException - due to read method from SocketChannel
     * @throws InterruptedException - due to processIn method
     * @see java.nio.channels.SocketChannel
     */
    void doRead() throws IOException, InterruptedException;

    /**
     * Send the data to the SocketChannel. Refill the ByteBuffer as possible and change the interest operator.
     * @throws IOException - due to write method from SocketChannel
     */
    void doWrite() throws IOException;

    /**
     * Reads the ByteBuffer used to read data as a Frame and execute operation associates to the frame.
     * @throws IOException - due to accept method from Visitor interface
     * @throws InterruptedException - due to accept method from Visitor interface
     * @see Visitor
     */
    void processIn() throws IOException, InterruptedException;

    /**
     * Fills as possible the Bytebuffer used to send data.
     */
    void processOut();

    /**
     * Uses to connect the context to the SocketChannel.
     * Can't be used for a context of type <i>PUBLIC_CLIENT</i>.
     * @throws IOException - due to finishConnect method from SocketChannel
     * @see java.nio.channels.SocketChannel
     */
    void doConnect() throws IOException;

    /**
     * Adds the Frame <i>frame</i> to the context queue as a ByteBuffer.
     * Afterwards, fill as possible the Bytebuffer used to send data and update the interestOps.
     * @param frame - the frame we wanting to put to the queue.
     */
    void addToQueue(Frame frame);

    /**
     * Update the SelectionKey interest set.
     */
    void updateInterestOps();

}
