package main.frame;

import java.nio.ByteBuffer;

/**
 * AskLoginWithCredentialsServerToServerFrame is an implementation of a frame.
 * It allows us to represent a frame when the server send a request of a connection with credential to the MDPserver.
 */
public class AskLoginWithCredentialsServerToServerFrame implements Frame{

    private final byte opcode;
    private final long id;
    private final String login;
    private final String password;

    /**
     * This method is the constructor of the class AskLoginWithCredentialsServerToServerFrame.
     * It create a frame when the server ask to the MDPserver if the password and login are in the database. 
     * @param opcode opcode of the frame as a byte.
     * @param login name of a client as a String.
     * @param password password of a client as a String.
     */
    public AskLoginWithCredentialsServerToServerFrame(byte opcode, String login, String password) {
        this.opcode = opcode;
        this.id = System.currentTimeMillis();
        this.login = login;
        this.password = password;
    }

    /**
     * Get the id of the frame.
     * @return id id of the request as long.
     */
    public long getId() {
        return id;
    }

    /**
     * Get the opcode of the frame.
     * @return opcode opcode of the frame as a short.
     */
    public short getOpcode() {
        return opcode;
    }

    /**
     * Get the login of the client in the frame.
     * @return login login of a client as a String.
     */
    public String getLogin() {
        return login;
    }
    
    /**
     * Create a ByteBuffer with all the information of the frame.
     * The login and password are from the client who wants a connection with credentials and the long is to find the write answer from the MDPserver.
     * @return ByteBuffer
     */
    @Override
    public ByteBuffer bufferize() {
        var loginBuffer = Utils.putString(login);
        var passwordBuffer = Utils.putString(password);
        var buffer = ByteBuffer.allocate(Short.BYTES + Long.BYTES + loginBuffer.remaining() + passwordBuffer.remaining());
        buffer.put(opcode).putLong(id).put(loginBuffer).put(passwordBuffer);
        return buffer.flip();
    }

}
