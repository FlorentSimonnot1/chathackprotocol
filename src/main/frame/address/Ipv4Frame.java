package main.frame.address;

import main.opcodes.Opcodes;

import java.nio.ByteBuffer;

/**
 * Ipv4Frame is an implementation of a frame.
 * It allows us to represent the address of a client when we need to establish a private connection. 
 */
public class Ipv4Frame implements AddressFrame{

    private final byte[] address;
    private final int port;

    /**
     * This method is the constructor of the class Ipv4Frame.
     * @param address an address as a byte array.
     * @param port port of connection as a int.
     */
    public Ipv4Frame(byte[] address, int port) {
        this.address = address;
        this.port = port;
    }

    /**
     * Gets the version of the address here Ipv4.
     * @return byte version of the address as a byte.
     */
    @Override
    public byte getVersion() {
        return Opcodes.IPV4;
    }

	/**
	 * Creates a byteBuffer with address in Ipv4.
	 * @return ByteBuffer the ByteBuffer that represent the frame.
	 */
    @Override
    public ByteBuffer bufferize() {
        return ByteBuffer.allocate(Byte.BYTES + address.length + Integer.BYTES).put(Opcodes.IPV4).put(address).putInt(port).flip();
    }

    /**
     * Gets the port to connect with the client.
     * @return port port of connection as a int.
     */
    @Override
    public int getPort() {
        return port;
    }

    /**
     * Gets the address in Ipv4.
     * @return address address of the client as a byte array.
     */
    @Override
    public byte[] getAddress() {
        return address;
    }
}
