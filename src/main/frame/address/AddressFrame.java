package main.frame.address;

import main.frame.Frame;

/**
 * AddressFrame is an implementation of a frame.
 * It allows us to represent the of a client in Ipv4 and Ipv6. 
 */
public interface AddressFrame extends Frame {

    /**
     * Gets the version of the address here Ipv4.
     * @return byte version of the address as a byte.
     */
    byte getVersion();

    /**
     * Gets the port to connect with the client.
     * @return port port of connection as a int.
     */
    int getPort();

    /**
     * Gets the address in Ipv4.
     * @return address address of the client as a byte array.
     */
    byte[] getAddress();
}
