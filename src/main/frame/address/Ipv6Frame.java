package main.frame.address;

import main.opcodes.Opcodes;

import java.nio.ByteBuffer;

/**
 * Ipv6Frame is an implementation of a frame.
 * It allows us to represent the address of a client when we need to establish a private connection.
 */
public class Ipv6Frame implements AddressFrame{

    private final byte[] address;
    private final int port;

    /**
     * This method is the constructor of the class Ipv6Frame.
     * @param address an address as a byte array.
     * @param port port of connection as a int.
     */
    public Ipv6Frame(byte[] address, int port) {
        this.address = address;
        this.port = port;
    }

    /**
     * Get the version of the address here Ipv6.
     * @return byte version of the address as a byte.
     */
    @Override
    public byte getVersion() {
        return Opcodes.IPV6;
    }
    
	/**
	 * Create a byteBuffer with address in Ipv6.
	 * @return ByteBuffer the ByteBuffer that represent the frame.
	 */
    @Override
    public ByteBuffer bufferize() {
        return ByteBuffer.allocate(Byte.BYTES + address.length + Integer.BYTES).put(Opcodes.IPV6).put(address).putInt(port).flip();
    }

    /**
     * Get the port to connect with the client.
     * @return port port of connection as a int.
     */
    @Override
    public int getPort() {
        return port;
    }

    /**
     * Get the address in Ipv6.
     * @return address address of the client as a byte array.
     */
    @Override
    public byte[] getAddress() {
        return address;
    }
}
