/**
 * Provides classes to implements Frame which represents an IP address (IPV4 or IPV6).
 */
package main.frame.address;