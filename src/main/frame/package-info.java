/**
 * Provides Frame classes used to create frame which contains data.
 * All frames used an opcode to precise what represent the frame.
 * The frame is adapted from the RFC.
 */
package main.frame;