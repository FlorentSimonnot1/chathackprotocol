package main.frame;

import java.nio.ByteBuffer;

/**
 * AskLoginWithoutCredentialsServerToServerFrame is an implementation of a frame.
 * It allows us to represent a frame when the server send a request of a guest connection to the MDPserver.
 */
public class AskLoginWithoutCredentialsServerToServerFrame implements Frame{

    private final byte opcode;
    private final long id;
    private final String login;

    /**
     * This method is the constructor of the class AskLoginWithoutCredentialsServerToServerFrame.
     * It create a frame when the server send an request to the MDPserver of a public connection.
     * @param opcode the opcode of the request as a byte.
     * @param login the name of the client who wants a connection as a String.
     */
    public AskLoginWithoutCredentialsServerToServerFrame(byte opcode, String login) {
        this.opcode = opcode;
        this.id = System.currentTimeMillis();
        this.login = login;;
    }

    /** 
     * Get the id of the connection
     * @return id id of the connection request as a long.
     */
    public long getId() {
        return id;
    }

    /**
     * get the opcode of the frame.
     * @return opcode opcode of the frame as a short.
     */
    public short getOpcode() {
        return opcode;
    }

    /**
     * Get the login of the client who want a public connection.
     * @return login login of a client as a String.
     */
    public String getLogin() {
        return login;
    }
    
    /**
     * Create a ByteBuffer with all the information of the frame.
     * The login is the one from the client who want a public connection.
	 * @return buff the ByteBuffer that represent the frame.
     */
    @Override
    public ByteBuffer bufferize() {
        var loginBuffer = Utils.putString(login);

        var buffer = ByteBuffer.allocate(Short.BYTES + Long.BYTES + loginBuffer.remaining());
        buffer.put(opcode).putLong(id).put(loginBuffer);
        return buffer.flip();
    }

}
