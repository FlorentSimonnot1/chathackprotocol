package main.frame;

import main.opcodes.Opcodes;
import main.visitor.ClientVisitor;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * ServerForwardPrivateConnexionDemandingFrame is an implementation of a frame.
 * It allows us to represent the demand from a client, this demand is forward by the server to another client. 
 */
public class ServerForwardPrivateConnexionDemandingFrame implements Frame {

    private final String sender;

    /**
     * This method is the constructor of the class ServerForwardPrivateConnexionDemandingFrame.
     * It create a frame when the server send the demand of a private connection from another client.
     * @param sender name of the sender as a String.
     */
    public ServerForwardPrivateConnexionDemandingFrame(String sender) {
        this.sender = sender;
    }

    /**
     * Get the sender.
     * @return the sender name.
     */
    public String getSender() {
        return sender;
    }

	/**
	 * Create a byteBuffer with the information when the server forward the demand of a private connection to a client.
	 * The frame contain the login of the client who's asking to a private connection.
	 * @return result the ByteBuffer that represent the frame.
	 */
    @Override
    public ByteBuffer bufferize() {
        var senderBuffer = Utils.putString(sender);
        var result = ByteBuffer.allocate(Byte.BYTES + senderBuffer.remaining());
        return result.put(Opcodes.PV_CNX_ASKED).put(senderBuffer).flip();
    }

	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the Client visitor that will accept this frame.
	 * @see ClientVisitor
	 */
    @Override
    public void accept(ClientVisitor visitor) throws IOException, InterruptedException {
        visitor.visitReceivePrivateConnexionDemanding(this);
    }
}
