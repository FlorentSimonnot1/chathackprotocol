package main.frame;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Objects;
import java.util.Optional;

/**
 * The Utils class implements some methods in order to avoid redundant code.
 * The class contains methods which treat buffer or files.
 */
public class Utils {
	/**
	 * Encodes a text and get the ByteBuffer which corresponds to the text
	 * @param txt the text we want to encode in UTF8 charset
	 * @return a ByteBuffer of the encoding
	 */
	public static ByteBuffer putString(String txt) {
		Objects.requireNonNull(txt);
		ByteBuffer txtBuffer = StandardCharsets.UTF_8.encode(txt);
		ByteBuffer buff = ByteBuffer.allocate(Integer.BYTES + txtBuffer.remaining());
        buff.putInt(txtBuffer.remaining());
        buff.put(txtBuffer);
        
        return buff.flip();
	}

	/**
	 * Decodes a ByteBuffer and get the text which corresponds to the ByteBuffer
	 * @param buffer a ByteBuffer
	 * @return a text of the decoding
	 */
	public static String getString(ByteBuffer buffer){
		Objects.requireNonNull(buffer);
		return StandardCharsets.UTF_8.decode(buffer).toString();
	}

	/**
	 * Create a ByteBuffer in according to a file get as a path
	 * @param path the path corresponding to the file
	 * @return a ByteBuffer corresponding to the file
	 */
	public static ByteBuffer putFile(Path path) {
		Objects.requireNonNull(path);
		try(FileChannel fc = FileChannel.open(path, StandardOpenOption.READ)){
			int size = (int) fc.size();
			ByteBuffer buff = ByteBuffer.allocate(size);

			while(buff.hasRemaining()) {
				if(fc.read(buff)==-1) {
					break;
				}
			}
			buff.flip();
			return ByteBuffer.allocate(Integer.BYTES + buff.remaining()).putInt(size).put(buff).flip();
		}catch(IOException e){
			System.out.println(e.getMessage());
			return null;
		}
	}

	/**
	 * Create a frame corresponding to the send of a file
	 * @param path the path corresponding to the file
	 * @return an optional of SendFileFrame. Return empty if the encoding failed.
	 */
	public static Optional<SendFileFrame> createSendFileFrame(String path){
		var file = putFile(Paths.get(path));
		if(file == null)
			return Optional.empty();
		return Optional.of(new SendFileFrame(path, file));
	}


	private static Path createDirectory(String clientName) throws IOException {
		var root = Paths.get("files");
		if(!Files.exists(root)){
			Files.createDirectory(root);
		}
		var folder = Paths.get("files/"+clientName);
		if(!Files.exists(folder)){
			Files.createDirectory(folder);
		}
		return folder;
	}

	/**
	 * Write a file for the client.
	 * @param clientName the client we want to write the file
	 * @param fileName the name of the file
	 * @param file the ByteBuffer which contains the file
	 * @throws IOException throw exception if writing file failed
	 */
	public static void writeFile(String clientName, String fileName, ByteBuffer file) throws IOException {
		var rootPath = createDirectory(clientName);
		try(FileChannel fc = FileChannel.open(Paths.get(rootPath+"/"+fileName), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE)){
			while(file.hasRemaining()){
				if(fc.write(file) == -1) break;
			}
		}
	}

	/**
	 * Transfer bytes from src ByteBuffer to dst ByteBuffer as long as possible.
	 * @param src  the source ByteBuffer
	 * @param dst the dest ByteBuffer
	 */
	public static void fillAsPossible(ByteBuffer src, ByteBuffer dst){
		while(src.hasRemaining() && dst.hasRemaining()){
			dst.put(src.get());
		}
	}
}
