package main.frame;

import main.opcodes.Opcodes;
import main.visitor.ServerVisitor;

import java.nio.ByteBuffer;

/**
 * DiffMsgClientFrame is an implementation of a frame.
 * It allows us to represent the frame when a client wants to send a broadcast message to all clients connected to the server. 
 */
public class DiffMsgClientFrame implements Frame{
	private final String msg;
	
	/**
     * This method is the constructor of the class DiffMsgClientFrame.
     * It create a frame when a client wants to send a broadcast message.
	 * @param msg the broadcast message the client wants to send as a String.
	 */
	public DiffMsgClientFrame(String msg) {
		this.msg = msg;
	}

	/**
	 * Get the message in the frame.
	 * @return msg the broadcast message as a String.
	 */
	public String getMessage() {
		return msg;
	}
	
	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the server visitor that will accept this frame.
	 * @see ServerVisitor
	 */
	@Override
	public void accept(ServerVisitor visitor) {
		visitor.visitBroadcastMessageFromClient(this);
	}

	/**
	 * Create a byteBuffer with the information when a client want to send a broadcast message.
	 * The frame contain the broadcast message.
	 * @return buff the ByteBuffer that represent the frame.
	 */
	@Override
	public ByteBuffer bufferize() {
		ByteBuffer msgBuff = Utils.putString(msg);
		
		ByteBuffer buff = ByteBuffer.allocate(Byte.BYTES + msgBuff.remaining());
		buff.put(Opcodes.SND_BROADCAST_MSG).put(msgBuff);
		return buff.flip();
	}
	
}
