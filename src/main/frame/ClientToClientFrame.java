package main.frame;

import java.nio.ByteBuffer;

import main.opcodes.Opcodes;
import main.visitor.ServerVisitor;

/**
 * ClientToClientFrame is an implementation of a frame.
 * It allows us to represent the demand of a private connection of a client to the server. 
 */
public class ClientToClientFrame implements Frame{
	private final String pseudo;
	
	/**
     * This method is the constructor of the class ClientToClientFrame.
     * It create a frame when a client wants to establish a connection with another client.
	 * @param pseudo the login of the client as a String.
	 */
	public ClientToClientFrame(String pseudo) {
		this.pseudo = pseudo;
	}
	
	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the Server visitor that will accept this frame.
	 * @see ServerVisitor
	 */
    @Override
    public void accept(ServerVisitor visitor) {
        visitor.visitPrivateConnexionDemanding(this);
    }

    /**
     * Get the pseudo of a client.
     * @return pseudo the login of a client as a String.
     */
    public String getPseudo() {
        return pseudo;
    }
    
	/**
	 * Create a byteBuffer with the information when a client wants to establish a new private connection.
	 * The pseudo is the client who will receive the demand.
	 * @return buff the ByteBuffer that represent the frame.
	 */
    @Override
    public ByteBuffer bufferize() {
        ByteBuffer pseudoBuff = Utils.putString(pseudo);      
        
        ByteBuffer buff = ByteBuffer.allocate(Byte.BYTES + pseudoBuff.remaining());
        buff.put(Opcodes.ASK_PERMISSION_PV_CNX).put(pseudoBuff);
        
        return buff.flip();
    }
}
