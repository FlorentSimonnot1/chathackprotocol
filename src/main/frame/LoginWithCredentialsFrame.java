package main.frame;

import java.nio.ByteBuffer;

import main.opcodes.Opcodes;
import main.visitor.NotConnectedVisitor;

/**
 * Represents a frame to establish a private connection to a server with a login.
 * @author Florent Simonnot
 */
public class LoginWithCredentialsFrame implements Frame {
    private final String login;
    private final String password;

    /**
     * This method is the constructor of the class LoginWithCredentialsFrame.
     * It create a frame when a client wants to start a private connection with the server.
     * @param login login of a client as a String.
     * @param password password of a client as a String.
     */
    public LoginWithCredentialsFrame(String login, String password) {
        this.login = login;
        this.password = password;
    }

    /**
     * Get the login.
     * @return login name of a client as a String.
     */
    public String getLogin(){return login;}

    /**
     * Get the password.
     * @return password password of a client as a String.
     */
    public String getPassword(){return password;}

	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the not connected visitor that will accept this frame.
	 * @see NotConnectedVisitor
	 */
    @Override
    public void accept(NotConnectedVisitor visitor) {
        visitor.visitLoginWithCredentials(this);
    }

	/**
	 * Create a byteBuffer with the information when a client wants to connect with the server with his login.
	 * The frame contain the id of the connection the login and password of the client.
	 * @return buff the ByteBuffer that represent the frame.
	 */
    @Override
    public ByteBuffer bufferize() {
    	ByteBuffer loginBuffer = Utils.putString(login);
        ByteBuffer passwordBuffer = Utils.putString(password);

        ByteBuffer buffer = ByteBuffer.allocate(Byte.BYTES + loginBuffer.remaining() + passwordBuffer.remaining());

        buffer.put(Opcodes.ASK_LOG_CNX).put(loginBuffer).put(passwordBuffer);
        return buffer.flip();
    }
}
