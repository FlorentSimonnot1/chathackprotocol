package main.frame;

import main.opcodes.Opcodes;
import main.visitor.ClientVisitor;
import main.visitor.PrivateClientVisitor;
import main.visitor.ServerVisitor;

import java.nio.ByteBuffer;

/**
 * AckFrame is an implementation of a frame.
 * It allows us to represent an acknowledgment. 
 */
public class AckFrame implements Frame {

    private final byte opcode;

    /**
     * This method is the constructor of the class AckFrame.
     * It create a frame when we need to send an acknowledgment.
     * @param opcode Is the opcode of the acknowledgement as a byte.
     */
    public AckFrame(byte opcode) {
        this.opcode = opcode;
    }

    /**
     * get the opcode.
     * @return opcode the acknowledgement as a byte.
     */
    public byte getOpcode() {
        return opcode;
    }

	/**
	 * Accept the visitor and call the visit method associate to this frame when a client exit the server.
	 * @param visitor Is the server visitor that will accept this frame.
	 * @see ServerVisitor
	 */
    @Override
    public void accept(ServerVisitor visitor) {
        if(Opcodes.SQUIT == opcode)
            visitor.visitDisconnection(this);
    }
    
	/**
	 * Accept the visitor and call the visit method associate to this frame when a client exit a private connection.
	 * @param visitor Is the private Client visitor that will accept this frame.
	 * @see PrivateClientVisitor
	 */
    @Override
    public void accept(PrivateClientVisitor visitor) {
        if(Opcodes.CQUIT == opcode)
            visitor.visitDisconnection(this);
    }
    
	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the Client visitor that will accept this frame.
	 * @see ClientVisitor
	 */
    @Override
    public void accept(ClientVisitor visitor) {
        visitor.visitAck(this);
    }

    /**
     * Create a ByteBuffer of a new AckFrame with the opcode of the acknowledgment.
     * @return buff the ByteBuffer that represent the frame.
     */
    @Override
    public ByteBuffer bufferize() {
        return ByteBuffer.allocate(Byte.BYTES).put(opcode).flip();
    }
}
