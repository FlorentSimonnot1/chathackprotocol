package main.frame;

import java.nio.ByteBuffer;

import main.opcodes.Opcodes;
import main.visitor.ClientVisitor;
import main.visitor.ServerVisitor;

/**
 * ErrorFrame is an implementation of a frame.
 * It allows us to represent an error with a message to describe this error.
 */
public class ErrorFrame implements Frame{
	private final byte opcode;
	private final String msg;
	
	/**
     * This method is the constructor of the class ErrorFrame.
     * It create a frame when the server wants to send an error.
	 * @param opcode opcode of the frame as a byte.
	 * @param msg error message as String.
	 */
	public ErrorFrame(byte opcode, String msg) {
		this.opcode = opcode;
		this.msg = msg;
	}

	/**
	 * Get the opcode of the error.
	 * @return opcode opcode as a byte.
	 */
	public byte getOpcode() {
		return opcode;
	}

	/**
	 * Get the error message.
	 * @return msg error message as a String.
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * Create a byteBuffer with the information when a the server send an error.
	 * The frame contain the opcode and the error message.
	 * @return buff the ByteBuffer that represent the frame.
	 */
	@Override
	public ByteBuffer bufferize() {
		ByteBuffer msgBuff = Utils.putString(msg);
		ByteBuffer buff = ByteBuffer.allocate(Byte.BYTES * 2 + msgBuff.remaining());
		
		buff.put(Opcodes.ERR).put(opcode).put(msgBuff);
		
		return buff.flip();
	}

	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the server visitor that will accept this frame.
	 * @see ServerVisitor
	 */
	@Override
	public void accept(ServerVisitor visitor) {

	}

	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the Client visitor that will accept this frame.
	 * @see ClientVisitor
	 */
	@Override
	public void accept(ClientVisitor visitor) {
		visitor.visitError(this);
	}
}
