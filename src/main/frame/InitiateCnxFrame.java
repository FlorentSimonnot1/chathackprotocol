package main.frame;

import java.nio.ByteBuffer;

import main.opcodes.Opcodes;
import main.visitor.PrivateClientVisitor;
import main.visitor.ServerVisitor;

/**
 * InitiateCnxFrame is an implementation of a frame.
 * It allows us to represent the frame when a client request a private connection.
 */
public class InitiateCnxFrame implements Frame{
	private final long tokenID;
	
	/**
     * This method is the constructor of the class InitiateCnxFrame.
     * It create a frame when a client wants to start a private connection.
	 * @param tokenID is of the connection request as a long.
	 */
	public InitiateCnxFrame(long tokenID) {
		this.tokenID = tokenID;  
	}

	/**
	 * Create a byteBuffer with the information when a client initiate a private connection.
	 * The frame contain the id of the connection.
	 * @return buff the ByteBuffer that represent the frame.
	 */
	@Override
	public ByteBuffer bufferize() {		
		ByteBuffer buff = ByteBuffer.allocate(Byte.BYTES + Long.BYTES);

		buff.put(Opcodes.ASK_PV_CNX).putLong(tokenID);
		return buff.flip();
	}

	/**
	 * Get the token of the connection.
	 * @return tokenID id of the request as a long.
	 */
	public long getTokenID() {
		return tokenID;
	}

	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the server visitor that will accept this frame.
	 * @see ServerVisitor
	 */
	@Override
	public void accept(ServerVisitor visitor) {

	}
	
	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the private Client visitor that will accept this frame.
	 * @see PrivateClientVisitor
	 */
	@Override
    public void accept(PrivateClientVisitor visitor) {
		visitor.visitInitiateCnx(this);
    }
}
