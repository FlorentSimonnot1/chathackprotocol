package main.frame;

import main.opcodes.Opcodes;
import main.visitor.ClientVisitor;

import java.nio.ByteBuffer;

/**
 * ClientNotFoundFrame is an implementation of a frame.
 * It allows us to represent a frame when the server cannot find a client corresponding to a login.
 */
public class ClientNotFoundFrame implements Frame {

    private final String dest;

    /**
     * This method is the constructor of the class ClientNotFoundFrame.
     * It create a frame when a the server cannot find the client associate with the login.
     * @param dest the name not found among the client as a String.
     */
    public ClientNotFoundFrame(String dest) {
        this.dest = dest;
    }
    
	/**
	 * Create a byteBuffer with the information when a the server don't find the client.
	 * The frame contain the login of the client.
	 * @return buff the ByteBuffer that represent the frame.
	 */
    @Override
    public ByteBuffer bufferize() {
        var destBuffer = Utils.putString(dest);
        var buffer = ByteBuffer.allocate(Byte.BYTES + destBuffer.remaining());
        buffer.put(Opcodes.ERR_CLIENT_NOT_ACCESSIBLE).put(destBuffer).flip();
        return buffer;
    }

    
    /**
     * Get the login of the client who doesn't exist.
     * @return dest the name not found as a String.
     */
    public String getDest() {
        return dest;
    }

	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the Client visitor that will accept this frame.
	 * @see ClientVisitor
	 */
    @Override
    public void accept(ClientVisitor visitor) {
        visitor.visitClientNotFound(this);
    }
}
