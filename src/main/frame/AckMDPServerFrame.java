package main.frame;

import main.visitor.MdpServerVisitor;

import java.nio.ByteBuffer;

/**
 * AckMDPServerFrame is an implementation of a frame.
 * It allows us to represent the answer of the MDPserver when a client wants a connection (with credentials or as a guest). 
 */
public class AckMDPServerFrame  implements Frame{

    private final byte opcode;
    private final long id;

    /**
     * This method is the constructor of the class AckMDPServerFrame.
     * It create a frame when the MDPserver send an acknowledgment to the server.
     * @param opcode opcode of the frame as a byte.
     * @param id id of the request as a long.
     */
    public AckMDPServerFrame(byte opcode, long id) {
        this.opcode = opcode;
        this.id = id;
    }

    /**
     * Get the opcode of the frame
     * @return opcode opcode of the frame.
     */
    public byte getOpcode() {
        return opcode;
    }

    /**
     * Get the id of the frame
     * @return id id of the request as a long.
     */
    public long getId() {
        return id;
    }

    /**
     * Create a ByteBuffer with all the information of the frame.
     * The opcode of the frame and the id of the connection.
	 * @return buff the ByteBuffer that represent the frame.
     */
    @Override
    public ByteBuffer bufferize() {
        var buffer = ByteBuffer.allocate(Byte.BYTES + Long.BYTES);
        return buffer.put(opcode).putLong(id).flip();
    }
    
	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the Mdpserver visitor that will accept this frame.
	 * @see MdpServerVisitor
	 */
    @Override
    public void accept(MdpServerVisitor visitor) {
        visitor.visitAckMDPServer(this);
    }
}
