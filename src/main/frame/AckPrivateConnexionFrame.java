package main.frame;

import main.frame.address.AddressFrame;
import main.opcodes.Opcodes;
import main.visitor.ClientVisitor;
import main.visitor.ServerVisitor;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * AckPrivateConnexionFrame is an implementation of a frame.
 * It allows us to represent the answer of the server to a client when he succeed to connect with the server.
 */
public class AckPrivateConnexionFrame implements Frame {

    private String pseudo;
    private final AddressFrame address;
    private final long token;

    /**
     * This method is the constructor of the class AckPrivateConnexionFrame.
     * It create a frame when the server send an acknowledgment to the client of a private connection.
     * @param pseudo name of a client as a String.
     * @param address the address of a client as an AddressFrame.
     * @param token id of a connection as a long.
     * @see AddressFrame
     */
    public AckPrivateConnexionFrame(String pseudo, AddressFrame address, long token) {
        this.pseudo = pseudo;
        this.address = address;
        this.token = token;
    }

    /**
     * Get the pseudo of the client who ask the connection
     * @return pseudo name of a client as a String.
     */
    public String getPseudo() {
        return pseudo;
    }

    /**
     * Change the field pseudo in the class.
     * @param newPseudo new name of a client as a String.
     */
    public void setPseudo(String newPseudo){
        this.pseudo = newPseudo;
    }

    /**
     * get the token of the connection
     * @return token the id of the frame as a long.
     */
    public long getToken() {
        return token;
    }

    /**
     * Get the address of the frame.
     * @return address the address of a client.
     * @see AddressFrame
     */
    public AddressFrame getAddress() {
        return address;
    }

    /**
     * Create a ByteBuffer with all the information of the frame.
     * The pseudo of the sender of the frame, the address of the sender and the id of the connection.
	 * @return buff the ByteBuffer that represent the frame.
     */
    @Override
    public ByteBuffer bufferize() {
        var pseudoBuffer = Utils.putString(pseudo);
        var frameBuffer = address.bufferize();
        var result = ByteBuffer.allocate(Byte.BYTES + pseudoBuffer.remaining() + frameBuffer.remaining() + Long.BYTES);
        result.put(Opcodes.ACK_ACCEPT_PV_CNX).put(pseudoBuffer).put(frameBuffer).putLong(token);
        return result.flip();
    }
    
	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the server visitor that will accept this frame.
	 * @see ServerVisitor
	 */
    @Override
    public void accept(ServerVisitor visitor) {
        visitor.visitPrivateConnexionAccepted(this);
    }
    
	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the Client visitor that will accept this frame.
	 * @see ClientVisitor
	 */
    @Override
    public void accept(ClientVisitor visitor) throws IOException, InterruptedException {
        visitor.visitPrivateConnexionRequestAccepted(this);
    }
}
