package main.frame;

import java.io.IOException;
import java.nio.ByteBuffer;

import main.opcodes.Opcodes;
import main.visitor.ClientVisitor;
import main.visitor.ServerVisitor;

/**
 * ClientToServerErrorFrame is an implementation of a frame.
 * It allows us to represent an error when a client failed his connection with the server.
 */
public class ClientToServerErrorFrame implements Frame {
	private String pseudoRequest;
	
	/**
     * This method is the constructor of the class ClientToServerErrorFrame.
     * It create a frame when there is an error in the connection client to server.
	 * @param pseudoRequest the pseudo of the client that send the request as a String.
	 */
	public ClientToServerErrorFrame(String pseudoRequest) {
		this.pseudoRequest = pseudoRequest;
	}

	/**
	 * Get the pseudo from the frame.
	 * @return pseudoRequest the pseudo of a client as a String.
	 */
	public String getPseudo() {
		return pseudoRequest;
	}

	
	/**
	 * Set the field pseudo of the class.
	 * @param newPseudo a new pseudo as a String.
	 */
	public void setPseudo(String newPseudo){pseudoRequest = newPseudo;}

	/**
	 * Create a byteBuffer with the information when a the server sends a connection error.
	 * The frame contain an opcode and the pseudo.
	 * @return buff the ByteBuffer that represent the frame.
	 */
	@Override
	public ByteBuffer bufferize() {
		ByteBuffer pseudoRequestBuff = Utils.putString(pseudoRequest);
		
		ByteBuffer buff =ByteBuffer.allocate(Byte.BYTES + pseudoRequestBuff.remaining());
		buff.put(Opcodes.ACK_REFUSE_PV_CNX).put(pseudoRequestBuff);
		return buff.flip();
	}
	
	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the server visitor that will accept this frame.
	 * @see ServerVisitor
	 */
	@Override
	public void accept(ServerVisitor visitor) {
		visitor.visitPrivateConnexionRefused(this);
	}
	
	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the Client visitor that will accept this frame.
	 * @see ClientVisitor
	 */
	@Override
	public void accept(ClientVisitor visitor) throws IOException, InterruptedException {
		visitor.visitPrivateConnexionRequestRefused(this);
	}
}
