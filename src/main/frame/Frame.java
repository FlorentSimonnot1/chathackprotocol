package main.frame;

import main.visitor.*;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Represents a frame entity and its methods.
 */
public interface Frame {

    /**
     * Create and return a ByteBuffer for this frame.
     * @return a ByteBuffer with necessary information about this.
     */
    ByteBuffer bufferize();

    /**
     * Accept the visitor <i>visitor</i>.
     * @param visitor a visitor which implements the NotConnectedVisitor interface.
     * @see NotConnectedVisitor
     */
    default void accept(NotConnectedVisitor visitor){visitor.visitDefault(this);}

    /**
     * Accept the visitor <i>visitor</i>.
     * @param visitor a visitor which implements the ServerVisitor interface.
     * @see ServerVisitor
     */
    default void accept(ServerVisitor visitor) {/*Nothing*/};

    /**
     * Accept the visitor <i>visitor</i>.
     * @param visitor a visitor which implements the ClientVisitor interface.
     * @see ClientVisitor
     * @throws IOException
     * @throws InterruptedException
     */
    default void accept(ClientVisitor visitor) throws IOException, InterruptedException {/*Nothing*/};

    /**
     * Accept the visitor <i>visitor</i>.
     * @param visitor a visitor which implements the PrivateClientVisitor interface.
     * @see PrivateClientVisitor
     */
    default void accept(PrivateClientVisitor visitor) {/*Nothing*/}

    /**
     * Accept the visitor <i>visitor</i>.
     * @param visitor a visitor which implements the MdpServerVisitor interface.
     * @see MdpServerVisitor
     */
    default void accept(MdpServerVisitor visitor){/*Nothing*/}
}
