package main.frame;

import java.nio.ByteBuffer;

import main.opcodes.Opcodes;
import main.visitor.PrivateClientVisitor;

/**
 * SendFileFrame is an implementation of a frame.
 * It allows us to represent a file send by a client to another. 
 */
public class SendFileFrame implements Frame{
	private final String path;
	private final ByteBuffer file;
	
	/**
     * This method is the constructor of the class SendFileFrame.
     * It create a frame when a client wants to send a file to another client.
	 * @param path path to the file as a String.
	 * @param file the file as a ByteBuffer.
	 */
	public SendFileFrame(String path, ByteBuffer file) {
		this.path = path;
		this.file = file;
	}

	/**
	 * Create a byteBuffer with the information when a client wants to send a file to another client.
	 * The frame contain the file.
	 * @return buff the ByteBuffer that represent the frame.
	 */
	@Override
	public ByteBuffer bufferize() {
		ByteBuffer fileNameBuff = Utils.putString(path);
		ByteBuffer buff = ByteBuffer.allocate(Byte.BYTES + fileNameBuff.remaining() + file.remaining());
		
		buff.put(Opcodes.SND_PV_FILE).put(fileNameBuff).put(file);
		buff.flip();
		return buff;
	}

	/**
	 * Get the free space remaining in the buffer.
	 * @return buff.remaining() size of the buffer as a int.
	 */
	public int getRemaining(){
		return bufferize().remaining();
	}
	
	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the private Client visitor that will accept this frame.
	 * @see PrivateClientVisitor
	 */
	@Override
    public void accept(PrivateClientVisitor visitor) {
		visitor.visitReceiveFile(this);
    }

    /**
     * Get the name of the sender.
     * @return name name of the file as a String.
     */
    public String getName(){
		var split = path.split("/");
		var length = split.length;
		return split[length-1];
	}

	/**
	 * Get the file in the message.
	 * @return file the file as a ByteBuffer.
	 */
	public ByteBuffer getFile() {
		return file;
	}
}
