package main.frame;

import java.nio.ByteBuffer;

import main.opcodes.Opcodes;
import main.visitor.PrivateClientVisitor;
import main.visitor.ServerVisitor;

/**
 * PrivateMsgFrame is an implementation of a frame.
 * It allows us to represent a private message from a client to another. 
 */
public class PrivateMsgFrame implements Frame{
	private final String msg;
	
	/**
     * This method is the constructor of the class PrivateMsgFrame.
     * It create a frame when a client wants to send a private message to another client.
	 * @param msg a message as a String.
	 */
	public PrivateMsgFrame(String msg) {
		this.msg = msg;
	}

	/**
	 * Create a byteBuffer with the information when a client send a private message to another client.
	 * The frame contain the message.
	 * @return buff the ByteBuffer that represent the frame.
	 */
	@Override
	public ByteBuffer bufferize() {		
		ByteBuffer msgBuff = Utils.putString(msg);
		
		ByteBuffer buff = ByteBuffer.allocate(Byte.BYTES + msgBuff.remaining());
		buff.put(Opcodes.SND_PV_MSG).put(msgBuff);
		return buff.flip();
	}

	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the server visitor that will accept this frame.
	 * @see ServerVisitor
	 */
	@Override
	public void accept(ServerVisitor visitor) {

	}

	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the private Client visitor that will accept this frame.
	 * @see PrivateClientVisitor
	 */
	@Override
    public void accept(PrivateClientVisitor visitor) {
		visitor.visitReceivePrivateMsg(this);
    }

	/**
	 * Get the message in the frame.
	 * @return msg a message as a String.
	 */
	public String getMessage() {
		return msg;
	}
}
