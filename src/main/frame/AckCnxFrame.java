package main.frame;

import java.nio.ByteBuffer;

import main.opcodes.Opcodes;
import main.visitor.PrivateClientVisitor;


/**
 * AckCnxFrame is an implementation of a frame.
 * It allows us to represent the acknowledgment send by the server to a client. 
 */
public class AckCnxFrame implements Frame{

	private final long tokenId;
	
	
	/**
     * This method is the constructor of the class AckCnxFrame.
     * It create a frame when the server send an acknowledgment of a connection to a client.
     * @param tokenId the token of the request of connection as a long.
	 */
	public AckCnxFrame(long tokenId) {
		this.tokenId = tokenId;
	}
	
	
	/**
	 * Create a byteBuffer with the opcode and the id of connection.
	 * @return buff the ByteBuffer that represent the frame.
	 */
	@Override
	public ByteBuffer bufferize() {
		ByteBuffer buff = ByteBuffer.allocate(Byte.BYTES + Long.BYTES);

		buff.put(Opcodes.ACK_PV_CNX_ESTABLISHED).putLong(tokenId);

		return buff.flip();
	}
	
	/**
	 * Get the token of the frame.
	 * @return tokenId the id of the connection request.
	 */
	public long getTokenId(){return tokenId;}

	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the private Client visitor that will accept this frame.
	 * @see PrivateClientVisitor
	 */
	@Override
	public void accept(PrivateClientVisitor visitor) {
		visitor.visitAckCnx(this);
	}
}
