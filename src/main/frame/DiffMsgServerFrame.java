package main.frame;

import java.nio.ByteBuffer;

import main.opcodes.Opcodes;
import main.visitor.ClientVisitor;

/**
 * DiffMsgServerFrame is an implementation of a frame.
 * It allows us to represent the frame when the server forward a broadcast message.
 */
public class DiffMsgServerFrame implements Frame{
	private final String pseudo;
	private final String msg;
	
	
	
	/**
     * This method is the constructor of the class DiffMsgServerFrame.
     * It create a frame when the server send a broadcast message receive from a client.
	 * @param pseudo Is pseudo of the author that write the broadcast message as a String.
	 * @param msg the broadcast message as a String.
	 */
	public DiffMsgServerFrame(String pseudo, String msg) {
		this.pseudo = pseudo;
		this.msg = msg;
	}

	/**
	 * Get the pseudo.
	 * @return pseudo login of the author as a String.
	 */
	public String getPseudo() {
		return pseudo;
	}

	/**
	 * Get the message from the frame.
	 * @return msg broadcast message as a String.
	 */
	public String getMessage() {
		return msg;
	}

	/**
	 * Create a byteBuffer with the information when a the server send a broadcast message.
	 * The frame contain the broadcast message and it's sender.
	 * @return buff the ByteBuffer that represent the frame.
	 */
	@Override
	public ByteBuffer bufferize() {
		ByteBuffer pseudoBuff = Utils.putString(pseudo);
		ByteBuffer msgBuff = Utils.putString(msg);
		
		ByteBuffer buff = ByteBuffer.allocate(Byte.BYTES + pseudoBuff.remaining() + msgBuff.remaining());
		buff.put(Opcodes.BROADCAST_MSG).put(pseudoBuff).put(msgBuff);
		return buff.flip();
	}

	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the  Client visitor that will accept this frame.
	 * @see ClientVisitor
	 */
	@Override
	public void accept(ClientVisitor visitor) {
		visitor.visitReceiveBroadcastMessage(this);
	}
}
