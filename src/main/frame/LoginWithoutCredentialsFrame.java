package main.frame;

import java.nio.ByteBuffer;

import main.opcodes.Opcodes;
import main.visitor.NotConnectedVisitor;

/**
 * LoginWithoutCredentialsFrame is an implementation of a frame.
 * It allows us to represent the frame when a client request a guest connection to the server.
 */
public class LoginWithoutCredentialsFrame implements Frame {
    private final String login;

    /**
     * This method is the constructor of the class LoginWithoutCredentialsFrame.
     * It create a frame when a client wants to start a guest connection with the server.
     * @param login name of the client as a String.
     */
    public LoginWithoutCredentialsFrame(String login) {
        this.login = login;
    }

    /**
     * Get the login of the client.
     * @return login name of a client as a String.
     */
    public String getLogin(){return login;}

	/**
	 * Create a byteBuffer with the information when a client wants to establish a guest connection with the server.
	 * The frame contain the login of the client.
	 * @return buff the ByteBuffer that represent the frame.
	 */
    @Override
    public ByteBuffer bufferize() {
    	ByteBuffer buffer = Utils.putString(login);
        
        ByteBuffer buff = ByteBuffer.allocate(Byte.BYTES + buffer.remaining());
        
        buff.put(Opcodes.ASK_GUEST_CNX).put(buffer);
        return buff.flip();
    }

	/**
	 * Accept the visitor and call the visit method associate to this frame.
	 * @param visitor Is the not connected visitor that will accept this frame.
	 * @see NotConnectedVisitor
	 */
    @Override
    public void accept(NotConnectedVisitor visitor) {
        visitor.visitLoginWithoutCredentials(this);
    }
}
