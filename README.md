# ChatHackProtocol 

## Installation

This project uses <a href="https://ant.apache.org/">ant</a> for the distribution. 
If you want to use our chat system yo have to execute only one amazing command. 

    sh build.sh

This command allows to create the jars files. 
At the same time, the unit tests will be executed.

To execute this application we have to use three jars files : 
- The first is **ServerMDP.jar** in the project's root. 
- The second is **Client.jar** in the folder **bin/client**. 
- The third is **Server.jar** in the folder **bin/server**. 

## Usage 

### First Step 

First you have to execute the **ServerMDP.jar**. Run this command : 

    java -jar ServerMDP.jar 4545 file.txt

**4545** is the port. Please don't change the port ! <br>
**fil.txt** is the file which contain users information (login and password). 

### Second Step

Next you have to execute the **Server.jar**. Use this command : 

    java -jar bin/server/Server.jar 7777

**7777** is the port choose to configure the server. You can change it but assure you 
to use the same port when you execute the client. 

Note : You can execute only one Server at the same time.

### Last Step

Then you have to execute the **Client.jar**. The command is : 

    java -jar bin/client/Client.jar localhost 7777 login (password) 
    
**localhost** is the host name used. <br>
**7777** is the port use for the connection. You can change this value but verify its correspond to the server port value. <br> 
**login** is the login used to create a connection with the server. <br>
**(password)** is the password used to create a connection with the server. If you are not registered in the MDP database don't used a password.

### Commands 


Once the application has started, you have a wide choice of commands possible. <br>
Indeed you will be able to send public messages, establish a private connection with a user, send messages or files. <br>

*Note* : There is no command to establish a private connection. This connection is establish when the first private message or file is send. 

#### Send a message 

You have to write the message in your console. 

Example : Send a public message to say "Hello everyone !". 

    Hello everyone 


#### Send a private message (@ command)

You have to write the message preceded by the recipient login. 

Example : Send a private message which says "Hello Batman" to Batman. 

    @Batman Hello Batman

#### Send a private file (/ command)

You have to type the file path preceded by the recipient login. 

Example : Send a private file name "DetectiveComics.txt" to Batman. 

    /Batman DetectiveComics.txt
    
Obviously you have to adapt the path of the file. 

#### Accept a private connection ($ command)

To accept a private connection, you have to write "Y" or "y" preceded by the sender login. 

Example : Accept a private connection from Batman. 

    $Batman Y
    
or 

    $Batman y
    
#### Refuse a private connection ($ command)

To refuse a private connection, you have to write "N" or "n" preceded by the sender login. 

Example : Accept a private connection from Batman. 

    $Batman N
    
or 

    $Batman n
    
#### Disconnection from a private connection (# command)

If you want to close a private connection with a user you have to write the interlocutor preceded by the **#** symbol. 

Example : Quit a private connection with Batman. 

    #Batman 
    
#### Disconnection from the server (# command)

If you want to close a connection with the server, you have to write the **#** symbol. 

    #
    
Easy, isn't it ? ;) 

## Authorizations 

When you use this application, you authorize the application to access your files. <br>
When you receive a file, we save it in a folder corresponding to the interlocutor name.
This folder is locate in your home folder. 

## License

MIT License